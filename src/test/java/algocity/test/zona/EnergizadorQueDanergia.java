package algocity.test.zona;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.energia.Energizador;

class EnergizadorQueDanergia implements Energizador {
    @Override
    public void dameEnergía(int energia) throws EnergiaInsuficienteException {

    }

    @Override
    public boolean estaEnergizada() {
        return true;
    }

    @Override
    public int consultarEnergiaRestante() {
        return 10000;
    }

    @Override
    public void compensarEnergia(int energia) {

    }
}
