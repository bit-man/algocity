package algocity.test.zona;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.catastrofe.Terremoto;
import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.necesidades.TuberiaDeAgua;
import algocity.modelo.zona.*;
import algocity.test.AssertionsAlgoCity;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EstacionDeBomberosTest {

    private Jugador jugador;
    private Mapa mapa;

    @Before
    public void init(){
        try {
            FuncionAzar azar = new ConstructorAzarSimulado().
                    agregarHectareas(10*10).todoTierra().crear();
            this.mapa = new Mapa(10,10, azar);
            this.jugador = new Jugador("Pepe", mapa);;
        } catch (Throwable e) {
            // Nombre bien formado y constante
        }
    }

    @Test
    public void testConstructorNoNulo() throws LongitudNombreJugadorInsuficienteException {
        assertNotNull(new EstacionDeBomberos(jugador));
    }

    @Test
    public void testCostoConstruccion() throws LongitudNombreJugadorInsuficienteException {
        assertEquals(1500, new EstacionDeBomberos(new Jugador("Pepe", null)).getCosto());
    }

    @Test( expected = SuperficieInvalidaException.class)
    public void testConstruiteNoPuede() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos( jugador);
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        estacionDeBomberos.construite(hectarea);
    }

    @Test
    public void testConstruiteOKSobreLlano() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        estacionDeBomberos.construite(hectarea);
    }

    @Test
    public void testConstruiteVerificarHectarea() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        estacionDeBomberos.construite(hectarea);

        assertTrue(hectarea.getConstruccion() == estacionDeBomberos);
    }

    @Test(expected = DineroInsuficienteException.class)
    public void testConstruiteNoHayPlata() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);
        jugador.quitarDinero(jugador.getDinero());
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        estacionDeBomberos.construite(hectarea);

        assertTrue(hectarea.getConstruccion() == estacionDeBomberos);
    }

    @Test
    public void testRepararTerrenoBaldioNoEsPosiblePorquenNoSeRompe() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        final Jugador jugador = new Jugador("Pepe", mapa);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        final Construccion construccion = epicentro.getConstruccion();
        construccion.daniar( new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 1.0, construccion.getIntegridad());
    }

    @Test
    public void testRepararComercial() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        final Jugador jugador = new Jugador("Pepe", mapa);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        final Comercial construccion = new Comercial(jugador);
        construccion.construite(epicentro);

        // El danio es del 100%
        construccion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 0.07, construccion.getIntegridad());
    }

    @Test
    public void testRepararIndustria() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        new Industrial(jugador).construite(epicentro);
        final Construccion construccion = epicentro.getConstruccion();

        construccion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 0.03, construccion.getIntegridad());
    }

    @Test
    public void testRepararIndustriaEnCadaTurno() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, EnergiaInsuficienteException {
        final Jugador jugador = new Jugador("Pepe", mapa);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        new EstacionDeBomberos(jugador).construite(epicentro);

        new Industrial(jugador).construite(epicentro);

        final Construccion construccion = epicentro.getConstruccion();

        construccion.daniar(new Terremoto(mapa, epicentro));

        jugador.avanzarTiempo();
        AssertionsAlgoCity.assertEqualsFloat((float) 0.03, construccion.getIntegridad());

        jugador.avanzarTiempo();
        AssertionsAlgoCity.assertEqualsFloat((float) 0.06, construccion.getIntegridad());
    }

    @Test
    public void testRepararComercialEnCadaTurno() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, EnergiaInsuficienteException {
        final Jugador jugador = new Jugador("Pepe", mapa);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        new EstacionDeBomberos(jugador).construite(epicentro);

        new Comercial(jugador).construite(epicentro);

        final Construccion construccion = epicentro.getConstruccion();

        construccion.daniar(new Terremoto(mapa, epicentro));

        jugador.avanzarTiempo();
        AssertionsAlgoCity.assertEqualsFloat((float) 0.07, construccion.getIntegridad());

        jugador.avanzarTiempo();
        AssertionsAlgoCity.assertEqualsFloat((float) 0.14, construccion.getIntegridad());
    }

    @Test
    public void testNoReparaLineaDeTension() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        final Conexion conexion = new LineaDeTension(jugador);

        // La central se va a romper pero no importa, es para poder construir la línea de tensión
        Hectarea destino = mapa.iterator(0,1).elementoActual();
        new CentralElectrica(CentralEolica.getInstance(),mapa,jugador).construite(epicentro);

        conexion.construite(epicentro, destino);

        conexion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 0.0, conexion.getIntegridad());
    }

    @Test
    public void testNoReparaLineaDeTensionYLevantaLosEscombros() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        final Conexion conexion = new LineaDeTension(jugador);

        // La central se va a romper pero no importa, es para poder construir la línea de tensión
        Hectarea destino = mapa.iterator(0,1).elementoActual();
        new CentralElectrica(CentralEolica.getInstance(),mapa,jugador).construite(epicentro);

        conexion.construite(epicentro, destino);

        conexion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        Assert.assertTrue(epicentro.getConexionesDisponibles().isEmpty());
        Assert.assertTrue(destino.getConexionesDisponibles().isEmpty());
    }


    @Test
    public void testNoReparaTuberiaDeAgua() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        epicentro.setSuperficie(SuperficieAgua.getInstance());
        new PozoDeAgua(jugador).construite(epicentro);
        final Conexion conexion = new TuberiaDeAgua(jugador);

        // La central se va a romper pero no importa, es para poder construir la línea de tensión
        Hectarea destino = mapa.iterator(0,1).elementoActual();

        conexion.construite(epicentro, destino);

        conexion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 1.0, conexion.getIntegridad());
    }

    @Test
    public void testNoReparaTuberiaDeAguaNiLaLevantaPorqueEstaBajoTierra() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        epicentro.setSuperficie(SuperficieAgua.getInstance());
        new PozoDeAgua(jugador).construite(epicentro);
        final Conexion conexion = new TuberiaDeAgua(jugador);

        // La central se va a romper pero no importa, es para poder construir la línea de tensión
        Hectarea destino = mapa.iterator(0,1).elementoActual();

        conexion.construite(epicentro, destino);

        conexion.daniar(new Terremoto(mapa, epicentro));

        estacionDeBomberos.reparar();

        assertEquals(1, epicentro.getConexionesDisponibles().size());
        assertEquals(1, destino.getConexionesDisponibles().size());
    }


    @Test
    public void testReparaCentralEolica() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        final EstacionDeBomberos estacionDeBomberos = new EstacionDeBomberos(jugador);

        final Hectarea epicentro = mapa.iterator(0,0).elementoActual();
        CentralElectrica centralElectrica = new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);

        centralElectrica.construite(epicentro);
        new Terremoto(mapa, epicentro).hacerDanio();
        estacionDeBomberos.reparar();

        AssertionsAlgoCity.assertEqualsFloat((float) 0.15, centralElectrica.getIntegridad());
    }
}
