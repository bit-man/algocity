package algocity.test.zona;

import algocity.modelo.zona.SuperficieAgua;
import org.junit.Test;

import static junit.framework.Assert.*;

public class SuperficieAguaTest {


    @Test
    public void testConstructorNoNulo() {
        assertNotNull(SuperficieAgua.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(SuperficieAgua.getInstance() == SuperficieAgua.getInstance());
    }


    @Test
    public void testEsAgua() {
        assertTrue(SuperficieAgua.getInstance().esAgua());
    }

    @Test
    public void testNoEsLlano() {
        assertFalse(SuperficieAgua.getInstance().esLlano());
    }



}
