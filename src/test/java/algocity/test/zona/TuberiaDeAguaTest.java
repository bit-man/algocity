package algocity.test.zona;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.necesidades.TuberiaDeAgua;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;
import algocity.modelo.zona.PozoDeAgua;
import algocity.modelo.zona.SuperficieAgua;
import algocity.modelo.zona.SuperficieLlano;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class TuberiaDeAguaTest {


    private Mapa mapa;
    private Jugador jugador;

    @Before
    public void init() {
        try {
            final FuncionAzar azar = new ConstructorAzarSimulado().agregarHectareas(10 * 10).todoTierra().crear();
            mapa = new Mapa(10, 10, azar);
            jugador = new Jugador("Pepe", mapa);
        } catch (Throwable e) {
            // Nombre bien formado y parámetros fijos
        }
    }
    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new TuberiaDeAgua(MonetizableNull.getInstance()));
    }

    @Test
    public void testEsRuta() {
        assertFalse(new TuberiaDeAgua(MonetizableNull.getInstance()).esRutaPavimentada());
    }

    @Test
    public void testEsLineaDeTension() {
        assertFalse(new TuberiaDeAgua(MonetizableNull.getInstance()).esLineaDeTension());
    }

    @Test
    public void testEsTuberia() {
        assertTrue(new TuberiaDeAgua(MonetizableNull.getInstance()).esTuberiaDeAgua());
    }

    @Test
    public void testCostoDeConstruccion() {
        assertEquals(5, new TuberiaDeAgua(MonetizableNull.getInstance()).getCostoDeConstruccionPorHectarea());
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirTuberiaDeAguaOrigenLlano() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);
    }

    @Test
    public void testNoPuedoConstruirTuberiaDeAguaOrigenLlanoExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue(e.getSuperficie().esLlano());
        }
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirTuberiaDeAguaDestinoAgua() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        destino.setConstruccion(new PozoDeAgua(jugador));

        conexion.construite(origen, destino);
    }


    @Test
    public void testNoPuedoConstruirTuberiaDeAguaDestinoAguaExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        destino.setConstruccion(new PozoDeAgua(jugador));

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue( e.getSuperficie().esAgua() );
        }
    }


    @Test
    public void testPuedoConstruirTuberiaDeAguaVerHectareaDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( destino.getConexionesDisponibles().contains(conexion) );
    }

    @Test
    public void testPuedoConstruirTuberiaDeAguaVerHectareaOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( origen.getConexionesDisponibles().contains(conexion) );
    }


    @Test
    public void testPuedoConstruirTuberiaDeAguaVerConexionOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getOrigen() == origen );
    }

    @Test
    public void testPuedoConstruirTuberiaDeAguaVerConexionDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        origen.setConstruccion( new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getDestino() == destino );
    }


    @Test
    public void testConstruccionTuberiaDeAgua10haQuitaDineroJugador() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Conexion conexion = new TuberiaDeAgua(jugador);
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieAgua.getInstance());
        origen.setConstruccion(new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());

        final int dineroAntes = jugador.getDinero();

        conexion.construite(origen, destino);

        assertEquals( 50, dineroAntes - jugador.getDinero() );
    }

    @Test( expected = DineroInsuficienteException.class)
    public void testConstruccionDineroJugadorNoAlcanza() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        Conexion conexion = new TuberiaDeAgua(jugador);
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieAgua.getInstance());
        origen.setConstruccion(new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());

        // se queda sin dinero
        jugador.quitarDinero( jugador.getDinero() );

        conexion.construite(origen, destino);
    }

    @Test
    public void testConstruccionDineroJugadorNoAlcanzaOrigenNull() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Jugador jugador = new Jugador("Pepe", null);
        Conexion conexion = new TuberiaDeAgua(jugador);
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieAgua.getInstance());
        origen.setConstruccion(new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());

        // se queda sin dinero
        jugador.quitarDinero( jugador.getDinero() );

        try {
            conexion.construite(origen, destino);
            fail("No hay dinero suficiente, debe fallar la construcción");
        } catch (DineroInsuficienteException e) {
            assertNull(conexion.getOrigen());
        }
    }



    @Test
    public void testConstruccionDineroJugadorNoAlcanzaDestinoNull() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, LongitudNombreJugadorInsuficienteException {
        Jugador jugador = new Jugador("Pepe", null);
        Conexion conexion = new TuberiaDeAgua(jugador);
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieAgua.getInstance());
        origen.setConstruccion(new PozoDeAgua(jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());

        // se queda sin dinero
        jugador.quitarDinero( jugador.getDinero() );

        try {
            conexion.construite(origen, destino);
            fail("No hay dinero suficiente, debe fallar la construcción");
        } catch (DineroInsuficienteException e) {
            assertNull(conexion.getDestino());
        }
    }

}
