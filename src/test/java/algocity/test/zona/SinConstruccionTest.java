	package algocity.test.zona;

    import algocity.modelo.mapa.Hectarea;
    import algocity.modelo.mapa.Posicion;
    import algocity.modelo.zona.SinConstruccion;
    import algocity.modelo.zona.SuperficieAgua;
    import algocity.modelo.zona.SuperficieLlano;
    import org.junit.Test;

    import static junit.framework.Assert.*;

public class SinConstruccionTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new SinConstruccion());
    }

    @Test
    public void testConstruite() {

        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        SinConstruccion sinConstruccion = new SinConstruccion();
        sinConstruccion.construite(hectarea);

        assertTrue(sinConstruccion == hectarea.getConstruccion());
    }

    @Test
    public void testEstaSobreLLano() {

        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        SinConstruccion sinConstruccion = new SinConstruccion();
        sinConstruccion.construite(hectarea);

        assertTrue(sinConstruccion.estaSobreLlano() );
        assertFalse(sinConstruccion.estaSobreAgua() );
    }

    @Test
    public void testEstaSobreAgua() {

        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        SinConstruccion sinConstruccion = new SinConstruccion();
        sinConstruccion.construite(hectarea);

        assertFalse(sinConstruccion.estaSobreLlano() );
        assertTrue(sinConstruccion.estaSobreAgua() );
    }




}
