package algocity.test.zona;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.ComparadorNecesidad;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;
import algocity.modelo.zona.SuperficieAgua;
import algocity.modelo.zona.SuperficieLlano;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class LineaDeTensionTest {

    private Jugador jugador;
    private Mapa mapa;

    @Before
    public void init() {
        try {
            final FuncionAzar azar = new ConstructorAzarSimulado().agregarHectareas(10 * 10).todoTierra().crear();
            mapa = new Mapa(10, 10, azar);
            jugador = new Jugador("Pepe", mapa);
        } catch (Throwable e) {
            // Nombre bien formado y parámetros fijos
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new LineaDeTension(MonetizableNull.getInstance()));
    }

    @Test
    public void testEsRuta() {
        assertFalse(new LineaDeTension(MonetizableNull.getInstance()).esRutaPavimentada());
    }

    @Test
    public void testEsLineaDeTension() {
        assertTrue(new LineaDeTension(MonetizableNull.getInstance()).esLineaDeTension());
    }

    @Test
    public void testEsTuberia() {
        assertFalse(new LineaDeTension(MonetizableNull.getInstance()).esTuberiaDeAgua());
    }


    @Test
    public void testCostoDeConstruccion() {
        assertEquals(5, new LineaDeTension(MonetizableNull.getInstance()).getCostoDeConstruccionPorHectarea());
    }

    @Test
    public void testConstruccionPoneEnergizadorEnHectareaDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, ConstruccionInvalidaExcepcion {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(10_000_000);
        Conexion conexion = new LineaDeTension(jugador);
        Hectarea origen = new Hectarea(new Posicion(0, 5), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(5, 0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue(destino.estaEnergizada());
    }

    @Test
    public void testLaEnergiaEsLaMismaQueDeLaCentral() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, ConstruccionInvalidaExcepcion {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(10_000_000);
        Conexion conexion = new LineaDeTension(jugador);
        Hectarea origen = new Hectarea(new Posicion(0, 5), SuperficieLlano.getInstance());

        final CentralElectrica centralElectrica = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        origen.setConstruccion(centralElectrica);

        Hectarea destino = new Hectarea(new Posicion(5, 0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);
        assertEquals( centralElectrica.getCapacidadRestante(), destino.consultarEnergia() );
    }
    @Test
    public void testQuitarenergiaDeLaHectareaEsQuitarDeLaCentral() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException,  EnergiaInsuficienteException, ConstruccionInvalidaExcepcion {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(10_000_000);
        Conexion conexion = new LineaDeTension(jugador);
        Hectarea origen = new Hectarea(new Posicion(0, 5), SuperficieLlano.getInstance());

        final CentralElectrica centralElectrica = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        origen.setConstruccion(centralElectrica);

        Hectarea destino = new Hectarea(new Posicion(5, 0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        final int capacidadAbastecimiento = centralElectrica.getCapacidadRestante();

        destino.extraerEnergia(100);

        assertEquals(100, capacidadAbastecimiento - centralElectrica.getCapacidadRestante());
    }


    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirLineaDeTensionOrigenAgua() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);
    }

    @Test
    public void testNoPuedoConstruirLineaDeTensionOrigenAguaExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue(e.getSuperficie().esAgua());
        }
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirLineaDeTensionDestinoAgua() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());

        conexion.construite(origen, destino);
    }


    @Test
    public void testNoPuedoConstruirLineaDeTensionDestinoAguaExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue( e.getSuperficie().esAgua() );
        }
    }


    @Test
    public void testPuedoConstruirLineaDeTensionVerHectareaDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( destino.getConexionesDisponibles().contains(conexion) );
    }

    @Test
    public void testPuedoConstruirLineaDeTensionVerHectareaOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, FueraDelMapaException {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = mapa.iterator(0,0).elementoActual();
        Hectarea destino = mapa.iterator(0,1).elementoActual();
        new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), MonetizableNull.getInstance()).construite(origen);

        conexion.construite(origen, destino);

        assertTrue(ColeccionesAlgoCity.contiene(origen.getConexionesDisponibles(), new ComparadorNecesidad(conexion)));
    }


    @Test
    public void testPuedoConstruirLineaDeTensionVerConexionOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getOrigen() == origen );
    }

    @Test
    public void testPuedoConstruirLineaDeTensionVerConexionDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getDestino() == destino );
    }

    @Test
    public void testCostoConexionLineaDeTension1ha() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 5, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionLineaDeTension1haInversa() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 5, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionLineaDeTension10ha() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 50, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionLineaDeTension10haInversa() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieLlano.getInstance());
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals(50, conexion.calcularCosto());
    }

    // ToDo faltan test de calcularCosto() de PozoDeAgua

    @Test
    public void testConstruccionLineaDeTension10haQuitaDineroJugador() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new LineaDeTension(jugador);
        Hectarea origen = mapa.iterator(0,5).elementoActual();
        origen.setConstruccion(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador));
        Hectarea destino = mapa.iterator(5,0).elementoActual();

        final int dineroAntes = jugador.getDinero();

        conexion.construite(origen, destino);

        assertEquals( 50, dineroAntes - jugador.getDinero() );
    }

    @Test
    public void testEqualsSonIguales(){
        assertEquals(new LineaDeTension(jugador), new LineaDeTension(jugador));
    }
    @Test
    public void testEqualsNoSonIguales()
            throws JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, SuperficieInvalidaException,
            DineroInsuficienteException, FueraDelMapaException
    {
        LineaDeTension expected = new LineaDeTension(jugador);
        Hectarea hectareaCentral = mapa.iterator(0, 0).elementoActual();
        new CentralElectrica(CentralEolica.getInstance(), mapa, jugador).construite(hectareaCentral);
        expected.construite(hectareaCentral, mapa.iterator(0, 1).elementoActual());
        assertFalse(expected.equals( new LineaDeTension(jugador) ) );
    }
}
