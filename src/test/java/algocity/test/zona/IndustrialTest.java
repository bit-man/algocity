package algocity.test.zona;

import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.zona.Industrial;
import algocity.modelo.zona.SuperficieLlano;
import algocity.modelo.zona.Zona;
import algocity.test.TestHelper;
import org.junit.Test;

import static junit.framework.Assert.*;

public class IndustrialTest {

    @Test
    public void testConstructorNoNulo() throws LongitudNombreJugadorInsuficienteException {
        assertNotNull( new Industrial(new Jugador("Pepe", null)));
    }

    @Test
    public void testNoEsPozoDeAgua() throws LongitudNombreJugadorInsuficienteException {
        assertFalse(new Industrial(new Jugador("Pepe", null)).esPozoDeAgua());
    }


    @Test
    public void testNoEsHabitableFaltaTensionYCaminos() throws Throwable {
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        final Zona zona = new Industrial(jugador);
        zona.construite(hectareaZona);

        assertFalse(zona.esUsable());
    }

    @Test
    public void testNoEsHabitableFaltaTension() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        final Zona zona = new Industrial(jugador);
        zona.construite(hectareaZona);

        Hectarea hectareaOtroExtremo = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        new RutaPavimentada(jugador).construite(hectareaZona, hectareaOtroExtremo);  // Hay camino

        assertFalse(zona.esUsable());
    }

    @Test
    public void testNoEsHabitableFaltaCamino() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        hectarea.ahoraTieneEnergia(new EnergizadorQueDanergia()); // Hay tensión

        final Zona zona = new Industrial(jugador);
        zona.construite(hectarea);

        assertFalse(zona.esUsable());
    }

    @Test
    public void testEsHabitableSiHayEnergiaYCaminos() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        hectareaZona.ahoraTieneEnergia(new EnergizadorQueDanergia()); // Hay tensión

        Hectarea hectareaOtroExtremo = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        new RutaPavimentada(jugador).construite(hectareaZona, hectareaOtroExtremo);  // Hay camino

        final Zona zona = new Industrial(jugador);
        zona.construite(hectareaZona);

        assertTrue(zona.esUsable());
    }

}
