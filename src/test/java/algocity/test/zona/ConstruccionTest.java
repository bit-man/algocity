package algocity.test.zona;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.energia.EnergizadorCentralElectrica;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.*;
import algocity.test.TestHelper;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class ConstruccionTest {

    @Test
    public void puedoConstruirResidencial() throws EnergiaInsuficienteException, SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        Posicion posicion = new Posicion(0, 0);
        Superficie superficie = SuperficieLlano.getInstance();

        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(posicion, superficie);
        hectarea.agregarConexion(new LineaDeTension(jugador));
        hectarea.agregarConexion(new RutaPavimentada(jugador));

        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        hectarea.ahoraTieneEnergia(new EnergizadorCentralElectrica(central));

        Zona zona = new Residencial(jugador);

        zona.construite(hectarea);
    }

    @Test
    public void puedoConstruirResidencialVerHectarea() throws EnergiaInsuficienteException, SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        Posicion posicion = new Posicion(0, 0);
        Superficie superficie = SuperficieLlano.getInstance();

        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(posicion, superficie);
        hectarea.agregarConexion(new LineaDeTension(jugador));
        hectarea.agregarConexion(new RutaPavimentada(jugador));

        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        hectarea.ahoraTieneEnergia(new EnergizadorCentralElectrica(central));

        Zona zona = new Residencial(jugador);

        zona.construite(hectarea);

        assertTrue(hectarea.getConstruccion() == zona) ;
    }

    @Test
    public void puedoConstruirComercial() throws EnergiaInsuficienteException, SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        Posicion posicion = new Posicion(0, 0);
        Superficie superficie = SuperficieLlano.getInstance();

        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(posicion, superficie);
        hectarea.agregarConexion(new LineaDeTension(jugador));
        hectarea.agregarConexion(new RutaPavimentada(jugador));
        Zona zona = new Comercial(jugador);

        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        hectarea.ahoraTieneEnergia(new EnergizadorCentralElectrica(central));

        zona.construite(hectarea);
    }

    @Test
    public void puedoConstruirIndustrial() throws EnergiaInsuficienteException, SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        Posicion posicion = new Posicion(0, 0);
        Superficie superficie = SuperficieLlano.getInstance();

        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(posicion, superficie);
        hectarea.agregarConexion(new LineaDeTension(jugador));
        hectarea.agregarConexion(new RutaPavimentada(jugador));
        Zona zona = new Industrial(jugador);

        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        hectarea.ahoraTieneEnergia(new EnergizadorCentralElectrica(central));

        zona.construite(hectarea);
    }

    @Test(expected = DineroInsuficienteException.class)
    public void testNoPuedoConstruirSinPlata() throws EnergiaInsuficienteException, SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {

        Posicion posicion = new Posicion(0, 0);
        Superficie superficie = SuperficieLlano.getInstance();

        Jugador jugador = new Jugador("Pepe", null);
        jugador.quitarDinero(jugador.getDinero());
        Zona zona = new Industrial(jugador);
        Hectarea hectarea = new Hectarea(posicion, superficie);
        hectarea.agregarConexion(new LineaDeTension(jugador));
        hectarea.agregarConexion(new RutaPavimentada(jugador));

        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);
        hectarea.ahoraTieneEnergia(new EnergizadorCentralElectrica(central));
        final int capacidadAbastecimiento = central.getCapacidadRestante();

        zona.construite(hectarea);

    }

}
