package algocity.test.zona;

import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.PozoDeAgua;
import algocity.modelo.zona.SuperficieAgua;
import algocity.modelo.zona.SuperficieLlano;
import algocity.test.TestHelper;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class PozoDeAguaTest {


    @Test
    public void devolverElCostoDelPozoDeAgua() throws LongitudNombreJugadorInsuficienteException {
        assertEquals(250, new PozoDeAgua(new Jugador("Pepe", null)).getCosto());
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoConstruyeSobreTierra() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        new PozoDeAgua(new Jugador("Pepe", null)).construite(hectarea);
    }

    @Test
    public void testSeConstruyeSobreAgua() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        new PozoDeAgua(jugador).construite(hectarea);
    }

    @Test(expected = DineroInsuficienteException.class)
    public void testNoSeConstruyeSinPlata() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Jugador jugador = new Jugador("Pepe", null);
        jugador.quitarDinero(jugador.getDinero());
        new PozoDeAgua(jugador).construite(hectarea);
    }

    @Test
    public void testSeConstruyeconPlata() throws SuperficieInvalidaException, LongitudNombreJugadorInsuficienteException, DineroInsuficienteException {
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        new PozoDeAgua(jugador).construite(hectarea);
    }

    @Test
    public void testSeConstruyeconVerHectarea() throws SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException {
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        PozoDeAgua pozoDeAgua = new PozoDeAgua(jugador);
        pozoDeAgua.construite(hectarea);
        assertTrue( pozoDeAgua == hectarea.getConstruccion());
    }

}
