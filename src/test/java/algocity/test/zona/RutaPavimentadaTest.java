package algocity.test.zona;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.MonetizableNull;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;
import algocity.modelo.zona.SuperficieAgua;
import algocity.modelo.zona.SuperficieLlano;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class RutaPavimentadaTest {

    private Mapa mapa;
    private Jugador jugador;

    @Before
    public void init() {
        try {
            final FuncionAzar azar = new ConstructorAzarSimulado().agregarHectareas(10 * 10).todoTierra().crear();
            mapa = new Mapa(10, 10, azar);
            jugador = new Jugador("Pepe", mapa);
        } catch (Throwable e) {
            // Nombre bien formado y parámetros fijos
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new RutaPavimentada(MonetizableNull.getInstance()));
    }

    @Test
    public void testEsRuta() {
        assertTrue(new RutaPavimentada(MonetizableNull.getInstance()).esRutaPavimentada());
    }

    @Test
    public void testEsLineaDeTension() {
        assertFalse(new RutaPavimentada(MonetizableNull.getInstance()).esLineaDeTension());
    }

    @Test
    public void testEsTuberia() {
        assertFalse(new RutaPavimentada(MonetizableNull.getInstance()).esTuberiaDeAgua());
    }


    @Test
    public void testCostoDeConstruccion() {
        assertEquals(10, new RutaPavimentada(MonetizableNull.getInstance()).getCostoDeConstruccionPorHectarea());
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirRutaOrigenAgua() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);
    }

    @Test
    public void testNoPuedoConstruirRutaOrigenAguaExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue(e.getSuperficie().esAgua());
        }
    }

    @Test(expected = SuperficieInvalidaException.class)
    public void testNoPuedoConstruirRutaDestinoAgua() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());

        conexion.construite(origen, destino);
    }


    @Test
    public void testNoPuedoConstruirRutaDestinoAguaExcSuperficie() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());

        try {
            conexion.construite(origen, destino);
            fail("No puede construirse un extremo en el agua");
        } catch (SuperficieInvalidaException e) {
            assertTrue( e.getSuperficie().esAgua() );
        }
    }

    @Test
    public void testPuedoConstruirRutaVerHectareaDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( destino.getConexionesDisponibles().contains(conexion) );
    }

    @Test
    public void testPuedoConstruirRutaVerHectareaOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( origen.getConexionesDisponibles().contains(conexion) );
    }


    @Test
    public void testPuedoConstruirRutaVerConexionOrigen() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getOrigen() == origen );
    }

    @Test
    public void testPuedoConstruirRutaVerConexionDestino() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());

        conexion.construite(origen, destino);

        assertTrue( conexion.getDestino() == destino );
    }


    @Test
    public void testCostoConexionRuta1ha() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 10, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionRuta1haInversa() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 10, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionRuta10ha() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals( 100, conexion.calcularCosto() );
    }


    @Test
    public void testCostoConexionRuta10haInversa() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(MonetizableNull.getInstance());
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());
        conexion.construite(origen, destino);

        assertEquals(100, conexion.calcularCosto());
    }


    @Test
    public void testConstruccionRutaPavimentada10haQuitaDineroJugador() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Conexion conexion = new RutaPavimentada(jugador);
        Hectarea origen = new Hectarea(new Posicion(0,5), SuperficieLlano.getInstance());
        Hectarea destino = new Hectarea(new Posicion(5,0), SuperficieLlano.getInstance());

        final int dineroAntes = jugador.getDinero();

        conexion.construite(origen, destino);

        assertEquals( 100, dineroAntes - jugador.getDinero() );
    }

}
