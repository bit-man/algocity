package algocity.test.zona;

import algocity.modelo.zona.SuperficieLlano;
import org.junit.Test;

import static junit.framework.Assert.*;

public class SuperficieLlanoTest {


    @Test
    public void testConstructorNoNulo() {
        assertNotNull(SuperficieLlano.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(SuperficieLlano.getInstance() == SuperficieLlano.getInstance());
    }


    @Test
    public void testEsAgua() {
        assertFalse(SuperficieLlano.getInstance().esAgua());
    }

    @Test
    public void testEsLlano() {
        assertTrue(SuperficieLlano.getInstance().esLlano());
    }



}
