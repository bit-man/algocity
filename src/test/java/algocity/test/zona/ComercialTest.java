package algocity.test.zona;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.*;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.Comercial;
import algocity.modelo.zona.Construccion;
import algocity.modelo.zona.SuperficieLlano;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Test;

import static junit.framework.Assert.*;
import static junit.framework.Assert.assertEquals;

public class ComercialTest {

    @Test
    public void testConstructorNoNulo() throws LongitudNombreJugadorInsuficienteException {
        assertNotNull(new Comercial(new Jugador("Pepe", null)));
    }

    @Test
    public void testNoEsPozoDeAgua() throws LongitudNombreJugadorInsuficienteException {
        assertFalse(new Comercial(new Jugador("Pepe", null)).esPozoDeAgua());
    }

    @Test
    public void testSinDanio() throws LongitudNombreJugadorInsuficienteException {
        final Comercial comercial = new Comercial(new Jugador("Pepe", null));
        assertEquals((float) 1.0, comercial.getIntegridad());
    }

    //~~-------------------------------------------------------------------------------- esUsable() ?

    @Test
    public void testNoEsHabitableFaltaTensionYCaminos() throws Throwable {
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectareaZona);

        assertFalse( comercial.esUsable() );
    }

    @Test
    public void testNoEsHabitableFaltaTension() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectareaZona);

        Hectarea hectareaOtroExtremo = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        new RutaPavimentada(jugador).construite(hectareaZona, hectareaOtroExtremo);  // Hay camino

        assertFalse(comercial.esUsable());
    }

    @Test
    public void testNoEsHabitableFaltaCamino() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        hectarea.ahoraTieneEnergia(new EnergizadorQueDanergia()); // Hay tensión

        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectarea);

        assertFalse(comercial.esUsable());
    }

    @Test
    public void testEsHabitableSiHayEnergiaYCaminos() throws Throwable {
        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        hectareaZona.ahoraTieneEnergia(new EnergizadorQueDanergia()); // Hay tensión

        Hectarea hectareaOtroExtremo = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        new RutaPavimentada(jugador).construite(hectareaZona, hectareaOtroExtremo);  // Hay camino

        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectareaZona);

        assertTrue(comercial.esUsable());
    }


    //~~-------------------------------------------------------------------------------- energía

    @Test
    public void testSiConstruyoYHayEnergiaSeLaQuitaALaCentral()
            throws Throwable
    {
        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10*10)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(10,10,azar);
        final Jugador jugador = new Jugador("Pepe", mapa);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Construible hectareaCentral = mapa.iterator(0,1).elementoActual();
        final CentralElectrica centralElectrica = new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);
        centralElectrica.construite(hectareaCentral);
        final int capacidadInicial = centralElectrica.getCapacidadRestante();

        Hectarea hectareaZona = mapa.iterator(0,0).elementoActual();
        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectareaZona);

        assertEquals(2_000_000, capacidadInicial - centralElectrica.getCapacidadRestante() );
    }

    @Test
    public void testSiConstruyoYNoHayEnergiaSeLaQuitaALaCentralCuandoLaCentralSeCrea()
            throws Throwable
    {
        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10*10)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(10,10,azar);
        final Jugador jugador = new Jugador("Pepe", mapa);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = mapa.iterator(0,0).elementoActual();
        final Comercial comercial = new Comercial(jugador);
        comercial.construite(hectareaZona);

        Construible hectareaCentral = mapa.iterator(0,1).elementoActual();
        final CentralElectrica centralElectrica = new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);
        final int capacidadInicial = centralElectrica.getCapacidadRestante();
        centralElectrica.construite(hectareaCentral);

        assertEquals(2_000_000, capacidadInicial - centralElectrica.getCapacidadRestante() );
    }

    //~~-------------------------------------------------------------------------------- equals()
    @Test
    public void testEquals()
            throws LongitudNombreJugadorInsuficienteException, FueraDelMapaException, DineroInsuficienteException,
            EnergiaInsuficienteException, JuegoNoIniciadoException, SuperficieInvalidaException
    {

        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Construccion construccion2 = new Comercial(jugador);
        construccion2.construite(hectareaZona);
        Construccion construccion1 = new Comercial(jugador);
        construccion1.construite(hectareaZona);
        assertEquals(construccion1, construccion2);
    }
    @Test
    public void testEqualsDifferentHectarea()
            throws LongitudNombreJugadorInsuficienteException, FueraDelMapaException, DineroInsuficienteException,
            EnergiaInsuficienteException, JuegoNoIniciadoException, SuperficieInvalidaException
    {

        final Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        Hectarea hectareaZona = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Construccion construccion2 = new Comercial(jugador);
        construccion2.construite(hectareaZona);

        Hectarea hectareaZona1 = new Hectarea(new Posicion(0,1), SuperficieLlano.getInstance());
        Construccion construccion1 = new Comercial(jugador);

        construccion1.construite(hectareaZona1);
        assertFalse(construccion1.equals(construccion2));
    }

}
