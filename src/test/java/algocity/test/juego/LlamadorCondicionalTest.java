package algocity.test.juego;

import algocity.modelo.juego.Llamador;
import algocity.modelo.juego.LlamadorCondicional;
import algocity.modelo.juego.ObjetoNulo;
import org.junit.Test;

import static junit.framework.Assert.*;

public class LlamadorCondicionalTest {

    @Test
    public void testNoNulo() {
        assertNotNull( new LlamadorCondicional<>() );
    }

    @Test
    public void testLlamaATodos() {
        Llamador<Object,ObjetoNulo> llamadorCondicional = new LlamadorCondicional<>();

        ObservadorDeNotificacionCondicional observador = new ObservadorDeNotificacionCondicional(Boolean.TRUE);
        llamadorCondicional.registrar(observador);

        ObservadorDeNotificacionCondicional otroObservador = new ObservadorDeNotificacionCondicional(Boolean.TRUE);
        llamadorCondicional.registrar(otroObservador);

        llamadorCondicional.llamar(ObjetoNulo.getInstance());

        assertTrue( observador.notificado() );
        assertTrue( otroObservador.notificado() );
    }

    @Test
    public void testLlamaSoloAlPrimero() {
        Llamador<Object,ObjetoNulo> llamadorCondicional = new LlamadorCondicional<>();

        ObservadorDeNotificacionCondicional observador = new ObservadorDeNotificacionCondicional(Boolean.FALSE);
        llamadorCondicional.registrar(observador);

        ObservadorDeNotificacionCondicional otroObservador = new ObservadorDeNotificacionCondicional(Boolean.TRUE);
        llamadorCondicional.registrar(otroObservador);

        llamadorCondicional.llamar(ObjetoNulo.getInstance());

        assertTrue( observador.notificado() );
        assertFalse(otroObservador.notificado());
    }

    @Test
    public void testDeregistracionDeObservadorEspecifico() {
        Llamador<Object,ObjetoNulo> llamadorCondicional = new LlamadorCondicional<>();

        ObservadorDeNotificacionCondicional observador = new ObservadorDeNotificacionCondicional(Boolean.TRUE);
        llamadorCondicional.registrar(observador);

        ObservadorDeNotificacionCondicional otroObservador = new ObservadorDeNotificacionCondicional(Boolean.TRUE);
        llamadorCondicional.registrar(otroObservador);
        llamadorCondicional.deregistrar(otroObservador);

        llamadorCondicional.llamar(ObjetoNulo.getInstance());

        assertTrue( observador.notificado() );
        assertFalse(otroObservador.notificado());

        observador.limpiarNotificacion();
        llamadorCondicional.deregistrar(observador);
        llamadorCondicional.llamar(ObjetoNulo.getInstance());

        assertFalse(observador.notificado());
        assertFalse(otroObservador.notificado());
    }
}
