package algocity.test.juego;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.MinisterioDeBienestarSocial;
import algocity.modelo.juego.MinisterioDeGanancias;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Mapa;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class MinisterioDeGananciasTest {

    private Mapa mapa;

    @Before
    public void init() {
        FuncionAzar azar = null;
        try {
            azar = new ConstructorAzarSimulado().agregarHectareas(10*10)
                                    .crear();
            this.mapa = new Mapa(10,10, azar);
        } catch (Throwable e) {
            // Parámetros controlados
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( new MinisterioDeGanancias(new MinisterioBlueGanador(mapa)) );
    }

    @Test
    public void testDespuesDe10vecesGana() {
        MinisterioDeGanancias ministerioDeGanancias = new MinisterioDeGanancias(new MinisterioBlueGanador(mapa));

        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );

        // Al final devuelve false para marcar que ya ganó
        assertFalse(ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()));
    }

    @Test
    public void testDespuesDe10vecesGanaYAvisa() {
        MinisterioDeGanancias ministerioDeGanancias = new MinisterioDeGanancias(new MinisterioBlueGanador(mapa));
        ObservadorDeNotificacion observadorDeNotificacion = new ObservadorDeNotificacion();
        ministerioDeGanancias.registrar(observadorDeNotificacion);

        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());

        // Avisa que ya ganó
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertTrue(observadorDeNotificacion.notificado());
    }

    @Test
    public void testDespuesDe10vecesGanaYDespuésNoAvisaMas() {
        MinisterioDeGanancias ministerioDeGanancias = new MinisterioDeGanancias(new MinisterioBlueGanador(mapa));
        ObservadorDeNotificacion observadorDeNotificacion = new ObservadorDeNotificacion();
        ministerioDeGanancias.registrar(observadorDeNotificacion);

        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());

        // Avisa que ya ganó
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        observadorDeNotificacion.limpiarNotificacion();

        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
    }


    @Test
    public void testDespuesDe10vecesSigueSinGanar() {
        MinisterioDeGanancias ministerioDeGanancias = new MinisterioDeGanancias(new MinisterioBluePerdedor(mapa));

        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );
        assertTrue( ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()) );

        // Al final devuelve true para marcar que todavía ganó
        assertTrue(ministerioDeGanancias.trabajar(ObjetoNulo.getInstance()));
    }


    @Test
    public void testDespuesDe10vecesSigueSinGanarYNoAvisa() {
        MinisterioDeGanancias ministerioDeGanancias = new MinisterioDeGanancias(new MinisterioBluePerdedor(mapa));
        ObservadorDeNotificacion observadorDeNotificacion = new ObservadorDeNotificacion();
        ministerioDeGanancias.registrar(observadorDeNotificacion);

        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse( observadorDeNotificacion.notificado() );

        // No avisa porque no ganó
        ministerioDeGanancias.trabajar(ObjetoNulo.getInstance());
        assertFalse(observadorDeNotificacion.notificado());
    }


    private class MinisterioBlueGanador
            extends MinisterioDeBienestarSocial {

        public MinisterioBlueGanador(Mapa mapa) {
            super(mapa);
        }

        public int obtenerBalance() {
            return 1;
        }

        public int obtenerUltimoCenso() {
            return 1;
        }

        public Boolean trabajar(ObjetoNulo parametro) {
            return Boolean.TRUE;
        }
    }

    private class MinisterioBluePerdedor
            extends MinisterioDeBienestarSocial {

        public MinisterioBluePerdedor(Mapa mapa) {
            super(mapa);
        }

        public int obtenerBalance() {
            return -1;
        }

        public Boolean trabajar(ObjetoNulo parametro) {
            return Boolean.TRUE;
        }
    }
}
