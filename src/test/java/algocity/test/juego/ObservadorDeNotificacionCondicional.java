package algocity.test.juego;

public class ObservadorDeNotificacionCondicional
    extends ObservadorDeNotificacion{

    private Boolean valorADevolver;

    public ObservadorDeNotificacionCondicional(Boolean valorADevolver) {
        super();
        this.valorADevolver = valorADevolver;
    }


    @Override
    public Boolean operar(Object parametro) {
        super.operar(parametro);
        return valorADevolver;
    }
}
