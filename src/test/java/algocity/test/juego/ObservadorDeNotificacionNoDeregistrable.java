package algocity.test.juego;

import java.io.Serializable;

public class ObservadorDeNotificacionNoDeregistrable
    extends ObservadorDeNotificacion
    implements Serializable
{

    public Boolean esDeregistrable() {
        return false;
    };
}
