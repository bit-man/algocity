package algocity.test.juego;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorNoDeregistrable;

public class ObservadorDeNotificacionCatastrofe
        extends OperadorNoDeregistrable<Catastrofe>
{

    private boolean notificado;
    private Catastrofe catastrofe;

    public ObservadorDeNotificacionCatastrofe() {
        this.notificado = false;
    }

    public boolean notificado() {
        return this.notificado;
    }

    @Override
    public Boolean operar(Catastrofe parametro) {
       this.catastrofe = parametro;
        this.notificado = true;

        return Boolean.TRUE;
    }

    public Catastrofe getCatastrofe() {
        return catastrofe;
    }

}
