package algocity.test.juego;

import algocity.modelo.juego.*;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.zona.Industrial;
import algocity.modelo.zona.Residencial;
import algocity.modelo.zona.SuperficieLlano;
import org.junit.Test;

import static junit.framework.Assert.*;

public class RecolectorConstruccionesTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new RecolectorConstrucciones());
    }

    @Test
    public void testRecolectaZonaResidencial() throws LongitudNombreJugadorInsuficienteException {
        RecolectorConstrucciones recolectorZonas = new RecolectorConstrucciones();
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Residencial residencial = new Residencial(new Jugador("Pepe", null));
        hectarea.setConstruccion(residencial);
        recolectorZonas.operar( hectarea );

        assertTrue( residencial == recolectorZonas.getConstrucciones().iterator().next() );
    }

    @Test
    public void testRecolectaZonaResidencialNocomercial() throws LongitudNombreJugadorInsuficienteException {
        RecolectorConstrucciones recolectorZonas = new RecolectorConstrucciones();
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Residencial residencial = new Residencial(new Jugador("Pepe", null));
        hectarea.setConstruccion(residencial);
        recolectorZonas.operar( hectarea );

        assertEquals( 0, ColeccionesAlgoCity.filtrar(recolectorZonas.getConstrucciones(), new ComparadorComercios()).size());
    }

    @Test
    public void testRecolectaZonaResidencialNoIndustrial() throws LongitudNombreJugadorInsuficienteException {
        RecolectorConstrucciones recolectorZonas = new RecolectorConstrucciones();
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        Industrial residencial = new Industrial(new Jugador("Pepe", null));
        hectarea.setConstruccion(residencial);
        recolectorZonas.operar( hectarea );

        assertEquals( 0, ColeccionesAlgoCity.filtrar(recolectorZonas.getConstrucciones(), new ComparadorResidencias()).size());
    }

}
