package algocity.test.juego;

import algocity.modelo.juego.ColeccionesAlgoCity;
import algocity.modelo.juego.Filtrable;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static junit.framework.Assert.*;

public class ColeccionesAlgoCityTest {
    @Test
    public void testContieneEstaEnLAColeccion() {
        Collection<Integer> coleccion = new ArrayList<Integer>();
        coleccion.add(10);
        coleccion.add(20);
        coleccion.add(30);

        assertTrue(ColeccionesAlgoCity.contiene(coleccion, new FiltroDeEnteros(10)));
    }

    @Test
    public void testContieneNOEstaEnLaColeccion() {
        Collection<Integer> coleccion = new ArrayList<Integer>();
        coleccion.add(10);
        coleccion.add(20);
        coleccion.add(30);

        assertFalse(ColeccionesAlgoCity.contiene(coleccion, new FiltroDeEnteros(11)));
    }

    @Test
    public void testFiltrarHayElementos() {
        Collection<Integer> coleccionOrigen = new ArrayList<Integer>();
        coleccionOrigen.add(10);
        coleccionOrigen.add(20);
        coleccionOrigen.add(30);

        Collection<Integer> coleccionDestino = ColeccionesAlgoCity.filtrar(coleccionOrigen, new FiltroDeEnteros(10));

        assertEquals( 1, coleccionDestino.size() );
        assertEquals( 10, (int) coleccionDestino.iterator().next() );
    }

    @Test
    public void testFiltrarNoHayElementos() {
        Collection<Integer> coleccionOrigen = new ArrayList<Integer>();
        coleccionOrigen.add(11);
        coleccionOrigen.add(20);
        coleccionOrigen.add(30);

        Collection<Integer> coleccionDestino =
                ColeccionesAlgoCity.filtrar(coleccionOrigen, new FiltroDeEnteros(10));

        assertEquals( 0, coleccionDestino.size() );
    }



    private class FiltroDeEnteros
            implements Filtrable <Integer> {
        private final int elemento;

        public FiltroDeEnteros(int elemento) {
            this.elemento = elemento;
        }

        @Override
        public boolean aceptado(Integer parametro) {
            return parametro.equals(elemento);
        }
    }

}
