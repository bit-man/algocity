package algocity.test.juego;


import algocity.modelo.juego.*;
import algocity.modelo.mapa.Mapa;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class MinisterioDePuntosTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( new MinisterioDePuntos(null,null));
    }


    @Test
    public void testSumaPuntos() throws LongitudNombreJugadorInsuficienteException {

        Jugador jugador = new Jugador("Pepe", null);
        MinisterioDePuntos ministerioDePuntos = new MinisterioDePuntos(jugador, new MinisterioDeBienestarSocialBlue(5, null));
        ministerioDePuntos.trabajar(ObjetoNulo.getInstance());

        assertEquals( 5 * Parametros.MULTIPLICADOR_PUNTAJE, jugador.getPuntaje()  );
    }



    @Test
    public void testNoSumaPuntos() throws LongitudNombreJugadorInsuficienteException {

        Jugador jugador = new Jugador("Pepe", null);
        MinisterioDePuntos ministerioDePuntos = new MinisterioDePuntos(jugador, new MinisterioDeBienestarSocialBlue(-5, null));
        ministerioDePuntos.trabajar(ObjetoNulo.getInstance());

        assertEquals( 0, jugador.getPuntaje()  );
    }


    private class MinisterioDeBienestarSocialBlue
        extends MinisterioDeBienestarSocial{
        private final int balance;

        public MinisterioDeBienestarSocialBlue(int balance, Mapa mapa) {
            super(mapa);
            this.balance = balance;
        }

        public int obtenerBalance() {
            return this.balance;
        }
    }
}
