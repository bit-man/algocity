package algocity.test.juego;

import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.CalculadorRadios;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class CalculadorRadiosTest {


    @Test
    public void testConstructorNoNulo() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int hectareasVerticales = 11;
        final int hectareasHorizontales = 11;
        final FuncionAzar azar =  new ConstructorAzarSimulado()
                            .agregarHectareas(hectareasHorizontales * hectareasVerticales)
                            .todoTierra()
                            .crear();
        Mapa mapa = new Mapa(hectareasHorizontales, hectareasVerticales, azar);
        Hectarea epicentro = mapa.iterator(5, 5).elementoActual();
        assertNotNull( new CalculadorRadios(epicentro, mapa) );
    }

    @Test
    public void testRadio1Centrado() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int hectareasVerticales = 3;
        final int hectareasHorizontales = 3;

        final FuncionAzar azar =  new ConstructorAzarSimulado()
                .agregarHectareas(hectareasHorizontales * hectareasVerticales)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(hectareasHorizontales, hectareasVerticales, azar);
        Hectarea epicentro = mapa.iterator(1, 1).elementoActual();
        assertEquals(1, (int) new CalculadorRadios(epicentro, mapa).calcular());
    }

    @Test
    public void testRadio5Centrado() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int hectareasVerticales = 11;
        final int hectareasHorizontales = 11;
        final FuncionAzar azar =  new ConstructorAzarSimulado()
                .agregarHectareas(hectareasHorizontales * hectareasVerticales)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(hectareasHorizontales, hectareasVerticales, azar);
        Hectarea epicentro = mapa.iterator(5, 5).elementoActual();
        assertEquals(5, (int) new CalculadorRadios(epicentro, mapa).calcular());
    }

    @Test
    public void testRadio9EnUnaEsquina() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int hectareasVerticales = 10;
        final int hectareasHorizontales = 10;
        final FuncionAzar azar =  new ConstructorAzarSimulado()
                .agregarHectareas(hectareasHorizontales * hectareasVerticales)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(hectareasHorizontales, hectareasVerticales, azar);
        Hectarea epicentro = mapa.iterator(0, 0).elementoActual();
        assertEquals(9, (int) new CalculadorRadios(epicentro, mapa).calcular());
    }

    @Test
    public void testRadioSobreUnaArista() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int hectareasVerticales = 10;
        final int hectareasHorizontales = 10;
        final FuncionAzar azar =  new ConstructorAzarSimulado()
                .agregarHectareas(hectareasHorizontales * hectareasVerticales)
                .todoTierra()
                .crear();
        Mapa mapa = new Mapa(hectareasHorizontales, hectareasVerticales, azar);
        Hectarea epicentro = mapa.iterator(3, 0).elementoActual();
        assertEquals(9, (int) new CalculadorRadios(epicentro, mapa).calcular());
    }
}
