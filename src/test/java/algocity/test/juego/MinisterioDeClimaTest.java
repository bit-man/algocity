package algocity.test.juego;

import algocity.modelo.azar.*;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.MinisterioDeClima;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Mapa;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class MinisterioDeClimaTest {

    private Jugador jugador;
    private Mapa mapa;

    @Before
    public void init() {
        try {
            final FuncionAzar azar = new ConstructorAzarSimulado().agregarHectareas(10 * 10).todoTierra().crear();
            mapa = new Mapa(10, 10, azar);
            jugador = new Jugador("Pepe", mapa);
        } catch (Throwable e) {
            // Nombre bien formado y parámetros fijos
        }
    }


    @Test
    public void testConstructorNoNulo() throws AzarSimuladoElementoFueraDeRangoException {
        assertNotNull( new MinisterioDeClima(new AzarSimulado((float) 0.0), mapa));
    }

    @Test
    public void testNoHayCatastrofe() throws AzarSimuladoElementoFueraDeRangoException {
        final MinisterioDeClima ministerioDeClima = new MinisterioDeClima(new AzarSimulado((float) 0.9), mapa);
        final ObservadorDeNotificacionCatastrofe observador = new ObservadorDeNotificacionCatastrofe();
        ministerioDeClima.registrar(observador);

        assertFalse(observador.notificado());
    }

    @Test
    public void testHayCatastrofe() throws AzarSimuladoElementoFueraDeRangoException {
        final MinisterioDeClima ministerioDeClima = new MinisterioDeClima(new AzarSimulado((float) 0.001), mapa);
        final ObservadorDeNotificacionCatastrofe observador = new ObservadorDeNotificacionCatastrofe();
        ministerioDeClima.registrar(observador);
        ministerioDeClima.trabajar(ObjetoNulo.getInstance());

        assertTrue(observador.notificado());
    }

    @Test
    public void testHayGodzilla() throws AzarSimuladoElementoFueraDeRangoException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {
        FuncionAzar azar = new ConstructorAzarSimulado().numero((float) 0.001) // genera catástrofe;
                                                        .numero((float) 0.1)   // es Terremoto
                                                        .crear();
        final MinisterioDeClima ministerioDeClima = new MinisterioDeClima(azar, mapa);
        final ObservadorDeNotificacionCatastrofe observador = new ObservadorDeNotificacionCatastrofe();
        ministerioDeClima.registrar(observador);
        ministerioDeClima.trabajar(ObjetoNulo.getInstance());

        assertTrue(observador.getCatastrofe().esGodzilla());
    }

    @Test
    public void testHayTerremoto() throws AzarSimuladoElementoFueraDeRangoException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {
        FuncionAzar azar = new ConstructorAzarSimulado().numero((float) 0.001) // genera catástrofe;
                                                        .numero((float) 0.8)   // es Godzilla
                                                        .crear();
        final MinisterioDeClima ministerioDeClima = new MinisterioDeClima(azar, mapa);
        final ObservadorDeNotificacionCatastrofe observador = new ObservadorDeNotificacionCatastrofe();
        ministerioDeClima.registrar(observador);
        ministerioDeClima.trabajar(ObjetoNulo.getInstance());

        Assert.assertTrue(observador.getCatastrofe().esTerremoto());
    }
}
