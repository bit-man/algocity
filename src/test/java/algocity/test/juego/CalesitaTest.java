package algocity.test.juego;

import algocity.modelo.juego.Calesita;
import org.junit.Test;

import java.util.Collection;
import java.util.LinkedList;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.*;

public class CalesitaTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( new Calesita());
    }

    @Test
    public void testUnElementoDevuelveSiempreLoMismo() {
        final Calesita<Integer> calesita = new Calesita<Integer>();
        calesita.agregar(10);
        final Integer item = calesita.item();

        calesita.cambiar();

        assertEquals( item, calesita.item() );
    }

    @Test
    public void testAlternaEntreDosElementos() {
        final Calesita<Integer> calesita = new Calesita<Integer>();
        calesita.agregar(10);
        calesita.agregar(11);

        assertEquals(10, (int) calesita.item() );

        calesita.cambiar();
        assertEquals(11, (int) calesita.item() );

        calesita.cambiar();
        assertEquals(10, (int) calesita.item() );
    }

    @Test
    public void testAlternaEntreDosElementosCargadosDeUnaLista() {
        final Collection<Integer> lista = new LinkedList<Integer>();
        lista.add(10);
        lista.add(11);

        final Calesita<Integer> calesita = new Calesita<Integer>();
        calesita.agregar(lista);

        assertEquals(10, (int) calesita.item());

        calesita.cambiar();
        assertEquals(11, (int) calesita.item() );

        calesita.cambiar();
        assertEquals(10, (int) calesita.item() );
    }


    @Test
    public void testDioLaVueltaDosElementos() {
        final Calesita<Integer> calesita = new Calesita<Integer>();
        calesita.agregar(10);
        calesita.agregar(11);

        calesita.cambiar();
        assertFalse(calesita.dioLaVuelta());

        calesita.cambiar();
        assertTrue(calesita.dioLaVuelta());
    }
}
