package algocity.test.juego;

import algocity.modelo.azar.AzarSimulado;
import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.juego.Parametros;
import algocity.modelo.mapa.Mapa;
import org.junit.Test;

import static junit.framework.Assert.*;

public class JugadorTest {

    public static final int IMPUESTO_POR_HABITANTE = 10;

    @Test
    public void testConstructorNoNulo() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        assertNotNull( new Jugador("Pepe", new Mapa(2,2, azarSoloTierra)));
    }

    @Test
    public void testVerNombre() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        assertEquals("Pepe", jugador.getNombre());
    }

    @Test(expected = LongitudNombreJugadorInsuficienteException.class)
    public void testValidarNombreUsuarioMasDe4Chars() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        new Jugador("123", new Mapa(2, 2, azarSoloTierra));
    }

    @Test
    public void testInitTiempoActual() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        assertEquals(0, jugador.getTiempoActual());
    }

    @Test
    public void testavanzarTiempo() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        jugador.avanzarTiempo();
        assertEquals(1, jugador.getTiempoActual());
    }

    @Test
    public void testRecaudacionPorPoblacionNoAumentaDineroPorAjustMinisterioBienestarSocial() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));

        for ( int i = 1; i <= 29; i++) {
            jugador.avanzarTiempo();
        }

        int dinero = jugador.getDinero();
        int poblacion = 10;
        jugador.getMapa().setPoblacion(poblacion);
        jugador.avanzarTiempo();
        assertEquals(dinero, jugador.getDinero());
    }

    @Test
    public void testInitDinero() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        assertEquals(Parametros.DINERO_INICIAL, jugador.getDinero());
    }

    @Test
    public void testAgregarDinero() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        int dinero = jugador.getDinero();
        jugador.agregarDinero(100);
        assertEquals(dinero + 100, jugador.getDinero());
    }

    @Test
    public void testQuitarDinero() throws LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        int dinero = jugador.getDinero();
        jugador.agregarDinero(100);
        jugador.quitarDinero(10);
        assertEquals(dinero + 90, jugador.getDinero());
    }

    @Test(expected = DineroInsuficienteException.class)
    public void testQuitarDineroNoAlcanza() throws LongitudNombreJugadorInsuficienteException, DineroInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final FuncionAzar azarSoloTierra = new AzarSimulado((float) 0.2);
        Jugador jugador = new Jugador("Pepe", new Mapa(2, 2, azarSoloTierra));
        jugador.quitarDinero(Parametros.DINERO_INICIAL + 1);
    }


    //~~--------------------------------------------------------------------------------- Notificación


    @Test
    public void testNotificacionNotifica() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final Mapa mapaSoloTierra = new Mapa(10, 10, new AzarSimulado((float) 0.8));
        final Jugador jugador = new Jugador("Pepe", mapaSoloTierra);

        final ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        jugador.registrarAvanceTiempo(observador);
        jugador.avanzarTiempo();

        assertTrue(observador.notificado());
    }


    @Test
    public void testNotificacionDeregistraUnDeregistrable() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final Mapa mapaSoloTierra = new Mapa(10, 10, new AzarSimulado((float) 0.8));
        final Jugador jugador = new Jugador("Pepe", mapaSoloTierra);

        final ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        jugador.registrarAvanceTiempo(observador);
        jugador.avanzarTiempo();
        observador.limpiarNotificacion();

        assertEquals(1, jugador.deregistrarAvanceDeTiempo().size() );
    }

    @Test
    public void testNotificacionDeregistraEsteDeregistrable() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final Mapa mapaSoloTierra = new Mapa(10, 10, new AzarSimulado((float) 0.8));
        final Jugador jugador = new Jugador("Pepe", mapaSoloTierra);

        final ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        jugador.registrarAvanceTiempo(observador);
        jugador.avanzarTiempo();
        observador.limpiarNotificacion();

        assertTrue(observador == jugador.deregistrarAvanceDeTiempo().iterator().next());
    }

    @Test
    public void testNotificacionNoLlamaAObservadorDeregistrado() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final Mapa mapaSoloTierra = new Mapa(10, 10, new AzarSimulado((float) 0.8));
        final Jugador jugador = new Jugador("Pepe", mapaSoloTierra);

        final ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        jugador.registrarAvanceTiempo(observador);
        jugador.avanzarTiempo();

        jugador.deregistrarAvanceDeTiempo();
        observador.limpiarNotificacion();
        jugador.avanzarTiempo();

        assertFalse( observador.notificado() );
    }

    @Test
    public void testNotificacionDeregistraLlamaSoloAlNoDeregistrable() throws LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
        final Mapa mapaSoloTierra = new Mapa(10, 10, new AzarSimulado((float) 0.8));
        final Jugador jugador = new Jugador("Pepe", mapaSoloTierra);

        final ObservadorDeNotificacion observadorDeregistrable = new ObservadorDeNotificacion();
        final ObservadorDeNotificacion observadorNoDeregistrable = new ObservadorDeNotificacionNoDeregistrable();
        jugador.registrarAvanceTiempo(observadorDeregistrable);
        jugador.registrarAvanceTiempo(observadorNoDeregistrable);
        jugador.deregistrarAvanceDeTiempo();
        jugador.avanzarTiempo();

        assertTrue( observadorNoDeregistrable.notificado() );
        assertFalse( observadorDeregistrable.notificado() );

    }

}
