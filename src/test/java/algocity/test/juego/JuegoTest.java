package algocity.test.juego;

import algocity.modelo.almacenamiento.*;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.energia.CentralNuclear;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.Dimension;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.TuberiaDeAgua;
import algocity.modelo.zona.*;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Collection;

import static junit.framework.Assert.*;

public class JuegoTest {

    public static final int DINERO_AGREGADO = 20;
    private Persistible directorioConfiguracion;

    @Before
    public void before() {
        try
        {
            this.directorioConfiguracion = new AlmacenamientoPersistente(
                    Files.createTempDirectory(null).toFile()
            );
            directorioConfiguracion.inicializar();
        } catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testConstructorNoNulo() throws DirectorioDeConfiguracionNoInicializadoException
    {
        assertNotNull(new Juego(directorioConfiguracion));
    }

    @Test
    public void testSinUsuariosIniciales() throws DirectorioDeConfiguracionNoInicializadoException, ErrorAlObtenerPuntajes {
        Juego juego = new Juego(directorioConfiguracion);
        assertEquals(0, juego.puntajes().keySet().size());
    }

    @Test(expected = JuegoNoIniciadoException.class)
    public void testSinJugadorActualInicial() throws JuegoNoIniciadoException,
            DirectorioDeConfiguracionNoInicializadoException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.getJugadorActual();
    }

    //~~--------------------------------------------------------------------------------- Inicio de Partida

    @Test
    public void testPartidaIniciadaNombreJugador() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        assertEquals("Pepe", juego.getJugadorActual().getNombre());
    }

    @Test(expected = JugadorInexistenteException.class)
    public void testIniciarPartidaJugadorInexistente() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Juan");
    }

    @Test
    public void testIniciarPartidaJugadorInexistenteNombre() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        try {
            juego.iniciarPartida("Juan");
        } catch (JugadorInexistenteException e) {
            assertEquals("Juan", e.getNombreUsuario());
        }
    }

    @Test
    public void testIniciarPartidaPoneTodoEnCero() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        assertEquals(0, jugadorActual.getPuntaje());
        assertEquals(Parametros.DINERO_INICIAL, jugadorActual.getDinero());
        assertEquals(0, jugadorActual.getTiempoActual());
    }

    @Test
    public void testReiniciarPartidaPoneTodoEnCero()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
            ErrorAlCrearJugadorException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();
        juego.continuarPartida("Pepe");
        juego.finJugadorActual();
        juego.iniciarPartida("Pepe");

        jugadorActual = juego.getJugadorActual();
        assertEquals(Parametros.PUNTAJE_INICIAL_JUGADOR, jugadorActual.getPuntaje());
        assertEquals(Parametros.DINERO_INICIAL, jugadorActual.getDinero());
        assertEquals(0, jugadorActual.getTiempoActual());
    }

    @Test
    public void testReiniciarPartidaPoneEnCeroPuntaje()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException,
            JugadorExistenteException, JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
             ErrorAlCrearJugadorException, ErrorAlObtenerPuntajes

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();
        juego.continuarPartida("Pepe");
        juego.finJugadorActual();
        juego.iniciarPartida("Pepe");

        assertEquals(Parametros.PUNTAJE_INICIAL_JUGADOR, (int) juego.puntajes().get("Pepe"));
    }

    //~~--------------------------------------------------------------------------------- Fin jugador actual


    @Test(expected = JuegoNoIniciadoException.class)
    public void testFinJugadorActualNoHayJuegoIniciado() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorInexistenteException, JugadorExistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.finJugadorActual();
        juego.getJugadorActual();
    }

    @Test
    public void testFinJugadorActualNoActualizaPuntos()
            throws DirectorioDeConfiguracionNoInicializadoException, JugadorExistenteException,
            JugadorInexistenteException,  ErrorAlCrearJugadorException,
            LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException, ErrorAlGuardarJugadorException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        // Estos puntos deben perderse
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.iniciarPartida("Pepe");
        juego.finJugadorActual();

        assertEquals(Parametros.PUNTAJE_INICIAL_JUGADOR, (int) juego.puntajes().get("Pepe"));
    }

    //~~--------------------------------------------------------------------------------- Continuar Partida

    @Test
    public void testContinuarPartidaJugadorInexistente() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.finJugadorActual();
        try {
            juego.continuarPartida("Juan");
            fail("No se puede continuar la partida de un jugador inexistente");
        } catch (JugadorInexistenteException e) {
            // OK, debe fallar
        }
    }

    @Test
    public void testContinuarPartidaJugadorInexistenteNoCreaPuntaje()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException,
            JugadorExistenteException, JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
             ErrorAlCrearJugadorException, ErrorAlObtenerPuntajes

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.finJugadorActual();
        try
        {
            juego.continuarPartida("Juan");
        } catch (JugadorInexistenteException e)
        {
            // OK, debe fallar
        }

        assertEquals(1, juego.puntajes().keySet().size());
    }

    @Test
    public void testContinuarPartidaSinGuardarPoneTodoEnCero() throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        int dinero = jugadorActual.getDinero();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();

        // Los valores a obtener son los del inicio del juego (última vez que el Jugador fue guardado)
        juego.continuarPartida("Pepe");
        jugadorActual = juego.getJugadorActual();
        assertEquals(0, jugadorActual.getTiempoActual());
        assertEquals(dinero, jugadorActual.getDinero());
    }


    @Test
    public void testContinuarPartidaSinGuardarDejaEnCeroPuntajes()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException,
            JugadorExistenteException, JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
             ErrorAlCrearJugadorException, ErrorAlObtenerPuntajes

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        int dinero = jugadorActual.getDinero();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();

        // Los valores a obtener son los del inicio del juego (última vez que el Jugador fue guardado)
        juego.continuarPartida("Pepe");

        assertEquals( Parametros.PUNTAJE_INICIAL_JUGADOR, (int) juego.puntajes().get("Pepe") );
    }

    @Test
    public void testContinuarPartidaGuardandoArrancaDesdeUltimoJuegoGuardado()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
            ErrorAlCrearJugadorException, ErrorAlGuardarJugadorException, FileNotFoundException,
            GuardarPuntajesException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        int dinero = jugadorActual.getDinero();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();

        // Los valores a obtener son los que fueron guardados aquí
        juego.guardarJuego();

        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        jugadorActual = juego.getJugadorActual();
        assertEquals(1, jugadorActual.getTiempoActual());
        assertEquals(dinero + DINERO_AGREGADO, jugadorActual.getDinero());
    }

    @Test
    public void testContinuarPartidaGuardandoPuntajeDesdeUltimoJuegoGuardado()
            throws JuegoNoIniciadoException, LongitudNombreJugadorInsuficienteException,
            JugadorExistenteException, JugadorInexistenteException, DirectorioDeConfiguracionNoInicializadoException,
             ErrorAlCrearJugadorException, ErrorAlGuardarJugadorException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException


    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        Jugador jugadorActual = juego.getJugadorActual();
        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();
        int puntajeAntesDeGuardar = jugadorActual.getPuntaje();

        // Los valores a obtener son los que fueron guardados aquí
        juego.guardarJuego();

        jugadorActual.agregarDinero(DINERO_AGREGADO);
        jugadorActual.avanzarTiempo();

        juego.finJugadorActual();

        juego.continuarPartida("Pepe");


        assertEquals(puntajeAntesDeGuardar, (int) juego.puntajes().get("Pepe"));
    }


    /**
     * Este test muestra que una vez finalizado el juego el observador deregistrable se desvincula
     * completamente del juego y habrá que volver a registrarlo una vez reiniciada la partida
     * @throws Throwable
     */
    @Test
    public void testContnuarPartidaDeregistrableNOVuelveANotificarElMismoObjetoNotificador()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        juego.getJugadorActual().registrarAvanceTiempo(observador);
        juego.getJugadorActual().avanzarTiempo();
        assertTrue(observador.notificado());

        juego.guardarJuego();
        juego.finJugadorActual();
        juego.continuarPartida("Pepe");

        observador.limpiarNotificacion();
        juego.getJugadorActual().avanzarTiempo();
        assertFalse(observador.notificado());
    }

    @Test
    public void testContnuarPartidaNoDeregistrableContinuaLlamandoAlObservador()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        ObservadorDeNotificacion observador = new ObservadorDeNotificacionNoDeregistrable();
        juego.getJugadorActual().registrarAvanceTiempo(observador);
        juego.getJugadorActual().avanzarTiempo();
        assertTrue(observador.notificado());

        juego.guardarJuego();
        juego.finJugadorActual();
        juego.continuarPartida("Pepe");

        observador.limpiarNotificacion();
        juego.getJugadorActual().registrarAvisoPerdedor( observador );

        /**
         * Para este test sabemos que se registra el MinisterioDePerdidas para que sea llamado por DespertarAlMinisterio
         * que a su vez se registra como observador no deregistrable, entonces si avanzamos
         * el tiempo sin realizar niguna acción el jugador debe perder.
         * Registrándonos al aviso de perdedor podemos ver que el MinisterioDePerdidas segue funcionando
         * aún después de la deserialización
         */
        for ( int i = 0; i < Parametros.PERDEDOR_CUENTA_CONSECUTIVA_SIN_POBLACION; i++) {
            juego.getJugadorActual().avanzarTiempo();
        }

        assertTrue(observador.notificado());
    }

    //~~--------------------------------------------------------------------------------- Agregar jugador

    @Test
    public void testAgregar1JugadorYHay1Puntaje() throws LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlObtenerPuntajes, ErrorAlCrearJugadorException, JugadorInexistenteException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        assertEquals(1, juego.puntajes().keySet().size());
    }

    @Test
    public void testAgregar2JugadoresYHay2Puntajes() throws LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlObtenerPuntajes, ErrorAlCrearJugadorException, JugadorInexistenteException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.agregarJugador("Juan");
        assertEquals(2, juego.puntajes().keySet().size());
    }

    @Test
    public void testAgregarJugadorRepetidoFalla() throws LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlCrearJugadorException, JugadorInexistenteException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");

        try {
            juego.agregarJugador("Pepe");
            fail("El nombre del Jugador está repetido");
        }
        catch( JugadorExistenteException e) {
            // OK, debe tirar esta excepción
        }

    }

    @Test
    public void testAgregarJugadorRepetidoNoLoAgregaALosPuntajes() throws LongitudNombreJugadorInsuficienteException, JugadorExistenteException,
            DirectorioDeConfiguracionNoInicializadoException,  ErrorAlObtenerPuntajes, ErrorAlCrearJugadorException, JugadorInexistenteException

    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");

        try {
            juego.agregarJugador("Pepe");
            fail("El nombre del Jugador está repetido");
        }
        catch( JugadorExistenteException e) {
            // OK, debe tirar esta excepción
        }

        assertEquals(1, juego.puntajes().keySet().size());
        assertTrue(juego.puntajes().keySet().contains("Pepe"));
    }

    //~~--------------------------------------------------------------------------------- Guardar Juego

    @Test( expected = JuegoNoIniciadoException.class)
    public void testGuardarJuegoFallaSinHaberIniciadoPartida()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JuegoNoIniciadoException, FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.guardarJuego();
    }

    @Test
    public void testGuardarJuegoGuardaPuntajeEnUsuario()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(100, juego.getJugadorActual().getPuntaje());
    }

    @Test
    public void testGuardarJuegoGuardaPuntajeEnPuntajes()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(100, (int) juego.puntajes().get("Pepe"));
    }


    @Test
    public void testGuardarJuegoGuardaOtrosDatosEnUsuario()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugadorActual = juego.getJugadorActual();
        jugadorActual.avanzarTiempo();
        jugadorActual.avanzarTiempo();
        int tiempoGuardado = jugadorActual.getTiempoActual();

        jugadorActual.agregarDinero(200);
        int dineroGuardado = jugadorActual.getDinero();

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(dineroGuardado, juego.getJugadorActual().getDinero());
        assertEquals(tiempoGuardado, juego.getJugadorActual().getTiempoActual());
    }


    @Test
    public void testGuardarJuegoGuardaVariasVecesCreaUnaSolaEntradaEnPuntajes()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(1, juego.puntajes().keySet().size());
    }

    @Test
    public void testGuardarJuegoGuardaVariasVecesGuardaSoloLosUltimosCambiosEnPuntaje()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(200, (int) juego.puntajes().get("Pepe"));
    }

    @Test
    public void testGuardarJuegoGuardaVariasVecesGuardaSoloLosUltimosCambiosEnUsuario()
            throws ErrorAlGuardarJugadorException, DirectorioDeConfiguracionNoInicializadoException,
            JugadorExistenteException, JugadorInexistenteException, 
            ErrorAlCrearJugadorException, LongitudNombreJugadorInsuficienteException, JuegoNoIniciadoException,
            ErrorAlObtenerPuntajes, FileNotFoundException, GuardarPuntajesException
    {
        Juego juego = new Juego(directorioConfiguracion);
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.getJugadorActual().agregarPuntaje(100);
        juego.guardarJuego();

        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        assertEquals(200, juego.getJugadorActual().getPuntaje());
    }

    @Test
    public void testGuardarMapaTodoTierra()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        Mapa mapa = juego.getJugadorActual().getMapa();
        final Dimension dimensiones = mapa.getDimensiones();

        for ( int x = 0; x < dimensiones.getX(); x++) {
            for ( int y = 0; y < dimensiones.getY(); y++) {
                assertEquals(SuperficieLlano.getInstance(), mapa.iterator(x,y).elementoActual().getSuperficie() );
            }
        }
    }

    @Test
    public void testGuardarMapaTodoAgua()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoAgua()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        Mapa mapa = juego.getJugadorActual().getMapa();
        final Dimension dimensiones = mapa.getDimensiones();

        for ( int x = 0; x < dimensiones.getX(); x++) {
            for ( int y = 0; y < dimensiones.getY(); y++) {
                assertEquals(SuperficieAgua.getInstance(), mapa.iterator(x,y).elementoActual().getSuperficie() );
            }
        }
    }

    @Test
    public void testGuardarMapaSinCambioCompararHectareas()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        final Mapa mapaOriginal = juego.getJugadorActual().getMapa();
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        Mapa mapa = juego.getJugadorActual().getMapa();
        final Dimension dimensiones = mapa.getDimensiones();

        for ( int x = 0; x < dimensiones.getX(); x++) {
            for ( int y = 0; y < dimensiones.getY(); y++) {
                assertEquals(mapaOriginal.iterator(x,y).elementoActual(), mapa.iterator(x, y).elementoActual());
            }
        }
    }

    @Test
    public void testGuardarComercial()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea = mapa.iterator(0, 0).elementoActual();
        Construccion construccionOriginal = new Comercial(jugador);
        construccionOriginal.construite(hectarea);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarIndustrial()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea = mapa.iterator(0, 0).elementoActual();
        Construccion construccionOriginal = new Industrial(jugador);
        construccionOriginal.construite(hectarea);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarResidencial()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea = mapa.iterator(0, 0).elementoActual();
        Construccion construccionOriginal = new Residencial(jugador);
        construccionOriginal.construite(hectarea);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarEstacionDeBomberos()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea = mapa.iterator(0, 0).elementoActual();
        Construccion construccionOriginal = new EstacionDeBomberos(jugador);
        construccionOriginal.construite(hectarea);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }


    @Test
    public void testGuardarCentralEolica()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();

        final CentralElectrica construccionOriginal = new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);
        construccionOriginal.construite(hectarea1);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarCentralNuclear()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();

        final CentralElectrica construccionOriginal = new CentralElectrica(CentralNuclear.getInstance(), mapa, jugador);
        construccionOriginal.construite(hectarea1);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarPozoDeAgua()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();
        hectarea1.setSuperficie(SuperficieAgua.getInstance());
        final Construccion construccionOriginal = new PozoDeAgua(jugador);
        construccionOriginal.construite(hectarea1);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarCentralMinearal()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();

        final CentralElectrica construccionOriginal = new CentralElectrica(CentralMineral.getInstance(), mapa, jugador);
        construccionOriginal.construite(hectarea1);

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Construccion construccionDeserializada = mapa.iterator(0, 0).elementoActual().getConstruccion();
        assertEquals( construccionOriginal, construccionDeserializada);
    }

    @Test
    public void testGuardarRutaPavimentada()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();
        Hectarea hectarea2 = mapa.iterator(0, 1).elementoActual();

        Conexion conexionOriginal = new RutaPavimentada(jugador);
        conexionOriginal.construite(hectarea1, hectarea2);
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Collection<Conexion> conexionesDisponibles = mapa.iterator(0, 0).elementoActual().getConexionesDisponibles();
        Conexion conexionDeserializada = conexionesDisponibles.iterator().next();
        assertEquals( conexionOriginal, conexionDeserializada);
        assertEquals( 1, conexionesDisponibles.size() );
    }

    @Test
    public void testGuardarLineaDeTension()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();
        Hectarea hectarea2 = mapa.iterator(0, 1).elementoActual();

        new CentralElectrica(CentralEolica.getInstance(),mapa,jugador).construite(hectarea1);
        Conexion conexionOriginal = new LineaDeTension(jugador);
        conexionOriginal.construite(hectarea1, hectarea2);
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Collection<Conexion> conexionesDisponibles = mapa.iterator(0, 0).elementoActual().getConexionesDisponibles();
        Conexion conexionDeserializada = conexionesDisponibles.iterator().next();
        assertEquals( conexionOriginal, conexionDeserializada);
        assertEquals( 1, conexionesDisponibles.size() );
    }

    @Test
    public void testGuardarTuberiaDeAgua()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");
        Jugador jugador = juego.getJugadorActual();
        Mapa mapa = jugador.getMapa();

        Hectarea hectarea1 = mapa.iterator(0, 0).elementoActual();
        hectarea1.setSuperficie(SuperficieAgua.getInstance());
        Hectarea hectarea2 = mapa.iterator(0, 1).elementoActual();

        Conexion conexionOriginal = new TuberiaDeAgua(jugador);
        conexionOriginal.construite(hectarea1, hectarea2);
        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        mapa = juego.getJugadorActual().getMapa();
        final Collection<Conexion> conexionesDisponibles = mapa.iterator(0, 0).elementoActual().getConexionesDisponibles();
        Conexion conexionDeserializada = conexionesDisponibles.iterator().next();
        assertEquals( conexionOriginal, conexionDeserializada);
        assertEquals( 1, conexionesDisponibles.size() );
    }

    @Test
    public void testGuardarDeregistrableVuelveARegistrarElMismoNotificador()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        /**
         * * Observador desregistrable
         *  - se desregitra automáticamente cada vez que se guarda y se vuelve a registrar inmediatamente
         *  - en el fin del jueo se desregistra y no se vuelve a registrar
         *  * Observador no deregistrable :
         *  - se serializa con el jugador (debe ser Serializable)
         */
        ObservadorDeNotificacion observador = new ObservadorDeNotificacion();
        juego.getJugadorActual().registrarAvanceTiempo(observador);
        juego.getJugadorActual().avanzarTiempo();
        assertTrue(observador.notificado());

        juego.guardarJuego();

        observador.limpiarNotificacion();
        juego.getJugadorActual().avanzarTiempo();
        assertTrue( observador.notificado() );

    }

    @Test
    public void testGuardarNoDeregistrablesSigueNotificandoDespuesDeGuardar()
            throws Throwable
    {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        /**
         * * Observador desregistrable
         *  - se desregitra automáticamente cada vez que se guarda y se vuelve a registrar inmediatamente
         *  - en el fin del jueo se desregistra y no se vuelve a registrar
         *  * Observador no deregistrable :
         *  - se serializa con el jugador (debe ser Serializable)
         */
        ObservadorDeNotificacion observador = new ObservadorDeNotificacionNoDeregistrable();
        juego.getJugadorActual().registrarAvanceTiempo(observador);
        juego.getJugadorActual().avanzarTiempo();
        assertTrue(observador.notificado());

        juego.guardarJuego();

        observador.limpiarNotificacion();
        juego.getJugadorActual().avanzarTiempo();
        assertTrue(observador.notificado());

    }

    @Test
    public void testPerderGuardarYReanudarNoAvanzaTiempo()
            throws Throwable
    {
        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(Parametros.HECTAREAS_HORIZONTALES * Parametros.HECTAREAS_VERTICALES)
                .todoTierra()
                .crear();
        Juego juego = new Juego(directorioConfiguracion, new FabricaMapaAzar(azar));
        juego.agregarJugador("Pepe");
        juego.iniciarPartida("Pepe");

        final ObservadorDeNotificacion observadorDeNotificacion = new ObservadorDeNotificacion();
        juego.getJugadorActual().registrarAvisoPerdedor(observadorDeNotificacion);

        for ( int i = 0; i < Parametros.PERDEDOR_CUENTA_CONSECUTIVA_SIN_POBLACION; i++) {
            juego.getJugadorActual().avanzarTiempo();
        }

        // Se notifica que el jugador perdió
        assertTrue(observadorDeNotificacion.notificado());

        juego.guardarJuego();
        juego.finJugadorActual();

        juego.continuarPartida("Pepe");
        final ObservadorDeNotificacion observadorTiempo = new ObservadorDeNotificacion();
        juego.getJugadorActual().registrarAvanceTiempo(observadorTiempo);

        juego.getJugadorActual().avanzarTiempo();

        assertFalse(observadorTiempo.notificado());

    }
}
