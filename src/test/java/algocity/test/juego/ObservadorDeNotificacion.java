package algocity.test.juego;

import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;

public class ObservadorDeNotificacion
        extends OperadorDeregistrable<Object>
        implements Operador<Object>
{

    private boolean notificado;

    public ObservadorDeNotificacion() {
        this.notificado = false;
    }

    public boolean notificado() {
        return this.notificado;
    }

    @Override
    public Boolean operar(Object parametro) {
        notificado = true;

        return Boolean.TRUE;
    }

    public void limpiarNotificacion() {
        notificado = false;
    }
}