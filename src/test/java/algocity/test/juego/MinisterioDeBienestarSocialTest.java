package algocity.test.juego;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.*;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

public class MinisterioDeBienestarSocialTest {

    private static final int HECTAREAS_HORIZONTALES = 10;
    private static final int HECTAREAS_VERTICALES = 10;

    private Mapa mapa;
    private Jugador jugador;

    @Before
    public void init()  {

        try {
            FuncionAzar azar = new ConstructorAzarSimulado().
                    agregarHectareas(HECTAREAS_HORIZONTALES*HECTAREAS_VERTICALES).todoTierra().crear();
            mapa = new Mapa(HECTAREAS_HORIZONTALES, HECTAREAS_VERTICALES, azar);
            jugador = new Jugador("Pepe", mapa);
            jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        } catch (Throwable e) {
            // Parámetros validados
        }


    }
    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new MinisterioDeBienestarSocial(null));
    }

    @Test
    public void testNoFallaSiEstaVacioElMapa() {

        mapa.hectareaAlAzarLibre();

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

    }

    @Test
    public void testSiNoHayNadaLaGenteSeVa() throws LongitudNombreJugadorInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException {


        final Residencial residencial = (Residencial) construirResidencialSinServicios(jugador, 0, 0).getConstruccion();

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        final int genteInicial = residencial.getCantidadDeGenteAlojada();

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

        assertTrue(residencial.getCantidadDeGenteAlojada() < genteInicial);
    }


    @Test
    public void testSiHayIndustriasPeroSinServiciosLaGenteSeVa() throws LongitudNombreJugadorInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException {
        final Residencial residencial = (Residencial) construirResidencialSinServicios( jugador, 0, 0).getConstruccion();

        establecerIndustriaSinServicios(jugador, 0, 1);
        establecerIndustriaSinServicios(jugador, 0, 2);
        establecerIndustriaSinServicios(jugador, 0, 3);
        establecerIndustriaSinServicios(jugador, 0, 4);
        establecerIndustriaSinServicios(jugador, 0, 5);

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        final int genteInicial = residencial.getCantidadDeGenteAlojada();

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

        assertTrue(residencial.getCantidadDeGenteAlojada() < genteInicial);
    }


    @Test
    public void testSiHayIndustriasPeroSoloConAguaLaGenteSeVa() throws LongitudNombreJugadorInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {

        final Residencial residencial = (Residencial) construirResidencialSinServicios( jugador, 0, 0).getConstruccion();

        establecerIndustriaConRuta( new Posicion(0,1), new Posicion(0,2) );
        establecerIndustriaConRuta( new Posicion(0,3), new Posicion(0,4) );
        establecerIndustriaConRuta( new Posicion(0,5), new Posicion(0,6) );
        establecerIndustriaConRuta( new Posicion(0,7), new Posicion(0,8) );
        establecerIndustriaConRuta( new Posicion(0,9), new Posicion(1,0) );

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        final int genteInicial = residencial.getCantidadDeGenteAlojada();

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

        assertTrue(residencial.getCantidadDeGenteAlojada() < genteInicial);
    }

    @Test
    public void testSiHayIndustriasFullPeroResidencialSinServicioLaGenteSeVa() throws LongitudNombreJugadorInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {

        final Residencial residencial = (Residencial) construirResidencialSinServicios( jugador, 0, 0).getConstruccion();

        establecerIndustriaFull(new Posicion(0,1), new Posicion(0,2), new Posicion(0,3));
        establecerIndustriaFull(new Posicion(0,4), new Posicion(0,5), new Posicion(0,6));
        establecerIndustriaFull(new Posicion(0,7), new Posicion(0,8), new Posicion(0,9));
        establecerIndustriaFull(new Posicion(1,0), new Posicion(1,1), new Posicion(1,2));
        establecerIndustriaFull(new Posicion(1,3), new Posicion(1,4), new Posicion(1,5));

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        final int genteInicial = residencial.getCantidadDeGenteAlojada();

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

        assertTrue(residencial.getCantidadDeGenteAlojada() < genteInicial);
    }

    @Test
    public void testSiHayIndustriasFullYResidencialFullLaGenteViene() throws LongitudNombreJugadorInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, ConstruccionInvalidaExcepcion, JuegoNoIniciadoException, FueraDelMapaException {

        final Residencial residencial = (Residencial) construirResidencialFull(new Posicion(0,0), new Posicion(0,1)).getConstruccion();

        establecerIndustriaFull(new Posicion(0,2), new Posicion(0,3), new Posicion(0,4));
        establecerIndustriaFull(new Posicion(0,5), new Posicion(0,6), new Posicion(0,7));
        establecerIndustriaFull(new Posicion(0,8), new Posicion(0,9), new Posicion(1,0));
        establecerIndustriaFull(new Posicion(1,1), new Posicion(1,2), new Posicion(1,3));
        establecerIndustriaFull(new Posicion(1,4), new Posicion(1,5), new Posicion(1,6));

        MinisterioDeBienestarSocial ministerioDeBienestarSocial = new MinisterioDeBienestarSocial(mapa);

        final int genteInicial = residencial.getCantidadDeGenteAlojada();

        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());
        ministerioDeBienestarSocial.trabajar(ObjetoNulo.getInstance());

        assertTrue(residencial.getCantidadDeGenteAlojada() > genteInicial);
    }

    private Hectarea establecerIndustriaConRuta(Posicion posicionIndustria, Posicion otroExtremoDeLaRuta ) throws EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, FueraDelMapaException {
        Hectarea hectarea = mapa.iterator(otroExtremoDeLaRuta).elementoActual();
        hectarea.setSuperficie(SuperficieLlano.getInstance());


        Hectarea hectareaIndustria = establecerIndustriaSinServicios(jugador, posicionIndustria.getX(), posicionIndustria.getY());

        new RutaPavimentada(jugador).construite(hectarea, hectareaIndustria);

        return hectareaIndustria;
    }

    private void establecerIndustriaFull(Posicion posicionCentral, Posicion posicionIndustria, Posicion posicionOtroExtremoRuta) throws EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion, FueraDelMapaException {
        Hectarea hectareaCentral = mapa.iterator(posicionCentral).elementoActual();
        hectareaCentral.setSuperficie(SuperficieLlano.getInstance());
        new CentralElectrica(CentralEolica.getInstance(),mapa,jugador).construite(hectareaCentral);

        Hectarea hectareaIndustria = establecerIndustriaConRuta(posicionIndustria, posicionOtroExtremoRuta);

        new LineaDeTension(jugador).construite(hectareaCentral, hectareaIndustria);
    }

    private Hectarea construirResidencialSinServicios(Jugador jugador, int x, int y) throws EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException {
        Hectarea hectareaConstruccion = mapa.iterator(x, y).elementoActual();
        hectareaConstruccion.setSuperficie(SuperficieLlano.getInstance());
        Residencial construccion = new Residencial(jugador);
        construccion.construite(hectareaConstruccion);

        return hectareaConstruccion;
    }

    private Hectarea construirResidencialFull(Posicion posicionCentral, Posicion posicionResidencial) throws EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException, FueraDelMapaException, JuegoNoIniciadoException, ConstruccionInvalidaExcepcion {
        Hectarea hectareaCentral = mapa.iterator(posicionCentral).elementoActual();
        hectareaCentral.setSuperficie(SuperficieLlano.getInstance());
        new CentralElectrica(CentralEolica.getInstance(),mapa,jugador).construite(hectareaCentral);

        Hectarea hectarea = mapa.iterator(posicionResidencial).elementoActual();
        hectarea.setSuperficie(SuperficieLlano.getInstance());

        final Hectarea hectareaResidencial = construirResidencialSinServicios(jugador,
                                                    posicionResidencial.getX(), posicionResidencial.getY());

        new RutaPavimentada(jugador).construite(hectarea, hectareaResidencial);
        new LineaDeTension(jugador).construite(hectareaCentral, hectareaResidencial);
        return hectareaResidencial;
    }


    private Hectarea establecerIndustriaSinServicios(Jugador jugador, int x, int y) throws EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException {
        Hectarea hectareaIndustria = mapa.iterator(x,y).elementoActual();
        hectareaIndustria.setSuperficie(SuperficieLlano.getInstance());
        Industrial industria = new Industrial(jugador);
        industria.construite(hectareaIndustria);

        return hectareaIndustria;
    }

    // agregar tension y agua y la gente viene
    // lo mismo para residencial

}
