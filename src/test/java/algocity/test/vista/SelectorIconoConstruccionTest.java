package algocity.test.vista;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.mapa.Construible;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.zona.*;
import algocity.vista.Inicial;
import algocity.vista.SelectorIconoConstruccion;
import org.junit.Test;

import java.awt.*;

import static junit.framework.Assert.*;

public class SelectorIconoConstruccionTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( new SelectorIconoConstruccion(null) );
    }

    @Test
    public void testComercialAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(new Comercial(null));
        Construccion construccion = new Comercial(null);

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(construccion, null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }

    @Test
    public void testIndustrialAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(new Industrial(null));

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Industrial(null), null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }

    @Test
    public void testResidencialAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(new Residencial(null));

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Residencial(null), null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }


    @Test
    public void testCentralElectricaAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion =
                new SelectorIconoConstruccion(new CentralElectrica(CentralMineral.getInstance(),null,null));

        Inicial<Construccion, Image> inicial =
                new Inicial<Construccion, Image>(new CentralElectrica(CentralMineral.getInstance(),null,null), null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }


    @Test
    public void testEstacionDeBomberosAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion =
                new SelectorIconoConstruccion(new EstacionDeBomberos(null));

        Inicial<Construccion, Image> inicial =
                new Inicial<Construccion, Image>(new EstacionDeBomberos(null), null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }


    @Test
    public void testPozoDeAguaAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion =
                new SelectorIconoConstruccion(new PozoDeAgua(null));

        Inicial<Construccion, Image> inicial =
                new Inicial<Construccion, Image>(new PozoDeAgua(null), null);
        assertTrue(selectorIconoConstruccion.aceptado(inicial));
    }


    @Test
    public void testResidencialConComercialNoAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(new Comercial(null));

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Residencial(null), null);
        assertFalse(selectorIconoConstruccion.aceptado(inicial));
    }

    @Test
    public void testCentralElectricaConComercialNoAceptado() {
        SelectorIconoConstruccion selectorIconoConstruccion =
                new SelectorIconoConstruccion(new CentralElectrica(CentralEolica.getInstance(), null, null));

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Residencial(null), null);
        assertFalse(selectorIconoConstruccion.aceptado(inicial));
    }


    @Test
    public void testResidencialConSinConstruccionSobreTierraNoAceptado() {
        SinConstruccion sinConstruccion = new SinConstruccion();
        Construible hectarea = new Hectarea(new Posicion(0,0), SuperficieLlano.getInstance());
        sinConstruccion.construite(hectarea);
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(sinConstruccion);

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Residencial(null), null);
        assertFalse(selectorIconoConstruccion.aceptado(inicial));
    }

    @Test
    public void testResidencialConSinConstruccionSobreAguaNoAceptado() {
        SinConstruccion sinConstruccion = new SinConstruccion();
        Construible hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        sinConstruccion.construite(hectarea);
        SelectorIconoConstruccion selectorIconoConstruccion = new SelectorIconoConstruccion(sinConstruccion);

        Inicial<Construccion, Image> inicial = new Inicial<Construccion, Image>(new Residencial(null), null);
        assertFalse(selectorIconoConstruccion.aceptado(inicial));
    }




}
