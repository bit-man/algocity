package algocity.test;

import static java.lang.Math.abs;
import static junit.framework.Assert.assertTrue;


public class AssertionsAlgoCity {


    private static final float TOLERANCIA_COMPARACION_FLOAT = (float) 0.0001;

    public static void assertEqualsFloat(float esperado, float obtenido) {
        assertTrue("esperado : " + esperado + ", obtenido : " + obtenido, abs(obtenido - esperado) < TOLERANCIA_COMPARACION_FLOAT);
    }
}
