package algocity.test.mapa;

import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.SuperficieLlano;
import org.junit.Test;

import static junit.framework.Assert.*;

public class HectareaTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new Hectarea( new Posicion(0,0), SuperficieLlano.getInstance()));
    }
    @Test
    public void testVerPosicion() {
        Hectarea hectarea = new Hectarea(new Posicion(0, 0), SuperficieLlano.getInstance());
        assertEquals(new Posicion(0,0), hectarea.getPosicion());
    }

    @Test
    public void testHectareaNoEnergizada() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException
    {
        Hectarea hectarea = new Hectarea(new Posicion(5, 0), SuperficieLlano.getInstance());
        assertFalse(hectarea.estaEnergizada());
    }

    @Test
    public void testHectareaEsBaldio() throws SuperficieInvalidaException, DineroInsuficienteException, JuegoNoIniciadoException
    {
        Hectarea hectarea = new Hectarea(new Posicion(5, 0), SuperficieLlano.getInstance());
        assertTrue(hectarea.getSuperficie().esLlano());
    }

}
