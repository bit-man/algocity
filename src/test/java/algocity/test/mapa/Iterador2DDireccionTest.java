package algocity.test.mapa;


import algocity.modelo.mapa.Iterador2DDireccion;
import algocity.modelo.mapa.Posicion;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class Iterador2DDireccionTest {

    @Test
    public void testIrALaDerecha() {
        Posicion calculado = Iterador2DDireccion.DERECHA.desplazamiento(0, 0);
        Posicion esperado = new Posicion(1,0);
        assertEquals(esperado, calculado);
    }

    @Test
    public void testIrALaIzquierda() {
        Posicion calculado = Iterador2DDireccion.IZQUIERDA.desplazamiento(0, 0);
        Posicion esperado = new Posicion(-1,0);
        assertEquals(esperado, calculado);
    }

    @Test
    public void testIrArriba() {
        Posicion calculado = Iterador2DDireccion.ARRIBA.desplazamiento(0, 0);
        Posicion esperado = new Posicion(0,-1);
        assertEquals(esperado, calculado);
    }
    @Test
    public void testIrAbajo() {
        Posicion calculado = Iterador2DDireccion.ABAJO.desplazamiento(0, 0);
        Posicion esperado = new Posicion(0,1);
        assertEquals(esperado, calculado);
    }
}
