package algocity.test.mapa;

import algocity.modelo.azar.*;
import algocity.modelo.mapa.*;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static junit.framework.Assert.*;

public class MapaTest {

    private static final int MAX_ITERACIONES = 1000;

    FuncionAzar azar;

    @Before
    public void init() {

        try {
            azar = new ConstructorAzarSimulado().
                    agregarHectareas(10*10).todoTierra().crear();
        } catch (Throwable e) {
            // Parámetros validados
        }
    }
    @Test
    public void testConstructorNoNulo() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        assertNotNull(new Mapa(10, 10, azar));
    }

    @Test
    public void testSinPoblacionInicial() {
        Mapa mapa = new Mapa(10, 10, azar);
        assertEquals(0, mapa.getPoblacion());
    }

    @Test
    public void testInitPosicionHectareas() throws FueraDelMapaException {
        Mapa mapa = new Mapa(10, 10, azar);

        for (int i = 0; i < 10; i++) {
            Iterador2D<Hectarea> iterator = mapa.iterator(i, 0);

            // Se itera sobre todos los elementos (menos el último) para evitar que al avanzar
            // hacia abajo sobre el último arroje FueraDelMapaException
            for (int j = 0; j < 9; j++) {
                assertEquals(new Posicion(i, j), iterator.elementoActual().getPosicion());
                iterator.moverHacia(Iterador2DDireccion.ABAJO);
            }

            // Ahora sí probamos el último elemento
            assertEquals(new Posicion(i, 9), iterator.elementoActual().getPosicion());
        }
    }

    @Test
    public void testHectareaAlAzar1() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Collection<Float> lista = new ArrayList<Float>(4);
        lista.add( (float) 0.3 );
        lista.add( (float) 0.6 );
        Mapa mapa = new Mapa(10, 10, new AzarSimulado(lista));

        Hectarea hectarea = mapa.hectareaAlAzar();
        assertEquals(  3, (int) hectarea.getPosicion().getX());
        assertEquals(  6, (int) hectarea.getPosicion().getY());
    }

    @Test
    public void testHectareaAlAzar3() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        final int horizontales = 10;
        final int verticales = 10;
        Collection<Float> lista = dameFloats(horizontales * verticales);
        lista.add( (float) 0.9 );
        lista.add( (float) 0.9 );
        lista.add( (float) 0.5 );
        lista.add( (float) 0.4 );
        lista.add( (float) 0.0 );
        lista.add( (float) 0.0 );

        Mapa mapa = new Mapa(horizontales, verticales, new AzarSimulado(lista));

        Posicion posicion = mapa.hectareaAlAzar().getPosicion();
        assertEquals(  9, (int) posicion.getX());
        assertEquals(  9, (int) posicion.getY());

        posicion = mapa.hectareaAlAzar().getPosicion();
        assertEquals(  5, (int) posicion.getX());
        assertEquals(  4, (int) posicion.getY());

        posicion = mapa.hectareaAlAzar().getPosicion();
        assertEquals(  0, (int) posicion.getX());
        assertEquals(  0, (int) posicion.getY());
    }

    private Collection<Float> dameFloats(int numFloats) {
        ArrayList<Float> list = new ArrayList<Float>(numFloats);
        for( int i = 1; i <= numFloats; i++) {
            list.add( (float) 0.9 );
        }
        return list;
    }


    @Test
    public void testSoloTerrenoLlano() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException {

        Mapa mapa = new Mapa(10, 10, new AzarSimulado((float) 0.5));

        for (int i = 0; i < 10; i++) {
            Iterador2D<Hectarea> iterator = mapa.iterator(i, 0);

            // Se itera sobre todos los elementos (menos el último) para evitar que al avanzar
            // hacia abajo sobre el último arroje FueraDelMapaException
            for (int j = 0; j < 9; j++) {
                assertTrue(iterator.elementoActual().getSuperficie().esLlano());
                iterator.moverHacia(Iterador2DDireccion.ABAJO);
            }

            // Ahora sí probamos el último elemento
            assertTrue(iterator.elementoActual().getSuperficie().esLlano());
        }
    }

    @Test
    public void testSoloAgua() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException {

        Mapa mapa = new Mapa(10, 10, new AzarSimulado((float) 0.95));

        for (int i = 0; i < 10; i++) {
            Iterador2D<Hectarea> iterator = mapa.iterator(i, 0);

            // Se itera sobre todos los elementos (menos el último) para evitar que al avanzar
            // hacia abajo sobre el último arroje FueraDelMapaException
            for (int j = 0; j < 9; j++) {
                assertTrue(iterator.elementoActual().getSuperficie().esAgua());
                iterator.moverHacia(Iterador2DDireccion.ABAJO);
            }

            // Ahora sí probamos el último elemento
            assertTrue(iterator.elementoActual().getSuperficie().esAgua());
        }
    }

    @Test
    public void testHectareaAlAzarLlegaAlBorde00() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {

        final int HORIZONTALES = 5;
        final int VERTICALES = 6;

        FuncionAzar azar = new ConstructorAzarSimulado().
                agregarHectareas(HORIZONTALES * VERTICALES).todoTierra()
                .numero((float) 0.0).numero((float) 0.0)
                .crear();

        Mapa mapa = new Mapa(HORIZONTALES, VERTICALES, azar);

        Hectarea hectarea = mapa.hectareaAlAzar();

        assertEquals( 0, (int) hectarea.getPosicion().getX() );
        assertEquals( 0, (int) hectarea.getPosicion().getY() );

    }

    @Test
    public void testHectareaAlAzarLlegaAlBorde06() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {

        final int HORIZONTALES = 5;
        final int VERTICALES = 6;

        FuncionAzar azar = new ConstructorAzarSimulado().
                agregarHectareas(HORIZONTALES * VERTICALES).todoTierra()
                .numero((float) 0.0).numero((float) 0.9999)
                .crear();

        Mapa mapa = new Mapa(HORIZONTALES, VERTICALES, azar);

        Hectarea hectarea1 = mapa.hectareaAlAzar();

        assertEquals( 0, (int) hectarea1.getPosicion().getX() );
        assertEquals( VERTICALES - 1, (int) hectarea1.getPosicion().getY() );

    }

    @Test
    public void testHectareaAlAzarLlegaAlBorde50() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {

        final int HORIZONTALES = 5;
        final int VERTICALES = 6;

        FuncionAzar azar = new ConstructorAzarSimulado().
                agregarHectareas(HORIZONTALES * VERTICALES).todoTierra()
                .numero((float) 0.9999).numero((float) 0.0)     // posición hectarea
                .crear();

        Mapa mapa = new Mapa(HORIZONTALES, VERTICALES, azar);

        Hectarea hectarea = mapa.hectareaAlAzar();

        assertEquals( HORIZONTALES - 1, (int) hectarea.getPosicion().getX() );
        assertEquals( 0, (int) hectarea.getPosicion().getY() );

    }

    @Test
    public void testHectareaAlAzarLlegaAlBorde56() throws AzarSimuladoElementoFueraDeRangoException, FueraDelMapaException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {

        final int HORIZONTALES = 5;
        final int VERTICALES = 6;

        FuncionAzar azar = new ConstructorAzarSimulado().
                agregarHectareas(HORIZONTALES * VERTICALES).todoTierra()
                .numero((float) 0.9999).numero((float) 0.9999)     // posición hectarea
                .crear();

        Mapa mapa = new Mapa(HORIZONTALES, VERTICALES, azar);

        Hectarea hectarea = mapa.hectareaAlAzar();

        assertEquals( HORIZONTALES - 1, (int) hectarea.getPosicion().getX() );
        assertEquals( VERTICALES - 1, (int) hectarea.getPosicion().getY() );

    }


    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);

        assertEquals(new Posicion(0, 5), mapa.hectareaDeBordeAlAzar().getPosicion());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeDerecho95() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.9) // coordenadaX = 9
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);

        assertEquals( new Posicion(9,5),  mapa.hectareaDeBordeAlAzar().getPosicion());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeSuperior50() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.0) // coordenadaY = 0
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);

        assertEquals( new Posicion(5,0),  mapa.hectareaDeBordeAlAzar().getPosicion());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeInferior59() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.9) // coordenadaY = 9
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);

        assertEquals( new Posicion(5,9),  mapa.hectareaDeBordeAlAzar().getPosicion());
    }
}
