package algocity.test.mapa;

import algocity.modelo.mapa.Posicion;
import org.junit.Test;

import static junit.framework.Assert.*;

public class PosicionTest {


    @Test
    public void testConstructorNoNulo() {
        assertNotNull(new Posicion(10, 10));
    }

    @Test
    public void testX() {
        Posicion posicion = new Posicion(10, 11);
        assertEquals((Integer) 10, posicion.getX());
    }

    @Test
    public void testY() {
        Posicion posicion = new Posicion(10, 11);
        assertEquals(11, (int) posicion.getY());
    }

    @Test
    public void testEquals() {
        Posicion calculada = new Posicion(10, 11);
        Posicion esperada = new Posicion(10, 11);
        assertEquals( calculada, esperada);
    }

    @Test
    public void testMismoObjeto() {
        Posicion calculada = new Posicion(10, 11);
        assertTrue(calculada.equals(calculada));
    }


    @Test
    public void testToString() {
        Posicion calculada = new Posicion(10, 11);
        assertEquals( "{ x=10, y=11 }", calculada.toString());
    }


}
