package algocity.test.mapa;

import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.mapa.*;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class IteradorMapaTest {

    private Mapa mapa;

    @Before
    public void init() {

        FuncionAzar azar = null;
        try {
            azar = new ConstructorAzarSimulado().
                    agregarHectareas(10 * 10).todoTierra().crear();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        mapa = new Mapa(10, 10, azar);
    }
    @Test
    public void testIteradorNoNulo() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        assertNotNull(mapa.iterator(0,0));
    }

    @Test
    public void testNoHayMasHaciaHaciaArriba00() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        assertFalse(iterator.hayMasHacia(Iterador2DDireccion.ARRIBA));
    }

    @Test
    public void testNoHayMasHaciaLaHaciaIzquierda00() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        assertFalse(iterator.hayMasHacia(Iterador2DDireccion.IZQUIERDA));
    }

    @Test()
    public void testHayMasHaciaLaDerecha00() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.DERECHA));
    }

    @Test()
    public void testHayMasHaciaAbajo00() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.ABAJO));
    }

    @Test
    public void testHayMasHaciaArriba99() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.ARRIBA));
    }

    @Test
    public void testHayMasHaciaLaIzquierda99() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.IZQUIERDA));
    }

    @Test()
    public void testNoHayMasHaciaLaDerecha99() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        assertFalse(iterator.hayMasHacia(Iterador2DDireccion.DERECHA));
    }

    @Test()
    public void testNoHayMasHaciaAbajo99() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        assertFalse(iterator.hayMasHacia(Iterador2DDireccion.ABAJO));
    }
    @Test
    public void testHayMasHaciaArriba55() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(5, 5);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.ARRIBA));
    }

    @Test
    public void testHayMasHaciaLaIzquierda55() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(5, 5);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.IZQUIERDA));
    }

    @Test()
    public void testHayMasHaciaHaciaLaDerecha55() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Iterador2D<Hectarea> iterator = mapa.iterator(5, 5);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.DERECHA));
    }

    @Test()
    public void testHayMasHaciaAbajo55() {
        Iterador2D<Hectarea> iterator = mapa.iterator(5, 5);
        assertTrue(iterator.hayMasHacia(Iterador2DDireccion.ABAJO));
    }

    @Test()
    public void testActual00() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        assertEquals(0, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(0, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test()
    public void testActual99() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        assertEquals(9, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(9, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test()
    public void testActual55() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(5, 5);
        assertEquals(5, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(5, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test()
    public void testMoverHaciaLaDerecha00() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        iterator.moverHacia(Iterador2DDireccion.DERECHA);
        assertEquals(1, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(0, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test()
    public void testMoverHaciaAbajo00() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        iterator.moverHacia(Iterador2DDireccion.ABAJO);
        assertEquals(0, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(1, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test(expected = FueraDelMapaException.class)
    public void testMoverHaciaLaIzquierda00Falla() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        iterator.moverHacia(Iterador2DDireccion.IZQUIERDA);
    }

    @Test(expected = FueraDelMapaException.class)
    public void testMoverHaciaArriba00Falla() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(0, 0);
        iterator.moverHacia(Iterador2DDireccion.ARRIBA);
    }

    @Test(expected = FueraDelMapaException.class)
    public void testMoverHaciaAbajo99Falla() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        iterator.moverHacia(Iterador2DDireccion.ABAJO);
    }

    @Test(expected = FueraDelMapaException.class)
    public void testMoverHaciaLaDerecha99Falla() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        iterator.moverHacia(Iterador2DDireccion.DERECHA);
    }


    @Test()
    public void testMoverHaciaLaIzquierda99() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        iterator.moverHacia(Iterador2DDireccion.IZQUIERDA);
        assertEquals(8, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(9, (int) iterator.elementoActual().getPosicion().getY());
    }

    @Test()
    public void testMoverHaciaArriba99() throws FueraDelMapaException {
        Iterador2D<Hectarea> iterator = mapa.iterator(9, 9);
        iterator.moverHacia(Iterador2DDireccion.ARRIBA);
        assertEquals(9, (int) iterator.elementoActual().getPosicion().getX());
        assertEquals(8, (int) iterator.elementoActual().getPosicion().getY());
    }

}
