package algocity.test.catastrofe;

import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.catastrofe.Godzilla;
import algocity.modelo.catastrofe.GodzillaDiagonal;
import algocity.modelo.catastrofe.GodzillaZigZag;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Test;

import java.util.Iterator;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;

public class GodzillaDiagonalTest {

    @Test
    public void testConstructorNoNulo(){
        assertNotNull(new GodzillaZigZag(null, null));
    }

    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .numero((float) 0.0) // un solo salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        Godzilla godzillaLineal = new GodzillaDiagonal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        assertEquals(new Posicion(0, 5), caminoGodzillaRecolector.getCamino().iterator().next());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05NoHaceSalto() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .numero((float) 0.0) // sin salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);
        Godzilla godzillaLineal = new GodzillaDiagonal(mapa, azarMapa);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        final Iterator<Posicion> iterator = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(0, 5), iterator.next());
        assertEquals(new Posicion(1, 5), iterator.next());
        assertEquals(new Posicion(2, 5), iterator.next());
        assertEquals(new Posicion(3, 5), iterator.next());
        assertEquals(new Posicion(4, 5), iterator.next());
        assertEquals(new Posicion(5, 5), iterator.next());
        assertEquals(new Posicion(6, 5), iterator.next());
        assertEquals(new Posicion(7, 5), iterator.next());
        assertEquals(new Posicion(8, 5), iterator.next());
        assertEquals(new Posicion(9, 5), iterator.next());

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05HaceUnSalto() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azarMapa = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .numero((float) 0.1) // un solo salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azarMapa);
        Godzilla godzillaLineal = new GodzillaDiagonal(mapa, azarMapa);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        final Iterator<Posicion> iterator = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(0, 5), iterator.next());
        assertEquals(new Posicion(1, 5), iterator.next());
        assertEquals(new Posicion(2, 5), iterator.next());
        assertEquals(new Posicion(3, 5), iterator.next());
        assertEquals(new Posicion(4, 5), iterator.next());
        assertEquals(new Posicion(5, 5), iterator.next());
        assertEquals(new Posicion(5, 6), iterator.next());
        assertEquals(new Posicion(6, 6), iterator.next());
        assertEquals(new Posicion(7, 6), iterator.next());
        assertEquals(new Posicion(8, 6), iterator.next());
        assertEquals(new Posicion(9, 6), iterator.next());

        assertFalse(iterator.hasNext());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeSuperior50HaceUnSalto() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {


        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.0) // coordenadaY = 0
                .numero((float) 0.5) // coordenadaX = 5
                .numero((float) 0.1) // hace un salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        Godzilla godzillaLineal = new GodzillaDiagonal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        final Iterator<Posicion> iterator = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(5, 0), iterator.next());
        assertEquals(new Posicion(6, 0), iterator.next());
        assertEquals(new Posicion(6, 1), iterator.next());
        assertEquals(new Posicion(6 ,2), iterator.next());
        assertEquals(new Posicion(6, 3), iterator.next());
        assertEquals(new Posicion(6, 4), iterator.next());
        assertEquals(new Posicion(6, 5), iterator.next());
        assertEquals(new Posicion(7, 5), iterator.next());
        assertEquals(new Posicion(7, 6), iterator.next());
        assertEquals(new Posicion(7, 7), iterator.next());
        assertEquals(new Posicion(7, 8), iterator.next());
        assertEquals(new Posicion(7, 9), iterator.next());

        assertFalse(iterator.hasNext());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeInferior59HaceUnSalto() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.9) // coordenadaY = 9
                .numero((float) 0.3) // coordenadaX = 3
                .numero((float) 0.1) // hace un salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        Godzilla godzillaDiagonal = new GodzillaDiagonal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaDiagonal.registrar(caminoGodzillaRecolector);
        godzillaDiagonal.hacerDanio();

        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(3, 9), camino.next());
        assertEquals(new Posicion(4, 9), camino.next());
        assertEquals(new Posicion(5, 9), camino.next());
        assertEquals(new Posicion(5, 8), camino.next());
        assertEquals(new Posicion(5, 7), camino.next());
        assertEquals(new Posicion(5, 6), camino.next());
        assertEquals(new Posicion(5, 5), camino.next());
        assertEquals(new Posicion(5, 4), camino.next());
        assertEquals(new Posicion(6, 4), camino.next());
        assertEquals(new Posicion(7, 4), camino.next());
        assertEquals(new Posicion(7, 3), camino.next());
        assertEquals(new Posicion(7, 2), camino.next());
        assertEquals(new Posicion(7, 1), camino.next());
        assertEquals(new Posicion(7, 0), camino.next());

        assertFalse(camino.hasNext());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeDerecho95haceUnSalto() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.9) // coordenadaX = 9
                .numero((float) 0.3) // coordenadaY = 3
                .numero((float) 0.1) // hace un salto
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        Godzilla godzillaDiagonal = new GodzillaDiagonal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaDiagonal.registrar(caminoGodzillaRecolector);
        godzillaDiagonal.hacerDanio();

        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(9, 3), camino.next());
        assertEquals(new Posicion(8, 3), camino.next());
        assertEquals(new Posicion(7, 3), camino.next());
        assertEquals(new Posicion(6, 3), camino.next());
        assertEquals(new Posicion(5, 3), camino.next());
        assertEquals(new Posicion(4, 3), camino.next());
        assertEquals(new Posicion(4, 4), camino.next());
        assertEquals(new Posicion(4, 5), camino.next());
        assertEquals(new Posicion(3, 5), camino.next());
        assertEquals(new Posicion(2, 5), camino.next());
        assertEquals(new Posicion(1, 5), camino.next());
        assertEquals(new Posicion(0, 5), camino.next());

        assertFalse(camino.hasNext());
    }

}
