package algocity.test.catastrofe;

import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.catastrofe.GodzillaLineal;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Test;

import java.util.Iterator;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;

public class GodzillaLinealTest {


    @Test
    public void testConstructorNoNulo(){
        assertNotNull(new GodzillaLineal(null, null));
    }

    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa,azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        assertEquals( new Posicion(0,5),  caminoGodzillaRecolector.getCamino().iterator().next());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeDerecho95() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.9) // coordenadaX = 9
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa,azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        assertEquals( new Posicion(9,5),  caminoGodzillaRecolector.getCamino().iterator().next());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeSuperior50() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.0) // coordenadaY = 0
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        assertEquals( new Posicion(5,0),  caminoGodzillaRecolector.getCamino().iterator().next());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeInferior59() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.9) // coordenadaY = 9
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        assertEquals( new Posicion(5,9),  caminoGodzillaRecolector.getCamino().iterator().next());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeIzquierdo05YVaHaciaLaDerecha() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.0) // coordenaday = 0
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);

        godzillaLineal.hacerDanio();
        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals( new Posicion(0, 5), camino.next());
        assertEquals( new Posicion(1, 5), camino.next());
        assertEquals( new Posicion(2, 5), camino.next());
        assertEquals( new Posicion(3, 5), camino.next());
        assertEquals( new Posicion(4, 5), camino.next());
        assertEquals( new Posicion(5, 5), camino.next());
        assertEquals( new Posicion(6, 5), camino.next());
        assertEquals( new Posicion(7, 5), camino.next());
        assertEquals( new Posicion(8, 5), camino.next());
        assertEquals( new Posicion(9, 5), camino.next());

        assertFalse(camino.hasNext());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeSuperior50YVaHaciaAbajo() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.0) // coordenadaY = 0
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);

        godzillaLineal.hacerDanio();
        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals( new Posicion(5, 0), camino.next());
        assertEquals( new Posicion(5, 1), camino.next());
        assertEquals( new Posicion(5, 2), camino.next());
        assertEquals( new Posicion(5, 3), camino.next());
        assertEquals( new Posicion(5, 4), camino.next());
        assertEquals( new Posicion(5, 5), camino.next());
        assertEquals( new Posicion(5, 6), camino.next());
        assertEquals( new Posicion(5, 7), camino.next());
        assertEquals( new Posicion(5, 8), camino.next());
        assertEquals( new Posicion(5, 9), camino.next());

        assertFalse(camino.hasNext());
    }


    @Test
    public void testCaminoEmpiezaEnElBordeInferior59yVaHaciaArriba() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.9) // x NO en el extremo
                .numero((float) 0.9) // coordenadaY = 9
                .numero((float) 0.5) // coordenadaX = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();
        assertEquals(new Posicion(5, 9), camino.next());
        assertEquals(new Posicion(5, 8), camino.next());
        assertEquals(new Posicion(5, 7), camino.next());
        assertEquals(new Posicion(5, 6), camino.next());
        assertEquals(new Posicion(5, 5), camino.next());
        assertEquals(new Posicion(5, 4), camino.next());
        assertEquals(new Posicion(5, 3), camino.next());
        assertEquals(new Posicion(5, 2), camino.next());
        assertEquals(new Posicion(5, 1), camino.next());
        assertEquals(new Posicion(5, 0), camino.next());

        assertFalse(camino.hasNext());
    }

    @Test
    public void testCaminoEmpiezaEnElBordeDerecho95YVaHaviaLaIzquierda() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

        FuncionAzar azar = new ConstructorAzarSimulado()
                .agregarHectareas(10 * 10)
                .todoTierra()
                .numero((float) 0.0) // x en el extremo
                .numero((float) 0.9) // coordenadaX = 9
                .numero((float) 0.5) // coordenadaY = 5
                .crear();
        Mapa mapa = new Mapa(10, 10, azar);
        GodzillaLineal godzillaLineal = new GodzillaLineal(mapa, azar);
        CaminoGodzillaRecolector caminoGodzillaRecolector = new CaminoGodzillaRecolector();
        godzillaLineal.registrar(caminoGodzillaRecolector);
        godzillaLineal.hacerDanio();

        final Iterator<Posicion> camino = caminoGodzillaRecolector.getCamino().iterator();

        assertEquals(new Posicion(9, 5), camino.next());
        assertEquals(new Posicion(8, 5), camino.next());
        assertEquals(new Posicion(7, 5), camino.next());
        assertEquals(new Posicion(6, 5), camino.next());
        assertEquals(new Posicion(5, 5), camino.next());
        assertEquals(new Posicion(4, 5), camino.next());
        assertEquals(new Posicion(3, 5), camino.next());
        assertEquals(new Posicion(2, 5), camino.next());
        assertEquals(new Posicion(1, 5), camino.next());
        assertEquals(new Posicion(0, 5), camino.next());

        assertFalse(camino.hasNext());
    }


}