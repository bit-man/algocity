package algocity.test.catastrofe;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorNoDeregistrable;
import algocity.modelo.mapa.Posicion;

import java.util.ArrayList;
import java.util.Collection;

public class CaminoGodzillaRecolector
        extends OperadorNoDeregistrable<Catastrofe>
{

    private Collection<Posicion> camino;

    public CaminoGodzillaRecolector() {
        camino = new ArrayList<Posicion>();
    }

    @Override
    public Boolean operar(Catastrofe posicion) {
        this.camino.add(posicion.getPosicionDaniada());
        return Boolean.TRUE;
    }

    public Collection<Posicion> getCamino() {
        return camino;
    }


}
