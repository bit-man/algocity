package algocity.test.catastrofe;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.catastrofe.Terremoto;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.necesidades.TuberiaDeAgua;
import algocity.modelo.zona.*;
import algocity.test.AssertionsAlgoCity;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class TerremotoTest {

    private Mapa unMapa;
    private Hectarea unEpicentro;
    private Jugador jugador;

    @Before
    public void init() {

        FuncionAzar azar = null;
        try {
            azar = new ConstructorAzarSimulado().
                    agregarHectareas(10 * 10).todoTierra().crear();
        } catch (Throwable e) {
            // Constantes validadas, sin posibilidad de error
        }

        unMapa = new Mapa(10, 10, azar);
        unEpicentro = unMapa.iterator(5,5).elementoActual();

        try {
            jugador = new Jugador("JugadorDePrueba",unMapa);
        } catch (LongitudNombreJugadorInsuficienteException e) {
            // No falla, nombre bien formado
        }

    }
	
	@Test
	public void testConstructorNoVacio(){
		 assertNotNull(new Terremoto(unMapa, unEpicentro));
	}

    @Test
    public void testHacerDanioNoCausaDanioSinConstrucciones(){

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        assertEquals( (float) 1.0, unEpicentro.getConstruccion().getIntegridad());
    }

    @Test
    public void testHacerDanioConConstruccionesVerificarEpicentro() throws SuperficieInvalidaException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);
        unEpicentro.setSuperficie(SuperficieLlano.getInstance());
        new Comercial(jugador).construite(unEpicentro);


        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        assertEquals( (float) 0.0, unEpicentro.getConstruccion().getIntegridad());
    }

    @Test
    public void testHacerDanioConConstruccionesVerificarHectareasVecinasRadio1() throws SuperficieInvalidaException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        final Posicion posicionEpicentro = unEpicentro.getPosicion();
        final Posicion posicionHectareaVecina = new Posicion(posicionEpicentro.getX(), posicionEpicentro.getY() + 1);
        Hectarea hecrareaVecina = unMapa.iterator(posicionHectareaVecina).elementoActual();
        hecrareaVecina.setSuperficie(SuperficieLlano.getInstance());
        new Comercial(jugador).construite(hecrareaVecina);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        AssertionsAlgoCity.assertEqualsFloat( Parametros.DANIO_TERREMOTO, hecrareaVecina.getConstruccion().getIntegridad());
    }

    @Test
    public void testHacerDanioConConstruccionesVerificarHectareasVecinasRadio2() throws SuperficieInvalidaException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        final Posicion posicionEpicentro = unEpicentro.getPosicion();
        final Posicion posicionHectareaVecina = new Posicion(posicionEpicentro.getX(), posicionEpicentro.getY() + 2);
        Hectarea hecrareaVecina = unMapa.iterator(posicionHectareaVecina).elementoActual();
        hecrareaVecina.setSuperficie(SuperficieLlano.getInstance());
        new Comercial(jugador).construite(hecrareaVecina);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        AssertionsAlgoCity.assertEqualsFloat( Parametros.DANIO_TERREMOTO * 2, hecrareaVecina.getConstruccion().getIntegridad());
    }

    @Test
    public void testHacerDanioConConstruccionesVerificarHectareasVecinasRadio3() throws SuperficieInvalidaException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        final Posicion posicionEpicentro = unEpicentro.getPosicion();
        final Posicion posicionHectareaVecina = new Posicion(posicionEpicentro.getX(), posicionEpicentro.getY() + 3);
        Hectarea hecrareaVecina = unMapa.iterator(posicionHectareaVecina).elementoActual();
        hecrareaVecina.setSuperficie(SuperficieLlano.getInstance());
        new Comercial(jugador).construite(hecrareaVecina);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        AssertionsAlgoCity.assertEqualsFloat( Parametros.DANIO_TERREMOTO * 3, hecrareaVecina.getConstruccion().getIntegridad());
    }

    @Test
    public void testHacerDanioConConstruccionesEnUnaAristaYCalcularEnLaAristaOpuesta() throws SuperficieInvalidaException, DineroInsuficienteException {

        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

        unEpicentro = unMapa.iterator(0,0).elementoActual();
        final Posicion posicionEpicentro = unEpicentro.getPosicion();
        final Posicion posicionHectareaVecina = new Posicion(posicionEpicentro.getX(), posicionEpicentro.getY() + 9);
        Hectarea hectareaOpuesta = unMapa.iterator(posicionHectareaVecina).elementoActual();
        hectareaOpuesta.setSuperficie(SuperficieLlano.getInstance());
        new Comercial(jugador).construite(hectareaOpuesta);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        AssertionsAlgoCity.assertEqualsFloat((float) Parametros.DANIO_TERREMOTO * 9, hectareaOpuesta.getConstruccion().getIntegridad());
    }

    @Test
    public void testTuberiaNoSeRompe() throws ConstruccionInvalidaExcepcion, DineroInsuficienteException, SuperficieInvalidaException, JuegoNoIniciadoException

    {

        final Posicion posicionEpicentro = unEpicentro.getPosicion();
        unEpicentro.setSuperficie(SuperficieAgua.getInstance());
        final Posicion posicionHectareaVecina = new Posicion(posicionEpicentro.getX(), posicionEpicentro.getY() + 1);
        Hectarea destino = unMapa.iterator(posicionHectareaVecina).elementoActual();
        final TuberiaDeAgua tuberiaDeAgua = new TuberiaDeAgua(jugador);
        tuberiaDeAgua.construite(unEpicentro, destino);
        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        assertEquals( (float) 1.0, tuberiaDeAgua.getIntegridad());
    }

    @Test
    public void testPozoDeAguaNoSeRompe() throws Throwable {
        unEpicentro.setSuperficie(SuperficieAgua.getInstance());

        final Construccion pozoDeAgua = new PozoDeAgua(jugador);
        pozoDeAgua.construite(unEpicentro);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        assertEquals((float) 1.0, pozoDeAgua.getIntegridad());
    }

    @Test
    public void testEstacionDeBomberosSeRompe() throws Throwable {

        final Construccion pozoDeAgua = new EstacionDeBomberos(jugador);
        pozoDeAgua.construite(unEpicentro);

        Terremoto t = new Terremoto(unMapa, unEpicentro);
        t.hacerDanio();

        assertEquals((float) 1.0, pozoDeAgua.getIntegridad());
    }
}
