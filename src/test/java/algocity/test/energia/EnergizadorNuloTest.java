package algocity.test.energia;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.energia.EnergizadorNulo;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EnergizadorNuloTest {

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( EnergizadorNulo.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(EnergizadorNulo.getInstance() == EnergizadorNulo.getInstance());
    }

    @Test( expected = EnergiaInsuficienteException.class)
    public void testNoDaEnergia() throws EnergiaInsuficienteException {
        EnergizadorNulo.getInstance().dameEnergía(10);
    }


}
