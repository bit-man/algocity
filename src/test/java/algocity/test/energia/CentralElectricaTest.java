package algocity.test.energia;

import algocity.modelo.azar.*;
import algocity.modelo.energia.*;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.SuperficieAgua;
import algocity.test.TestHelper;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

public class CentralElectricaTest {

    private Jugador jugador;

    @Before
	public void init() {
		try {
            jugador = new Jugador("Pepe", null);
        } catch (LongitudNombreJugadorInsuficienteException e) {
			// Nombre constante bien formado
		}
	}

	@Test
	public void testConstructorNoNulo() {
		assertNotNull( new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), jugador));
	}

	//----------------------------------------- Energía Eólica

	@Test
	public void obtenerRangoDeProporcionElectricaEnEolica(){
		CentralElectrica central =  new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), jugador);

		assertEquals(central.getRango(), 4);
	}

	//----------------------------------------- Energía Eólica

	@Test
	public void obtenerRangoDeProporcionElectricaEnMineral(){
		CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);

		assertEquals(central.getRango(), 10);
	}

	@Test
	public void obtenerRangoDeProporcionElectricaEnNulcear(){
		CentralElectrica central = new CentralElectrica(CentralNuclear.getInstance(), jugador.getMapa(), jugador);

		assertEquals(central.getRango(), 25);
	}

	@Test
	public void testPoneEnergiaEnLasHectareasCorrectasVerificaHorizontalDerecha() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

		FuncionAzar azarSoloTierra = new ConstructorAzarSimulado()
				.agregarHectareas(100*100)
				.todoTierra()
				.crear();
		Mapa mapa = new Mapa(100, 100, azarSoloTierra );
		Jugador jugador = new Jugador("Pepe", null);
		jugador.agregarDinero(TestHelper.MUCHO_DINERO);
		CentralElectrica central =  new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);

		Hectarea hectarea = mapa.iterator(50,50).elementoActual();

		central.construite( hectarea );

		// horizontal en línea recta
		assertTrue( mapa.iterator(51,50).elementoActual().estaEnergizada() );
		assertTrue( mapa.iterator(52,50).elementoActual().estaEnergizada() );
		assertTrue( mapa.iterator(53,50).elementoActual().estaEnergizada() );
		assertTrue( mapa.iterator(54,50).elementoActual().estaEnergizada() );
		assertFalse(mapa.iterator(55,50).elementoActual().estaEnergizada());
	}

	@Test
	public void testPoneEnergiaEnLasHectareasCorrectasVerificaVertice() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

		FuncionAzar azar = new ConstructorAzarSimulado()
				.agregarHectareas(100*100)
				.todoTierra()
				.crear();
		Mapa mapa = new Mapa(100,100, azar);
		Jugador jugador = new Jugador("Pepe", null);
		jugador.agregarDinero(TestHelper.MUCHO_DINERO);
		CentralElectrica central =  new CentralElectrica(CentralEolica.getInstance(), mapa, jugador);

		Hectarea hectarea = mapa.iterator(50,50).elementoActual();

		central.construite( hectarea );

		// Alrededor de un vértice
		assertTrue(  mapa.iterator(54,54).elementoActual().estaEnergizada() );
		assertFalse( mapa.iterator(54,55).elementoActual().estaEnergizada() );
		assertFalse( mapa.iterator(55,54).elementoActual().estaEnergizada() );
		assertFalse( mapa.iterator(55,55).elementoActual().estaEnergizada() );
	}

	@Test
	public void testPoneEnergiaEnLasHectareasCorrectasConMapaReducido() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {

		final int CENTRO_X = 1;
		final int CENTRO_Y = 1;
		final int HORIZONTALES = 5;
		final int VERTICALES = 5;

		FuncionAzar azarSoloTierra = new ConstructorAzarSimulado()
								.agregarHectareas(HORIZONTALES*VERTICALES)
								.todoTierra()
								.crear();
		Mapa mapaSoloTierra = new Mapa(HORIZONTALES,VERTICALES, azarSoloTierra);
		Jugador jugador = new Jugador("Pepe", null);
		jugador.agregarDinero(TestHelper.MUCHO_DINERO);
		CentralElectrica central =  new CentralElectrica(CentralEolica.getInstance(), mapaSoloTierra, jugador);
		Hectarea hectarea = mapaSoloTierra.iterator(CENTRO_X,CENTRO_Y).elementoActual();

		central.construite( hectarea );

		for ( int x = 0; x < HORIZONTALES; x++) {
			for ( int y = 0; y < VERTICALES; y++) {
				if ( x != CENTRO_X && y != CENTRO_Y) {
					final Hectarea hectareaActual = mapaSoloTierra.iterator(x, y).elementoActual();
					assertTrue("Fallo en las coordenadas x=" + x + " , y=" + y,
							hectareaActual.estaEnergizada());
				}
			}
		}
	}

	@Test
	public void testHayEnergiaSuficiente() throws EnergiaInsuficienteException {
		CentralElectrica central = new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), jugador);

		// Se le quita toda la energía y alcanza
		central.dameEnergia(central.getCapacidadRestante());
	}

	@Test( expected = EnergiaInsuficienteException.class)
	public void testHayEnergiaINSuficiente() throws EnergiaInsuficienteException {
		CentralElectrica central = new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), jugador);

		// Se le quita toda la energía y alcanza
		central.dameEnergia( central.getCapacidadRestante() + 1);
	}

    @Test( expected = SuperficieInvalidaException.class )
    public void testNoConstruyeSobreElAgua() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException {
        CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), null, new Jugador("Pepe",null));
        Hectarea hectarea = new Hectarea(new Posicion(0,0), SuperficieAgua.getInstance());
        central.construite(hectarea);
    }


    @Test
    public void testSeConstruyeSobreTierra() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

		FuncionAzar azarSoloTierra = new ConstructorAzarSimulado()
				.agregarHectareas(1*1)
				.todoTierra()
				.crear();
		final Mapa mapa = new Mapa(1, 1, azarSoloTierra);
		CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), mapa, jugador);
        Hectarea hectarea = mapa.iterator(0,0).elementoActual();
        central.construite(hectarea);
    }

    @Test
    public void testSeConstruyeVerConstruccion() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Jugador jugador = new Jugador("Pepe", null);
        jugador.agregarDinero(TestHelper.MUCHO_DINERO);

		FuncionAzar azarSoloTierra = new ConstructorAzarSimulado()
				.agregarHectareas(1*1)
				.todoTierra()
				.crear();
		final Mapa mapa = new Mapa(1, 1, azarSoloTierra);
		CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), mapa, jugador);
        Hectarea hectarea = mapa.iterator(0,0).elementoActual();
        central.construite(hectarea);

        assertTrue( central == hectarea.getConstruccion() );
    }

    @Test(expected = DineroInsuficienteException.class)
    public void testNoSeConstruyeCentralMineralsinPlata() throws FueraDelMapaException, JuegoNoIniciadoException, SuperficieInvalidaException, DineroInsuficienteException, LongitudNombreJugadorInsuficienteException, AzarSimuladoElementoFueraDeRangoException {
		final Mapa mapaSinAgua = new Mapa(1, 1, new AzarSimulado((float) 0.1));
		final Jugador jugador = new Jugador("Pepe", null);
		jugador.quitarDinero(jugador.getDinero() );
		CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), mapaSinAgua, jugador);
        Hectarea hectarea = mapaSinAgua.iterator(0,0).elementoActual();
        central.construite(hectarea);

        assertTrue( central == hectarea.getConstruccion() );
    }
}
