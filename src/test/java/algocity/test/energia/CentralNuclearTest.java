package algocity.test.energia;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralNuclear;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

public class CentralNuclearTest {
    private Jugador jugador;

    @Before
    public void init() {
        try {
            jugador = new Jugador("Pepe", null);
        } catch (LongitudNombreJugadorInsuficienteException e) {
            // Nombre constante bien formado
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(CentralNuclear.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(CentralNuclear.getInstance() == CentralNuclear.getInstance());
    }


    @Test
    public void obtenerRangoDeProporcionElectricaEnNulcear(){
        CentralElectrica central = new CentralElectrica(CentralNuclear.getInstance(), jugador.getMapa(), jugador);

        assertEquals(central.getRango(), 25);
    }


}
