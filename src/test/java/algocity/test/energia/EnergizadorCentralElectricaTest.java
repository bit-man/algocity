package algocity.test.energia;


import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.energia.EnergizadorCentralElectrica;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EnergizadorCentralElectricaTest {

    @Test
    public void testConstructorNoNulo() {
        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), null, null);
        assertNotNull( new EnergizadorCentralElectrica(central));
    }


    @Test
    public void testEnergiaRestanteAlcanza() throws EnergiaInsuficienteException {
        CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), null, null);
        EnergizadorCentralElectrica energizador = new EnergizadorCentralElectrica(central) ;

        final int capacidadAbastecimiento = central.getCapacidadRestante();
        energizador.dameEnergía(100);

        assertEquals( 100, capacidadAbastecimiento - central.getCapacidadRestante());
    }

    @Test( expected = EnergiaInsuficienteException.class )
    public void testEnergiaRestanteNOAlcanza() throws EnergiaInsuficienteException {
        CentralElectrica central = new CentralElectrica( CentralMineral.getInstance(), null, null);
        EnergizadorCentralElectrica energizador = new EnergizadorCentralElectrica(central) ;

        energizador.dameEnergía(central.getCapacidadRestante() + 1);
    }

}
