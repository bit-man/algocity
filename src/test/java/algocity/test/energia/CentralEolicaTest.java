package algocity.test.energia;

import algocity.modelo.energia.CentralElectricaTipo;
import algocity.modelo.energia.CentralEolica;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

public class CentralEolicaTest {
    @Test
    public void testConstructorNoNulo() {
        assertNotNull(CentralEolica.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(CentralEolica.getInstance() == CentralEolica.getInstance());
    }


    @Test
    public void obtenerRangoDeProporcionElectricaEnNulcear(){
        CentralElectricaTipo central = CentralEolica.getInstance();

        assertEquals(central.getRango(), 4);
    }

}
