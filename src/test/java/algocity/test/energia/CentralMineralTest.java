package algocity.test.energia;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

public class CentralMineralTest {

    private Jugador jugador;

    @Before
    public void init() {
        try {
            jugador = new Jugador("Pepe", null);
        } catch (LongitudNombreJugadorInsuficienteException e) {
            // Nombre constante bien formado
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull(CentralMineral.getInstance());
    }

    @Test
    public void testConstructorMismoObjeto() {
        assertTrue(CentralMineral.getInstance() == CentralMineral.getInstance());
    }


    @Test
    public void obtenerRangoDeProporcionElectricaEnNulcear(){
        CentralElectrica central = new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador);

        assertEquals(central.getRango(), 10);
    }


}
