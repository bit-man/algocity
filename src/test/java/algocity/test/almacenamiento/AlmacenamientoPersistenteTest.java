package algocity.test.almacenamiento;

import algocity.modelo.almacenamiento.*;
import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.Mapa;
import algocity.test.azar.util.ConstructorAzarSimulado;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AlmacenamientoPersistenteTest {

    private static final String DIRECTORIO_JUGADOR = "jugador";
    private static final String NOMBRE_JUGADOR_DEFAULT = "Pepe";
    private File directorioDeconfiguracion;
    private Jugador jugador;
    private Mapa mapa;

    @Before
    public void init() {
        try {
            this.directorioDeconfiguracion = Files.createTempDirectory(null).toFile();
            FuncionAzar azar = new ConstructorAzarSimulado()
                    .agregarHectareas(10*10)
                    .todoTierra()
                    .crear();
            mapa = new Mapa(10, 10, azar);
            jugador = new Jugador(NOMBRE_JUGADOR_DEFAULT, mapa);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConstructorNoNulo() {
        assertNotNull( new AlmacenamientoPersistente(directorioDeconfiguracion));
    }

    //~~---------------------------------------------------------- Creación de drectorios
    @Test
    public void testCreacionCreaDirectorioConfiguracion() throws
            DirectorioDeConfiguracionNoInicializadoException
    {
        File directorioInexistente = new File(directorioDeconfiguracion, "noExiste");
        new AlmacenamientoPersistente(directorioInexistente).inicializar();

        assertTrue(directorioInexistente.exists());
    }

    @Test
    public void testCreacionCreaArchivoDePuntajes() throws DirectorioDeConfiguracionNoInicializadoException
    {
        new AlmacenamientoPersistente(directorioDeconfiguracion).inicializar();

        assertTrue(new File(directorioDeconfiguracion, "puntajes").exists());
    }

    @Test
    public void testCreacionCreaDirectorioJugadores() throws DirectorioDeConfiguracionNoInicializadoException
    {
        new AlmacenamientoPersistente(directorioDeconfiguracion).inicializar();

        assertTrue( new File( directorioDeconfiguracion, DIRECTORIO_JUGADOR).exists() );
    }

    @Test
    public void testCreacionCreaDirectorioJugadoresVacio() throws DirectorioDeConfiguracionNoInicializadoException
    {
        new AlmacenamientoPersistente(directorioDeconfiguracion).inicializar();

        assertEquals(0, new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR).listFiles().length);
    }


    @Test( expected = DirectorioDeConfiguracionNoInicializadoException.class)
    public void testCreacionFallaAlCrearDirectorioJugadores() throws DirectorioDeConfiguracionNoInicializadoException,
            IOException
    {
        new File( directorioDeconfiguracion, Parametros.ALMACENAMIENTO_DIRECTORIO_JUGADOR).createNewFile();
        new AlmacenamientoPersistente(directorioDeconfiguracion).inicializar();

    }

    @Test( expected = DirectorioDeConfiguracionNoInicializadoException.class)
    public void testCreacionFallaAlCrearArchivoPuntajes() throws DirectorioDeConfiguracionNoInicializadoException,
            IOException
    {
        new File( directorioDeconfiguracion, Parametros.ALMACENAMIENTO_PUNTAJES).mkdirs();
        new AlmacenamientoPersistente(directorioDeconfiguracion).inicializar();
    }


    //~~---------------------------------------------------------- Agregado de jugadores

    @Test( expected = ErrorAlObtenerJugadorException.class)
    public void testCreacionObtenerJugadorFalla() throws Throwable {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        configuracion.inicializar();

        configuracion.getJugador(NOMBRE_JUGADOR_DEFAULT);
    }


    @Test
    public void testAgregar1Jugador() throws Throwable {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        configuracion.inicializar();
        configuracion.agregarJugador(this.jugador);
        assertEquals(NOMBRE_JUGADOR_DEFAULT, configuracion.getJugador(NOMBRE_JUGADOR_DEFAULT).getNombre());
    }

    @Test
    public void testAgregar1JugadorEsPepe() throws Throwable {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        configuracion.inicializar();
        configuracion.agregarJugador(this.jugador);
        assertEquals(NOMBRE_JUGADOR_DEFAULT, configuracion.getJugador(NOMBRE_JUGADOR_DEFAULT).getNombre());
    }

    @Test
    public void testAgregar1JugadorNoTienePuntos() throws Throwable {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        configuracion.inicializar();
        configuracion.agregarJugador(this.jugador);
        assertEquals(0, configuracion.getJugador(NOMBRE_JUGADOR_DEFAULT).getPuntaje());
    }

    @Test
    public void testAgregar1JugadorCreaArchivo() throws JugadorExistenteException, GuardarPuntajesException, ErrorAlCrearJugadorException {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(this.jugador);

        assertTrue(new File(new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR), NOMBRE_JUGADOR_DEFAULT).exists());
    }

    @Test
    public void testAgregar2JugadoresCreaArchivoParaJuan() throws JugadorExistenteException, LongitudNombreJugadorInsuficienteException, GuardarPuntajesException, ErrorAlCrearJugadorException {

        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(this.jugador);
        configuracion.agregarJugador(new Jugador("Juan", this.mapa));

        assertTrue(new File(new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR), "Juan").exists());
    }

    @Test
    public void testAgregar2JugadoresCrea2Archivos() throws JugadorExistenteException, LongitudNombreJugadorInsuficienteException, GuardarPuntajesException, ErrorAlCrearJugadorException {

        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(this.jugador);
        configuracion.agregarJugador(new Jugador("Juan", this.mapa));

        assertEquals(2, new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR).list().length);
    }

    @Test
    public void testAgregarJugadorRepetidoFalla() throws JugadorExistenteException, GuardarPuntajesException, ErrorAlCrearJugadorException {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(this.jugador);
        try {
            configuracion.agregarJugador(jugador);
            fail("El nombre del Jugador está repetido");
        }
        catch( JugadorExistenteException e) {
            // OK, debe tirar esta excepción
        }

    }

    @Test
    public void testAgregarJugadorRepetidoFallaYNoCreaArchivoNuevo()
            throws JugadorExistenteException, ErrorAlCrearJugadorException, GuardarPuntajesException
    {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(this.jugador);
        try {
            configuracion.agregarJugador(jugador);
        }
        catch (JugadorExistenteException e) {
            // OK
        }

        assertEquals(1, new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR).list().length);
    }

    //~~---------------------------------------------------------- Borrado de jugadores

    @Test
    public void testBorrarJugadorExistenteBorraArchivo() throws JugadorExistenteException, ErrorAlCrearJugadorException, ErrorAlObtenerPuntajes, GuardarPuntajesException, JugadorInexistenteException {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(jugador);
        configuracion.borrarJugador(NOMBRE_JUGADOR_DEFAULT);

        assertFalse(new File(new File(directorioDeconfiguracion, DIRECTORIO_JUGADOR), NOMBRE_JUGADOR_DEFAULT).exists());
    }

    @Test( expected = ErrorAlObtenerJugadorException.class )
    public void testBorrarJugadorExistenteNoExisteAlBuscarlo() throws GuardarPuntajesException, ErrorAlObtenerPuntajes, JugadorExistenteException, ErrorAlCrearJugadorException, ErrorAlObtenerJugadorException, JugadorInexistenteException {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.agregarJugador(jugador);
        configuracion.borrarJugador(NOMBRE_JUGADOR_DEFAULT);

        configuracion.getJugador(NOMBRE_JUGADOR_DEFAULT);
    }

    @Test( expected = JugadorInexistenteException.class )
    public void testBorrarJugadorInExistenteDaError() throws GuardarPuntajesException, ErrorAlObtenerPuntajes, JugadorExistenteException, ErrorAlCrearJugadorException, ErrorAlObtenerJugadorException, JugadorInexistenteException {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        configuracion.borrarJugador(NOMBRE_JUGADOR_DEFAULT);
    }


    //~~---------------------------------------------------------- Puntajes


    @Test
    public void testCreacionSinPuntajes() throws GuardarPuntajesException, ErrorAlObtenerPuntajes {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }

        assertTrue( configuracion.getPuntajes().isEmpty() );
    }

    @Test
    public void testGuardarPuntajeHayUnoMas() throws GuardarPuntajesException, ErrorAlObtenerPuntajes {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }

        Map<String,Integer> puntajes = new HashMap<String,Integer>();
        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 10 );
        configuracion.guardarPuntajes(puntajes);

        assertEquals(1, configuracion.getPuntajes().size());
    }

    @Test
    public void testGuardarPuntajeDosVecesElMismoHaySoloUno()
            throws GuardarPuntajesException, ErrorAlObtenerPuntajes
    {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }

        Map<String,Integer> puntajes = new HashMap<String,Integer>();
        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 10 );
        configuracion.guardarPuntajes(puntajes);

        puntajes.put(NOMBRE_JUGADOR_DEFAULT, 20);
        configuracion.guardarPuntajes(puntajes);

        assertEquals(1, configuracion.getPuntajes().size());
    }

    @Test
    public void testGuardarPuntajeDosVecesElMismoGuardaSoloElUltimo()
            throws GuardarPuntajesException, ErrorAlObtenerPuntajes
    {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        Map<String,Integer> puntajes = new HashMap<String,Integer>();

        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 10 );
        configuracion.guardarPuntajes(puntajes);

        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 20 );
        configuracion.guardarPuntajes(puntajes);

        assertEquals(20, (int) configuracion.getPuntajes().get("Pepe"));
    }

    @Test
    public void testGuardarPuntajeGuardaNombre() throws GuardarPuntajesException, ErrorAlObtenerPuntajes {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        Map<String,Integer> puntajes = new HashMap<String,Integer>();
        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 10 );
        configuracion.guardarPuntajes( puntajes );

        assertTrue( configuracion.getPuntajes().keySet().contains(NOMBRE_JUGADOR_DEFAULT) );
    }

    @Test
    public void testGuardarPuntajeGuardaPuntos() throws GuardarPuntajesException, ErrorAlObtenerPuntajes {
        AlmacenamientoPersistente configuracion = new AlmacenamientoPersistente(directorioDeconfiguracion);
        try
        {
            configuracion.inicializar();
        } catch (DirectorioDeConfiguracionNoInicializadoException e)
        {
            e.printStackTrace();
        }
        Map<String,Integer> puntajes = new HashMap<String,Integer>();
        puntajes.put( NOMBRE_JUGADOR_DEFAULT, 10 );
        configuracion.guardarPuntajes( puntajes );

        assertEquals( 10, (int) configuracion.getPuntajes().get("Pepe") );
    }

}
