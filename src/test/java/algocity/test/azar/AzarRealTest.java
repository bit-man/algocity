package algocity.test.azar;

import algocity.modelo.azar.AzarReal;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

public class AzarRealTest {

    private final int CANT_VALORES_A_PROBAR = 1000;

    @Test
    public void testConstructorNoDevuelveNull() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {
        assertNotNull(new AzarReal());
    }


    @Test
    public void testLlamadasDevuelvenDistintosValores() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {

        final int CANT_VALORES_A_PROBAR = 100;

        AzarReal azar = new AzarReal();
        List<Float> valorDevuelto = new ArrayList<Float>(CANT_VALORES_A_PROBAR);
        for( int i = 0; i < 10; i++) {
            float nuevoValor = azar.nuevoValor((float) 0.0, (float) 1.0);
            assertFalse("El valor " + nuevoValor + " ya fue generado", valorDevuelto.contains(nuevoValor));
            valorDevuelto.add(nuevoValor);
        }
    }

    @Test
    public void testLlamadasDevuelvenValoresEntre0y1() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {
        probarMaxyMin((float) 0.0, (float) 1.0);
    }

    @Test
    public void testLlamadasDevuelvenValoresEntre10y33() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {
        probarMaxyMin((float) 10.0, (float) 33.0);
    }

    @Test
    public void testLlamadasDevuelvenValoresEntre0y1Int() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {
        probarMaxyMin(0, 5);
    }

    @Test
    public void testLlamadasDevuelvenValoresEntre10y33Int() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException {
        probarMaxyMin(10,  33);
    }

    private void probarMaxyMin(float min, float max) {

        AzarReal azar = new AzarReal();
        for( int i = 0; i < CANT_VALORES_A_PROBAR; i++) {
            float nuevoValor = azar.nuevoValor(min, max);
            assertTrue("azar :" + nuevoValor + " , min : " + min, nuevoValor >= min);
            assertTrue("azar :" + nuevoValor + " , max : " + max, nuevoValor < max);
        }
    }

    private void probarMaxyMin(int min, int max) {
        AzarReal azar = new AzarReal();
        for( int i = 0; i < CANT_VALORES_A_PROBAR; i++) {
            int nuevoValor = azar.nuevoValor(min, max);
            assertTrue("azar :" + nuevoValor + " , min : " + min, nuevoValor >= min);
            assertTrue("azar :" + nuevoValor + " , max : " + max, nuevoValor < max);
        }
    }

}
