package algocity.test.azar.util;


import algocity.modelo.azar.*;
import algocity.modelo.juego.Parametros;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ConstructorAzarSimulado {
    private int numHectareas;
    private List<AzarControlado> azares;

    public ConstructorAzarSimulado() {
        numHectareas = 100;
        this.azares = new ArrayList<AzarControlado>();
    }

    public ConstructorAzarSimulado agregarHectareas(int numHectareas) {
        this.numHectareas = numHectareas;
        return this;
    }

    public ConstructorAzarSimulado todoTierra() throws AzarSimuladoElementoFueraDeRangoException {
        azares.add( new LimitadorAzar(numHectareas, new AzarSimulado(Parametros.UMBRAL_DE_TIERRA - (float) 0.01)));
        return this;
    }

    public ConstructorAzarSimulado todoAgua() throws AzarSimuladoElementoFueraDeRangoException {
        azares.add( new LimitadorAzar(numHectareas, new AzarSimulado(Parametros.UMBRAL_DE_TIERRA + (float) 0.01)));
        return this;
    }

    public ConstructorAzarSimulado numero(float valor) throws AzarSimuladoElementoFueraDeRangoException {
        azares.add( new AzarSimple( new AzarSimulado(valor) ));
        return this;
    }

    public FuncionAzar crear() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        Collection<Float> valores =  new ArrayList<Float>();

        for ( AzarControlado azar : azares ) {
            while ( azar.hayMas() ) {
                valores.add(azar.valorActual());
                azar.avanzar();
            }
        }

        return new AzarSimulado(valores);
    }
}
