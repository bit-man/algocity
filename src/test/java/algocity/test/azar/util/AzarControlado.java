package algocity.test.azar.util;

public interface AzarControlado {
    public boolean hayMas();

    public void avanzar();

    public float valorActual();
}
