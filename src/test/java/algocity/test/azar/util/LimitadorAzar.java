package algocity.test.azar.util;

import algocity.modelo.azar.FuncionAzar;

public class LimitadorAzar
    implements AzarControlado {

    private final int limite;
    private final FuncionAzar funcionAzar;
    private int cursor;

    public LimitadorAzar(int limite, FuncionAzar funcionAzar) {
        this.limite = limite;
        this.funcionAzar = funcionAzar;
        this.cursor = 0;
    }

    public boolean hayMas() {
        return (cursor < limite);
    }

    public void avanzar() {
        cursor++;
    }

    public float valorActual() {
        return funcionAzar.nuevoValor((float) 0.0, (float) 1.0);
    }
}
