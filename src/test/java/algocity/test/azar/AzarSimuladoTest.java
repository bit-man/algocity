package algocity.test.azar;

import algocity.modelo.azar.AzarSimulado;
import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.azar.AzarSimuladoListaNullException;
import algocity.modelo.azar.AzarSimuladoListaVaciaException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

public class AzarSimuladoTest {

    @Test
    public void testConstructorListaNoNulo() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        List<Float> lista = new ArrayList<Float>();
        lista.add((float) 0.9);
        assertNotNull(new AzarSimulado(lista));
    }

    @Test
    public void testConstructorFloatNoNulo() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        assertNotNull(new AzarSimulado((float) 0.9));
    }

    @Test
    public void testConstructorListaVacia() throws AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        try {
            assertNotNull(new AzarSimulado(new ArrayList<Float>()));
            fail("No puede usarse una lista vacía");
        } catch (AzarSimuladoListaVaciaException e) {
            // OK
        }
    }

    @Test
    public void testConstructorListaNull() throws AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        try {
            assertNotNull(new AzarSimulado(null));
            fail("No puede usarse una referencia null a una lista");
        } catch (AzarSimuladoListaNullException e) {
            // OK
        }
    }

    @Test(expected = AzarSimuladoElementoFueraDeRangoException.class)
    public void testConstructorElementoGrande() throws AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException, AzarSimuladoListaVaciaException {
        ArrayList<Float> lista = new ArrayList<Float>();
        lista.add((float) 10);
        new AzarSimulado(lista);
    }

    @Test
    public void testConstructorGetElementoGrande() throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException {
        ArrayList<Float> lista = new ArrayList<Float>();
        lista.add((float) 10);
        try {
            new AzarSimulado(lista);
            fail("Elemento fuera de rango no detectado");
        } catch (AzarSimuladoElementoFueraDeRangoException e) {
            assertEquals((float) 10, e.getElemento());
        }
    }

    @Test
    public void testUnicoValorConConstructorNoLista() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        AzarSimulado azarSimulado = new AzarSimulado((float) 0.9 );

        assertEquals((float) 9, azarSimulado.nuevoValor((float) 0.0, (float) 10.0));
        assertEquals((float) 9, azarSimulado.nuevoValor((float) 0.0, (float) 10.0));
    }

    @Test
    public void testUnicoValorLista() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        List<Float> lista = new ArrayList<Float>();
        lista.add((float) 0.9);
        AzarSimulado azarSimulado = new AzarSimulado(lista);

        assertEquals((float) 9, azarSimulado.nuevoValor((float) 0.0, (float) 10.0));
        assertEquals((float) 9, azarSimulado.nuevoValor((float) 0.0, (float) 10.0));
    }

    @Test
    public void testDosValoresLista() throws AzarSimuladoListaVaciaException, AzarSimuladoListaNullException, AzarSimuladoElementoFueraDeRangoException {
        List<Float> lista = new ArrayList<Float>();
        lista.add((float) 0.9);
        lista.add((float) 0.75);
        AzarSimulado azarSimulado = new AzarSimulado(lista);

        assertEquals((float) 0.9, azarSimulado.nuevoValor((float) 0.0, (float) 1.0));
        assertEquals((float) 0.75, azarSimulado.nuevoValor((float) 0.0, (float) 1.0));
        assertEquals((float) 0.9, azarSimulado.nuevoValor((float) 0.0, (float) 1.0));
    }

    @Test
    public void testBooleanRandomFalse() throws AzarSimuladoElementoFueraDeRangoException {
        AzarSimulado azar = new AzarSimulado( (float) 0.9);
        assertFalse(azar.nuevoBoolean());
    }

    @Test
    public void testBooleanRandomTrue() throws AzarSimuladoElementoFueraDeRangoException {
        AzarSimulado azar = new AzarSimulado( (float) 0.1);
        assertTrue(azar.nuevoBoolean());
    }


    @Test
    public void testRandomEntero() throws AzarSimuladoElementoFueraDeRangoException {
        AzarSimulado azar = new AzarSimulado( (float) 0.3 );
        assertEquals(2, azar.nuevoValor(0, 9));
    }

    @Test
    public void testRandomEnteroNuncaEsMaximo() throws AzarSimuladoElementoFueraDeRangoException {
        AzarSimulado azar = new AzarSimulado( (float) 0.9999999 );
        assertEquals(8, azar.nuevoValor( 0, 9));
    }
}
