package algocity.vista;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.OperadorDeregistrable;

import java.awt.*;

public class DibujadorMapaAbstracto
        extends OperadorDeregistrable<Hectarea>
        implements  DibujadorMapa {


    protected Graphics2D contextoGrafico;


    public void setContextoGrafico(Graphics2D contextoGrafico) {
        this.contextoGrafico = contextoGrafico;
    }

    @Override
    public void postScan() {

    }


    @Override
    public Boolean operar(Hectarea parametro) {
        return Boolean.TRUE;
    }
}
