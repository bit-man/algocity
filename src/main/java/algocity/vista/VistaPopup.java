package algocity.vista;

import javax.swing.*;

public class VistaPopup
    extends JPopupMenu
    implements PopupVista {
    public VistaPopup() {
    }

    public void mostar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
}
