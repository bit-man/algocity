package algocity.vista;


import algocity.controlador.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HabilitaPanelDeJugador
        extends JFrame implements ActionListener
 {

    // configurar GUI
    public HabilitaPanelDeJugador(String textoAccion, LlamadorAction controlador, CambiadorVista cambiador)
    {
       super( VistaParametros.NOMBRE_DEL_JUEGO );
      cambiador.setPrender(this);
       Container contenedor = getContentPane();
       contenedor.setLayout( new FlowLayout() );
       JFrame menuNuevoJugador = new JFrame();
       Dimension dimension = new Dimension();
       menuNuevoJugador.setLocation((int) ((dimension.getWidth() / 2) + 290), 50);
       
    
       
       // crear campo de texto con tama�o predeterminado
       JTextField nombrejugador = new JTextField(20);
       contenedor.add(nombrejugador);
 
       // crear campo de texto con texto predeterminado
       JLabel mensaje = new JLabel("NOMBRE JUGADOR");
       contenedor.add(mensaje);

       JButton comienzo = new JButton(textoAccion);
       controlador.setNombre(nombrejugador);
       comienzo.addActionListener(controlador);
      

        JButton volver = new JButton("VOLVER");
       
       volver.addActionListener(
               new VueltaAtrasAccion(cambiador.inverso())
       );
       
       mensaje.setForeground(Color.blue);
       nombrejugador.setForeground(Color.blue);
       
       contenedor.add(comienzo, BorderLayout.NORTH);
       contenedor.add(volver, BorderLayout.CENTER);
       
       contenedor.add(new InsertarImagenes("src/main/java/ciudad2.jpg"));
       setSize(400, 400);
       setLocationRelativeTo(null);
       cambiador.cambiar();
 
    } 

	@Override
	public void actionPerformed(ActionEvent arg0) {
				
	}
 
 }


