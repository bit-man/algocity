package algocity.vista;

import javax.swing.*;

public class Mensajero
        implements MensajeroEstado {

    private final JPanel panelMensajes;

    public Mensajero(JPanel panelMensajes) {
        this.panelMensajes = panelMensajes;
    }

    @Override
    public void agregarMensaje(String mensaje) {
        panelMensajes.add(new Rotulo<String>(mensaje));
        panelMensajes.revalidate();
    }
}
