package algocity.vista;

import java.awt.event.ActionListener;

public interface ControlVista
    extends Pantallero {
    public void registrarModoPruebas(ActionListener listener);
}
