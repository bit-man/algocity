package algocity.vista;

public class FormateadorAnchoVariable
        implements Formateador {
    private Integer ancho;

    public FormateadorAnchoVariable(Integer ancho) {
        this.ancho = ancho;
    }

    @Override
    public String forzarAncho(String valor) {
            return  "<html><div WIDTH=" + ancho + ">" + valor + "</html>";

    }
}
