package algocity.vista;


import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;

public class PopupObserver
        extends OperadorDeregistrable<String>
    implements Operador<String> {
    private final PopupVista popupVista;

    public PopupObserver(PopupVista popupVista) {
        this.popupVista = popupVista;
    }

    @Override
    public Boolean operar(String parametro) {
        popupVista.mostar(parametro);

        return Boolean.TRUE;
    }
}
