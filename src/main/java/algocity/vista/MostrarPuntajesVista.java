package algocity.vista;

import javax.swing.*;
import java.util.Map;

public class MostrarPuntajesVista
{

    public MostrarPuntajesVista() {
        JFrame frame = new JFrame(VistaParametros.NOMBRE_DEL_JUEGO);
        JPanel panel = new JPanel();
        frame.getContentPane().add(panel);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setOpaque(true);
        panel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Puntajes"),
                VistaParametros.BORDE_PANELES));

        try
        {
            final Map<String, Integer> puntajes = MenuAlgoCity.juego().puntajes();

            for ( String jugador : puntajes.keySet() )  {
                panel.add(
                        new JLabel(jugador + " : " + puntajes.get(jugador))
                );
            }

            frame.pack();
            frame.setVisible(true);

        } catch (Throwable ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
        }
    }
}
