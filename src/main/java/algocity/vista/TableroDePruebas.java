package algocity.vista;

import algocity.controlador.AvanzarTiempo;
import algocity.controlador.GenerarGodzilla;
import algocity.controlador.GenerarTerremoto;
import algocity.controlador.LlamarBomberos;
import algocity.modelo.azar.AzarReal;
import algocity.modelo.catastrofe.GodzillaDiagonal;
import algocity.modelo.catastrofe.GodzillaLineal;
import algocity.modelo.catastrofe.GodzillaZigZag;
import algocity.modelo.juego.Jugador;

import javax.swing.*;
import java.awt.*;

public class TableroDePruebas
        extends  EstadoBase {

    private static final int FACTOR_DE_MULTIPLICACION = 10;
    private final Jugador jugador;
    private final Mensajero mensajero;

    public TableroDePruebas(Jugador jugador, Mensajero mensajero) {
        super();
        this.jugador = jugador;
        this.mensajero = mensajero;
    }

    @Override
    protected void inicializarPantalla(JFrame pantalla) {

        //~~----------------------------------------------------- Panel principal
        JPanel panel = new JPanel();
        panel.setOpaque(true);
        panel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createTitledBorder("Pruebas"),
                        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        //~~----------------------------------------------------- Botón de avance
        JButton avanzar = new JButton("Avanzar Tiempo");
        avanzar.addActionListener(new AvanzarTiempo(this.jugador));
        avanzar.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton avanzarRapido = new JButton("Avanzar Tiempo Rápido");
        // ToDo hacer que el avanze sea configurable con un slider
        avanzarRapido.addActionListener(new AvanzarTiempo(this.jugador, FACTOR_DE_MULTIPLICACION));
        avanzarRapido.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton terremoto = new JButton("Terremoto");
        terremoto.addActionListener(
                new GenerarTerremoto(
                        jugador.getMapa(),
                        mensajero
                )
        );
        terremoto.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton godzilla = new JButton("Godzilla Lineal");
        godzilla.addActionListener(
                new GenerarGodzilla(
                        new GodzillaLineal(jugador.getMapa(), new AzarReal()),
                        mensajero
                )
        );
        godzilla.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton godzillaZigZag = new JButton("Godzilla ZigZag");
        godzillaZigZag.addActionListener(
                new GenerarGodzilla(
                        new GodzillaZigZag(jugador.getMapa(), new AzarReal()),
                        mensajero
                )
        );
        godzillaZigZag.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton godzillaDiagonal = new JButton("Godzilla Diagonal");
        godzillaDiagonal.addActionListener(
                new GenerarGodzilla(
                        new GodzillaDiagonal(jugador.getMapa(), new AzarReal()),
                        mensajero
                )
        );
        godzillaDiagonal.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton bomberos = new JButton("Bomberos");
        bomberos.addActionListener(
                new LlamarBomberos(
                        jugador,
                        mensajero
                )
        );
        bomberos.setAlignmentX(Component.CENTER_ALIGNMENT);


        panel.add(avanzar);
        panel.add(avanzarRapido);
        panel.add(terremoto);
        panel.add(godzilla);
        panel.add(godzillaDiagonal);
        panel.add(godzillaZigZag);
        panel.add(bomberos);
        pantalla.getContentPane().add(panel);

        pantalla.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
