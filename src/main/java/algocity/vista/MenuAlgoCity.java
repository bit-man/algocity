package algocity.vista;


import algocity.controlador.*;
import algocity.modelo.almacenamiento.AlmacenamientoPersistente;
import algocity.modelo.almacenamiento.Persistible;
import algocity.modelo.juego.DirectorioDeConfiguracionNoInicializadoException;
import algocity.modelo.juego.Juego;
import algocity.modelo.juego.Parametros;

import javax.swing.*;
import java.awt.*;
 
public class MenuAlgoCity {

    private static Juego juego;


	public MenuAlgoCity(){

		JFrame panel = new JFrame(VistaParametros.NOMBRE_DEL_JUEGO);
		panel.setLayout(new BorderLayout());//Configurar como se dispondra el espacio del jframe

		      //Instanciando botones con texto
			JButton inicio = new JButton("INICAR PARTIDA");
		    JButton  carga = new JButton("CONTINUAR PARTIDA");
			JButton puntaje = new JButton("VER PUNTAJES");
		    JButton salida = new JButton ("SALIDA");
		    panel.add(new InsertarImagenes("src/main/java/ciudad.png"));

		      inicio.setForeground(Color.blue);
		      carga.setForeground(Color.blue);
		      puntaje.setForeground(Color.blue);
		      salida.setForeground(Color.blue);
		      

		      //agregando los botones a la ventana
		      panel.add(inicio, BorderLayout.PAGE_START);
		      panel.add(carga, BorderLayout.EAST);
		      panel.add(puntaje, BorderLayout.WEST);
		      panel.add(salida, BorderLayout.PAGE_END);
		      
		      inicio.setAlignmentX(Component.CENTER_ALIGNMENT);
		      carga.setAlignmentX(Component.CENTER_ALIGNMENT);
		      puntaje.setAlignmentX(Component.CENTER_ALIGNMENT);
		      salida.setAlignmentX(Component.CENTER_ALIGNMENT);
				

		      //a�adiendo el listener a los botones para manipular los eventos del click
		      inicio.addActionListener(new PasajeAMenuJugador(panel));
		      carga.addActionListener(new ManejadorCampoTexto("COMENZAR !",
					  new LlamadorAction(
							  new ContinuarPartidaAccion()
					  ),
					  new IrANuevaVista(panel)));

		      puntaje.addActionListener( new MostrarPuntajesAction());
		      salida.addActionListener(new ForzarSalida(panel));

		      panel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//finaliza el programa cuando se da click en la X
		      panel.setResizable(false);//para configurar si se redimensiona la ventana
		      panel.setSize(400, 400);//configurando tama�o de la ventana (ancho, alto)
		      panel.setLocationRelativeTo(null);
		      panel.setVisible(true);//configurando visualizaci�n de la venta
		  }

    public static Juego juego()
    {
        return juego;
    }

    public static void main(String[] args)
			throws DirectorioDeConfiguracionNoInicializadoException
	{
        Persistible almacenamiento = new AlmacenamientoPersistente(Parametros
                .DIRECTORIO_ALMACENAMIENTO_CONFIGURACION);
        almacenamiento.inicializar();

        juego = new Juego(almacenamiento);

        new MenuAlgoCity();
    }

}





