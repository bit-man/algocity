package algocity.vista;

import algocity.controlador.*;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.JugadorExistenteException;
import algocity.modelo.juego.JugadorInexistenteException;
import algocity.modelo.juego.LongitudNombreJugadorInsuficienteException;

import javax.swing.*;
import java.awt.*;

public class MenuJugador {

	public MenuJugador() throws LongitudNombreJugadorInsuficienteException, JugadorExistenteException, JugadorInexistenteException, JuegoNoIniciadoException
      {//constructor de la clase


		  JFrame ventana = new JFrame(VistaParametros.NOMBRE_DEL_JUEGO);
		  ventana.setLayout(new BorderLayout());//Configurar como se dispondra el espacio del jframe

		  //Instanciando botones con texto
		  JButton nuevojugador = new JButton("JUGADOR NUEVO");
		  JButton existentejugador = new JButton("JUGADOR EXISTENTE");
		  JButton volver = new JButton(" VOLVER ");

	      //margenes para texto en boton
	      nuevojugador.setMargin(new Insets(3, 5, 3, 5));
	      existentejugador.setMargin(new Insets(1, 1, 1, 1));
	      volver.setMargin(new Insets(1, 2, 3, 6));
	      
	      nuevojugador.setForeground(Color.blue);
	      existentejugador.setForeground(Color.blue);
	      volver.setForeground(Color.blue);
	      
	      //agregando los botones a la ventana
	      ventana.add(nuevojugador, BorderLayout.PAGE_START);
	      ventana.add(existentejugador, BorderLayout.CENTER);
	      ventana.add(volver, BorderLayout.PAGE_END);
	      
	      nuevojugador.setAlignmentX(Component.CENTER_ALIGNMENT);
	      existentejugador.setAlignmentX(Component.CENTER_ALIGNMENT);
	      volver.setAlignmentX(Component.CENTER_ALIGNMENT);

	      //a�adiendo el listener a los botones para manipular los eventos del click
	      nuevojugador.addActionListener(new ManejadorCampoTexto("COMENZAR !",
				  													new LlamadorAction(
																		new IniciarNuevoUsuario()
																	),
						  new IrANuevaVista(ventana))
		  );
	      volver.addActionListener(new VueltaAlMenuInicial(ventana));

		  existentejugador.addActionListener(new ManejadorCampoTexto("COMENZAR !",
				  new LlamadorAction(
						  new ComenzarJuego()
				  ),
				  new IrANuevaVista(ventana)));

	      ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//finaliza el programa cuando se da click en la X
	      ventana.setResizable(false);//para configurar si se redimensiona la ventana
	      ventana.setSize(400, 400);//configurando tama�o de la ventana (ancho, alto)
	      ventana.setLocationRelativeTo(null);
	      ventana.setVisible(true);//configurando visualizaci�n de la venta
	  }

}
