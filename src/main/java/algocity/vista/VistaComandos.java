package algocity.vista;

import javax.swing.*;
import java.awt.event.ActionListener;

public class VistaComandos
        implements ComandosVista {

    private JCheckBox comandoDePruebas;
    private JCheckBox comandoMostrarConexiones;
    private JButton comandoGuardar;
    private JButton comandoSalirDelJuego;

    public VistaComandos()
    {
        comandoDePruebas = new JCheckBox("Modo pruebas");
        comandoMostrarConexiones = new JCheckBox("Mostrar Conexiones");
        comandoGuardar = new JButton("Guardar juego");
        comandoSalirDelJuego = new JButton("Salir!");
    }

        @Override
        public JCheckBox getComandoDePruebas() {
                return this.comandoDePruebas;
        }

        @Override
        public JCheckBox getComandoMostrarConexiones() {
                return this.comandoMostrarConexiones;
        }

    @Override
    public JButton getComandoGuardar()
    {
        return this.comandoGuardar;
    }

    @Override
    public JButton getComandoSalirDelJuego()
    {
        return this.comandoSalirDelJuego;
    }

    @Override
        public void addActionListenerModoPruebas(ActionListener listener) {
                comandoDePruebas.addActionListener(listener);
        }

        @Override
        public void addActionListenerMostrarConexiones(ActionListener listener) {
                comandoMostrarConexiones.addActionListener(listener);
        }

    @Override
    public void addActionListenerGuardarJuego(ActionListener listener)
    {
        comandoGuardar.addActionListener(listener);
    }

    @Override
    public void addActionListenerSalirDelJuego(ActionListener listener)
    {
        comandoSalirDelJuego.addActionListener(listener);
    }
}
