package algocity.vista;

import algocity.modelo.juego.Filtrable;
import algocity.modelo.zona.Construccion;
import algocity.modelo.zona.SinConstruccion;

import java.awt.*;

public class SelectorIconoConstruccion
        implements Filtrable<Inicial<Construccion,Image>> {

    private final Construccion construccion;

    public SelectorIconoConstruccion(Construccion construccion) {
        this.construccion = construccion;
    }

    @Override
    public boolean aceptado(Inicial<Construccion,Image> parametro) {
        Construccion construccionParametro = parametro.getElemento();

        // Comparación del tipo de construcción
        boolean construccionesIguales = (construccionParametro.esComercial() == construccion.esComercial()) &&
                (construccionParametro.esResidencial() == construccion.esResidencial()) &&
                (construccionParametro.esIndustrial() == construccion.esIndustrial()) &&
                (construccionParametro.esTerrenoBaldio() == construccion.esTerrenoBaldio()) &&
                (construccionParametro.esCentralElectrica() == construccion.esCentralElectrica()) &&
                (construccionParametro.esConstruccion() == construccion.esConstruccion()) &&
                (construccionParametro.esPozoDeAgua() == construccion.esPozoDeAgua()) &&
                (construccionParametro.esEstacionDeBomberos() == construccion.esEstacionDeBomberos());

        // Agregado de la integridad
        construccionesIguales &=
                (estaRota(construccion) == estaRota(construccionParametro));

        // Agregado del tipo de superficie sólo para terreno baldío
        if ( construccionesIguales && construccion.esTerrenoBaldio() && construccionParametro.esTerrenoBaldio() ) {
            construccionesIguales &=
                    ( ((SinConstruccion)construccionParametro).estaSobreAgua() == ((SinConstruccion)construccion).estaSobreAgua() ) &&
                    ( ((SinConstruccion)construccionParametro).estaSobreLlano() == ((SinConstruccion)construccion).estaSobreLlano() );
        }

        return construccionesIguales;
    }

    private boolean estaRota(Construccion construccion)
    {
        return construccion.getIntegridad() < (float) 1.0;
    }
}
