package algocity.vista;

import algocity.modelo.juego.Jugable;
import algocity.modelo.juego.Jugador;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class MenuHectarea
        extends MouseAdapter {

    private final VistaMapa vistaMapa;
    private final MensajeroEstado mensajero;
    private final SelectorHectarea selectorHectarea;
    private Jugable jugador;

    public MenuHectarea(VistaMapa vistaMapa, Jugador jugador, MensajeroEstado mensajero, SelectorHectarea selectorHectarea) {
        this.vistaMapa = vistaMapa;
        this.jugador = jugador;
        this.mensajero = mensajero;
        this.selectorHectarea = selectorHectarea;
    }

    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            mostrar(e);
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            mostrar(e);
    }

    private void mostrar(MouseEvent e){
        Point locationOnScreen = e.getLocationOnScreen();
        if ( vistaMapa.estaEnElMapa(locationOnScreen.getX(), locationOnScreen.getY()) ) {
            ItemsMenuHectarea menu = new ItemsMenuHectarea(locationOnScreen, vistaMapa, jugador, mensajero, selectorHectarea);
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }

}
