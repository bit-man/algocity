package algocity.vista;

import javax.swing.*;
import javax.swing.border.CompoundBorder;

public abstract class EstadoBase
        implements Pantallero  {

    private JFrame pantalla;

    public EstadoBase() {
        pantalla = new JFrame();
    }

    protected CompoundBorder construirBorde(String titulo) {
        return BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(titulo),
                VistaParametros.BORDE_PANELES);
    }

    @Override
    public JFrame crearPantalla() {
        inicializarPantalla(pantalla);
        return pantalla;
    }

    protected abstract void inicializarPantalla(JFrame pantalla);

    public void finalizar() {
        pantalla.dispose();
    }

}
