package algocity.vista;

import javax.swing.*;

public interface Pantallero {
    public JFrame crearPantalla();

    public void finalizar();
}
