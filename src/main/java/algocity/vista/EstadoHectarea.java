package algocity.vista;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.Conexion;

public class EstadoHectarea
    implements HectareaEstado {

    private final EstadoConstruccion estadoConstruccion;
    private final Rotulo<Posicion> posicion;
    private Rotulo<EstadoConstruccion> construccion;
    private Rotulo<Integer> energia;
    private Rotulo<String> conexiones;

    public EstadoHectarea() {
        this.estadoConstruccion = new EstadoConstruccion();
        construccion = new Rotulo<EstadoConstruccion>( "Construcción : " , VistaParametros.ANCHO_ESTADO_HECTAREA);
        energia = new Rotulo<Integer>("Energía : ", VistaParametros.ANCHO_ESTADO_HECTAREA);
        conexiones = new Rotulo<String>("Conexiones : ", VistaParametros.ANCHO_ESTADO_HECTAREA);
        posicion = new Rotulo<Posicion>("Posición : ", VistaParametros.ANCHO_ESTADO_HECTAREA);
    }

    public void actualizar(Hectarea hectarea) {
        estadoConstruccion.setConstruccion(hectarea.getConstruccion());
        construccion.setValor(estadoConstruccion);
        energia.setValor( hectarea.consultarEnergia() );
        posicion.setValor( hectarea.getPosicion());

        String conexionesvalor = "";
        for(Conexion conexion : hectarea.getConexionesDisponibles()) {
            conexionesvalor += "-> " + conexion;
        }
        conexiones.setValor(conexionesvalor);
    }

    public Rotulo<EstadoConstruccion> getConstruccion() {
        return construccion;
    }

    public Rotulo<Integer> getEnergia() {
        return energia;
    }

    public Rotulo<String> getConexiones() {
        return conexiones;
    }

    public Rotulo<Posicion> getPosicion() {
        return posicion;
    }

    public void limpiar() {
        construccion.limpiar();
        energia.limpiar();
    }

    @Override
    public void actualizar() {

    }
}
