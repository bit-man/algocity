package algocity.vista;

import algocity.modelo.zona.Construccion;

import java.awt.*;

public interface Selector {
    public Image seleccionar(Construccion construccion);
}
