package algocity.vista;

public class Inicial<S,T> {

    private final S elemento;
    private final T seleccion;

    public Inicial(S elemento, T seleccion) {
        this.elemento = elemento;
        this.seleccion = seleccion;
    }

    public S getElemento() {
        return elemento;
    }

    public T getSeleccion() {
        return seleccion;
    }
}
