package algocity.vista;

public interface Mensajeable
{
    public Mensajero getMensajero();
}
