package algocity.vista;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.concurrent.TimeUnit;

public class VistaParametros {
    public static final int VELOCIDAD_DEL_JUEGO = (int) TimeUnit.SECONDS.toMillis(5);
    public static final Color COLOR_TUBERIA_DE_AGUA = new Color(0, 0, 255);
    public static final Color COLOR_RUTA_PAVIMENTADA = new Color(0, 0, 0);
    public static final Color COLOR_LINEA_DE_TENSION = new Color(255, 0, 0);
    public static final Border BORDE_PANELES = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    public static final Integer ANCHO_ESTADO_HECTAREA = 250;
    public static final int HECTAREAS_POR_PANTALLA = 30;
    public static final int HECTAREAS_DE_AVANCE_EN_INCREMENTO_POR_BLOQUE = 10;
    public static final int HECTAREAS_DE_AVANCE_EN_INCREMENTO_UNITARIO = 1;
    public static final int ANCHO_FICTICIO_MAPA = 1000;
    public static final int ALTO_FICTICIO_MAPA = 1000;
    public static final String NOMBRE_DEL_JUEGO = "AlgoCity";
}
