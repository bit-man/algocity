package algocity.vista;

import algocity.controlador.GuardarJuegoAction;
import algocity.controlador.MostrarConexionesAction;
import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.juego.Jugador;
import algocity.modelo.mapa.Operador;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class TableroDeControl
    extends  EstadoBase
    implements ControlVista, Mensajeable {

    private final Jugador jugador;
    private ComandosVista comandosVista;
    private Mensajero mensajero;
    private ActionListener salirDelJuegoAction;
    private Operador<Catastrofe> avisadorCatastrofe;
    private JugadorEstado estadoJugador;

    public TableroDeControl(Jugador jugador, ActionListener salirDelJuegoAction) {
        super();
        this.jugador = jugador;
        this.salirDelJuegoAction = salirDelJuegoAction;
    }

    public void registrarModoPruebas(ActionListener listener) {
        comandosVista.addActionListenerModoPruebas(listener);
    }

    protected void inicializarPantalla(JFrame panel) {


        //~~----------------------------------------------------- Panel de mensajes

        // ToDo hacer que haga scroll hasta el final
        JPanel panelMensajes = new JPanel();
        panelMensajes.setLayout(new BoxLayout(panelMensajes, BoxLayout.Y_AXIS));

        JScrollPane scrollMensajero = new JScrollPane(panelMensajes);
        mensajero = new Mensajero(panelMensajes);
        scrollMensajero.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollMensajero.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollMensajero.setOpaque(true);
        scrollMensajero.setBorder(construirBorde("Mensajes"));

        mensajero.agregarMensaje("Bienvenido " + jugador.getNombre() + " !!!");
        this.avisadorCatastrofe = new AvisadorCatastrofe(mensajero);
        jugador.registrarAvisoCatastrofe(avisadorCatastrofe);

        //~~----------------------------------------------------- Panel principal

        JPanel panelPrincipal = new JPanel();
        panelPrincipal.setOpaque(true);
        panelPrincipal.setLayout(new BorderLayout());
        panelPrincipal.setBorder(construirBorde("Jugador : " + jugador.getNombre()));

        //~~----------------------------------------------------- Panel estado hectárea

        JPanel panelHectarea = new JPanel();
        panelHectarea.setOpaque(true);
        panelHectarea.setLayout(new BoxLayout(panelHectarea, BoxLayout.Y_AXIS));
        panelHectarea.setBorder(construirBorde("Hectárea"));

        HectareaEstado estadoHectarea = new EstadoHectarea();

        panelHectarea.add(estadoHectarea.getConstruccion());
        panelHectarea.add(estadoHectarea.getEnergia());
        panelHectarea.add(estadoHectarea.getConexiones());
        panelHectarea.add(estadoHectarea.getPosicion());


        //~~----------------------------------------------------- Mapa
        VistaMapa vistaMapa = new VistaMapa(jugador);
        JScrollPane panelMapa = new JScrollPane(vistaMapa);
        panelMapa.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        panelMapa.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        panelMapa.setOpaque(true);
        panelMapa.setBorder(construirBorde("Mapa"));

        panelMapa.addMouseMotionListener(new MapaListener(vistaMapa, estadoHectarea));
        SelectorHectarea selectorHectarea = new SelectorHectarea(vistaMapa);
        panelMapa.addMouseListener(selectorHectarea);
        panelMapa.addMouseListener(new MenuHectarea(vistaMapa, jugador, mensajero, selectorHectarea));

        JPanel myPanel = new JPanel();
        myPanel.add(panelMapa);

        //~~----------------------------------------------------- Panel de Estado
        // ToDo sale todo pegado
        JPanel panelDeEstado = new JPanel();
        panelDeEstado.setOpaque(true);
        panelDeEstado.setBorder(construirBorde("Estado"));

        this.estadoJugador = new EstadoJugador(jugador);
        panelDeEstado.add(estadoJugador.getTiempo());
        panelDeEstado.add(estadoJugador.getPuntos());
        panelDeEstado.add(estadoJugador.getDinero());
        panelDeEstado.add(estadoJugador.getPoblacion());

        //~~----------------------------------------------------- Panel de comandos
        JPanel panelDeComandos = new JPanel();
        panelDeComandos.setOpaque(true);
        panelDeComandos.setBorder(construirBorde("Comandos"));

        this.comandosVista = new VistaComandos();
        panelDeComandos.add(comandosVista.getComandoDePruebas());

        final JCheckBox comandoMostrarConexiones = comandosVista.getComandoMostrarConexiones();
        panelDeComandos.add(comandoMostrarConexiones);
        comandosVista.addActionListenerMostrarConexiones(new MostrarConexionesAction(vistaMapa));

        panelDeComandos.add(comandosVista.getComandoGuardar());
        comandosVista.addActionListenerGuardarJuego(new GuardarJuegoAction(mensajero));

        panelDeComandos.add(comandosVista.getComandoSalirDelJuego());
        comandosVista.addActionListenerSalirDelJuego(salirDelJuegoAction);

        //~~----------------------------------------------------- Coordinación paneles
        panel.getContentPane().add(panelPrincipal);
        panelPrincipal.add(panelDeEstado, BorderLayout.PAGE_START);
        panelPrincipal.add(panelDeComandos, BorderLayout.PAGE_END);
        panelPrincipal.add(panelMapa, BorderLayout.CENTER);
        panelPrincipal.add(panelHectarea, BorderLayout.LINE_END);
        panelPrincipal.add(scrollMensajero, BorderLayout.BEFORE_LINE_BEGINS);
        panel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public Mensajero getMensajero() {
        return mensajero;
    }
    public void finalizar() {
        super.finalizar();
        jugador.deregistrarAvisoCatastrofe(avisadorCatastrofe);
        estadoJugador.finalizar();
    }

}
