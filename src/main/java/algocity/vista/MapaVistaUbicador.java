package algocity.vista;

import algocity.modelo.mapa.Hectarea;

import java.awt.*;

public interface MapaVistaUbicador {
    public boolean estaEnElMapa(double xEnPantalla, double yEnPantalla);

    public Hectarea getHectarea(Point locationOnScreen);
}
