package algocity.vista;

import javax.swing.*;

public class VistaPopupTimer
        extends VistaPopup {

    private final Timer timer;

    public VistaPopupTimer(Timer timer) {
        this.timer = timer;
    }

    public void mostar(String mensaje) {
        timer.stop();
        super.mostar(mensaje);
    }
}
