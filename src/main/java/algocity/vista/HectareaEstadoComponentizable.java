package algocity.vista;

import algocity.modelo.mapa.Posicion;

public interface HectareaEstadoComponentizable {
    public Rotulo<EstadoConstruccion> getConstruccion();

    public Rotulo<Integer> getEnergia();

    public Rotulo<String> getConexiones();

    public Rotulo<Posicion> getPosicion();
}
