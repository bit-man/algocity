package algocity.vista;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.mapa.OperadorDeregistrable;

public class AdaptadorMensajeroCatastrofes
    extends OperadorDeregistrable<Catastrofe>
{

    private final MensajeroEstado mensajero;

    public AdaptadorMensajeroCatastrofes(MensajeroEstado mensajero) {
        this.mensajero = mensajero;
    }

    @Override
    public Boolean operar(Catastrofe catastrofe) {
        mensajero.agregarMensaje(catastrofe + " en " + catastrofe.getPosicionDaniada());
        return Boolean.TRUE;
    }
}
