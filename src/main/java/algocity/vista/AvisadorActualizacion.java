package algocity.vista;

import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.OperadorDeregistrable;


public class AvisadorActualizacion
        extends OperadorDeregistrable<ObjetoNulo>
{

    private final ActualizableVista<Hectarea> vistaMapa;

    public AvisadorActualizacion(ActualizableVista<Hectarea> vistaMapa) {
        this.vistaMapa = vistaMapa;
    }

    @Override
    public Boolean operar(ObjetoNulo parametro) {
        vistaMapa.actualizar(null);

        return Boolean.TRUE;
    }
}
