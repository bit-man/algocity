package algocity.vista;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Operador;

import java.awt.*;

public interface DibujadorMapa
    extends Operador<Hectarea>{

    public void setContextoGrafico(Graphics2D g2);

    public void postScan();
}
