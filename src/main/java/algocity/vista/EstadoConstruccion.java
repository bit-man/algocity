package algocity.vista;

import algocity.modelo.zona.Construccion;

public class EstadoConstruccion {

    private Construccion construccion;

    public void setConstruccion(Construccion construccion) {
        this.construccion = construccion;
    }

    public String toString() {
        return construccion.toString() +
                (construccion.esTerrenoBaldio() ? "" : " (" + Math.round(construccion.getIntegridad() * 100) +"%)" );
    }

}
