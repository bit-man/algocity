package algocity.vista;

import algocity.modelo.juego.Filtrable;
import algocity.modelo.necesidades.ComparadorNecesidad;
import algocity.modelo.necesidades.Conexion;

import java.awt.*;

public class SelectorColorConexion
        implements Filtrable<Inicial<Conexion, Color>> {

    private final Conexion conexion;

    public SelectorColorConexion(Conexion conexion) {
        this.conexion = conexion;
    }

    @Override
    public boolean aceptado(Inicial<Conexion, Color> parametro) {
        Conexion conexion1 = parametro.getElemento();
        return ComparadorNecesidad.comparar(conexion1, conexion);
    }

}
