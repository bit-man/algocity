package algocity.vista;


import algocity.modelo.juego.Jugable;

public class EstadoJugador
    implements JugadorEstado {

    private final Rotulo<Integer> rotuloTiempo;
    private final Rotulo<Integer> rotuloPuntos;
    private final Rotulo<Integer> rotuloDinero;
    private final Jugable jugador;
    private final Rotulo<Integer> rotuloPoblacion;
    private final NotificarVista notificador;

    public EstadoJugador(Jugable jugador){
        notificador = new NotificarVista(this);
        jugador.registrarAvanceTiempo(notificador);
        rotuloTiempo = new Rotulo<Integer>("Tiempo : ");
        rotuloPuntos = new Rotulo<Integer>("Puntos : ");
        rotuloDinero = new Rotulo<Integer>("Dinero : ");
        rotuloPoblacion = new Rotulo<Integer>("Población : ");
        this.jugador = jugador;
    }

    @Override
    public void actualizar() {
        rotuloPuntos.setValor( jugador.getPuntaje() );
        rotuloDinero.setValor( jugador.getDinero() );
        rotuloTiempo.setValor( jugador.getTiempoActual() );
        rotuloPoblacion.setValor( jugador.getMapa().getPoblacion() );
    }

    public Rotulo<Integer> getTiempo() {
        return rotuloTiempo;
    }

    public Rotulo<Integer> getPuntos() {
        return rotuloPuntos;
    }

    public Rotulo<Integer> getDinero() {
        return rotuloDinero;
    }

    @Override
    public Rotulo<Integer> getPoblacion() {
        return rotuloPoblacion;
    }

    @Override
    public void finalizar()
    {
        jugador.deregistrarAvanceDeTiempo(notificador);
    }
}
