package algocity.vista;

public interface MapaColector {
    public double getLadoHectarea();

    public void colectarLimite(double xRelativoHectarea00, double yRelativoHectarea00);

}
