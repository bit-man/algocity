package algocity.vista;

import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;

public class PopupFinalizacionObserver
        extends OperadorDeregistrable<Object>
        implements Operador<Object> {

    private final PopupVista vistaPopup;
    private String mensaje;

    public PopupFinalizacionObserver(PopupVista vistaPopup, String mensaje) {
        this.vistaPopup = vistaPopup;
        this.mensaje = mensaje;
    }

    @Override
    public Boolean operar(Object parametro) {
        vistaPopup.mostar(mensaje);
        return Boolean.TRUE;
    }
}
