package algocity.vista;

import algocity.modelo.mapa.Hectarea;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MapaListener
        implements MouseMotionListener {


    private final MapaVistaUbicador mapaVista;
    private final ActualizableVista<Hectarea> estadoHectarea;

    public MapaListener(MapaVistaUbicador mapaUbicador, ActualizableVista<Hectarea> estadoHectarea) {
        this.mapaVista = mapaUbicador;
        this.estadoHectarea = estadoHectarea;
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point locationOnScreen = e.getLocationOnScreen();
        final double xOnScreen = locationOnScreen.getX();
        final double yOnScreen = locationOnScreen.getY();

        if ( mapaVista.estaEnElMapa(xOnScreen, yOnScreen) ) {
            estadoHectarea.actualizar(mapaVista.getHectarea(locationOnScreen));
        } else {
            estadoHectarea.limpiar();
        }
    }
}
