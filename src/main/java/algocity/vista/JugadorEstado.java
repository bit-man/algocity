package algocity.vista;

public interface JugadorEstado
{
    public void actualizar();

    public Rotulo<Integer> getTiempo();

    public Rotulo<Integer> getPuntos();

    public Rotulo<Integer> getDinero();

    public Rotulo<Integer> getPoblacion();

    public void finalizar();
}
