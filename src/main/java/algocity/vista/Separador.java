package algocity.vista;

import javax.swing.*;

public class Separador
    extends JMenuItem{

    public Separador() {
        super("----------------------");
        setEnabled(false);
    }
}
