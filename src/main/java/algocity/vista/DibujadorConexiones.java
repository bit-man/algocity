package algocity.vista;

import algocity.modelo.juego.ColeccionesAlgoCity;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.TuberiaDeAgua;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class DibujadorConexiones
        extends DibujadorMapaAbstracto {

    private final VistaMapa vistaMapa;
    private Collection<Conexion> conexiones;;
    private Collection<Inicial<Conexion,Color>> iniciales;

    public Collection<Conexion> getConexiones() {
        return conexiones;
    }

    public DibujadorConexiones(VistaMapa vistaMapa) {
        this.vistaMapa = vistaMapa;
        this.conexiones = new HashSet<Conexion>();
        this.iniciales = new ArrayList<Inicial<Conexion,Color>>();

        iniciales.add( new Inicial<Conexion,Color>( new TuberiaDeAgua(null), VistaParametros.COLOR_TUBERIA_DE_AGUA) );
        iniciales.add( new Inicial<Conexion,Color>( new RutaPavimentada(null), VistaParametros.COLOR_RUTA_PAVIMENTADA) );
        iniciales.add( new Inicial<Conexion,Color>( new LineaDeTension(null), VistaParametros.COLOR_LINEA_DE_TENSION) );
    }


    @Override
    public Boolean operar(Hectarea hectarea) {
        for ( Conexion conexion : hectarea.getConexionesDisponibles()) {
            this.conexiones.add(conexion);
        }
        return Boolean.TRUE;
    }

    @Override
    public void postScan() {
        for( Conexion conexion : conexiones ) {
            contextoGrafico.setPaint(elegirColor(conexion));

            double ladoHectarea = vistaMapa.getLadoHectarea();

            // Posición de la hectárea relativa a la primer hectárea
            final Posicion posicionOrigen = conexion.getOrigen().getPosicion();
            double xRelativoHectarea00origen = (posicionOrigen.getX() * ladoHectarea) + (ladoHectarea / 2);
            double yRelativoHectarea00origen = (posicionOrigen.getY() * ladoHectarea) + (ladoHectarea / 2);

            // Posición de la hectárea relativa a la primer hectárea
            final Posicion posicionDestino = conexion.getDestino().getPosicion();
            double xRelativoHectarea00destino = ( posicionDestino.getX() * ladoHectarea ) + (ladoHectarea / 2);
            double yRelativoHectarea00destino = ( posicionDestino.getY() * ladoHectarea ) + (ladoHectarea / 2);

            Line2D.Double conevionView = new Line2D.Double(
                    xRelativoHectarea00origen, yRelativoHectarea00origen,
                    xRelativoHectarea00destino, yRelativoHectarea00destino
                );
            contextoGrafico.draw(conevionView);
        }
    }

    private Color elegirColor(Conexion conexion) {
        return ColeccionesAlgoCity.filtrar(this.iniciales, new SelectorColorConexion(conexion))
                .iterator().next().getSeleccion();
    }

}
