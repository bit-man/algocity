package algocity.vista;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.mapa.OperadorDeregistrable;

public class AvisadorCatastrofe
        extends OperadorDeregistrable<Catastrofe>
{
    private final Mensajero mensajero;

    public AvisadorCatastrofe(Mensajero mensajero) {
        this.mensajero = mensajero;
    }

    @Override
    public Boolean operar(Catastrofe catastrofe) {
        mensajero.agregarMensaje( catastrofe + " en " + catastrofe.getPosicionDaniada() );
        return Boolean.TRUE;
    }
}
