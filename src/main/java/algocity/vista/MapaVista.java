package algocity.vista;

import algocity.modelo.mapa.Hectarea;

public interface MapaVista
    extends  MapaEstado, MapaColector , MapaVistaUbicador, ActualizableVista<Hectarea> {
}
