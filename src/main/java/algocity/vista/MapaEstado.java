package algocity.vista;

public interface MapaEstado {
    public double getxMin();

    public double getyMin();

    public double getxMax();

    public double getyMax();

}
