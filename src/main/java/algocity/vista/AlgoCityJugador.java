package algocity.vista;

import algocity.controlador.AvanzarTiempo;
import algocity.controlador.HabilitaPanelDePruebas;
import algocity.controlador.SalirDelJuegoAction;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.juego.Jugador;

import javax.swing.*;
import java.awt.event.ActionListener;

public class AlgoCityJugador
    implements JugadorVista
{

    private Pantallero tableroDePruebas;
    private ControlVista tableroDeControl;
    private Timer timer;
    private PopupFinalizacionObserver popupGanador;
    private PopupFinalizacionObserver popupPerdedor;
    private PopupObserver popupError;

    public AlgoCityJugador(){
    }

    public void jugar()
            throws JuegoNoIniciadoException
    {
        Jugador jugadorActual = MenuAlgoCity.juego().getJugadorActual();

        this.timer = new Timer(VistaParametros.VELOCIDAD_DEL_JUEGO, new AvanzarTiempo(jugadorActual));
        timer.start();

        ActionListener salirDelJuegoAction = new SalirDelJuegoAction(this);
        this.tableroDeControl = new TableroDeControl(jugadorActual, salirDelJuegoAction);
        JFrame panelDeControl = tableroDeControl.crearPantalla();

        this.tableroDePruebas = new TableroDePruebas(jugadorActual,((Mensajeable) tableroDeControl).getMensajero());
        JFrame pantallaDePruebas = tableroDePruebas.crearPantalla();

        tableroDeControl.registrarModoPruebas(new HabilitaPanelDePruebas(pantallaDePruebas));

        pantallaDePruebas.pack();
        panelDeControl.pack();
        panelDeControl.setVisible(true);
        panelDeControl.setExtendedState(panelDeControl.getExtendedState() | JFrame.MAXIMIZED_BOTH);

        popupGanador = new PopupFinalizacionObserver(new VistaPopupTimer(timer), "Ganador !!!");
        jugadorActual.registrarAvisoGanador(popupGanador);

        popupPerdedor = new PopupFinalizacionObserver(new VistaPopupTimer(timer), "Perdedor !!!");
        jugadorActual.registrarAvisoPerdedor(popupPerdedor);

        popupError = new PopupObserver(new VistaPopup());
        jugadorActual.registrarAvisoError(popupError);
    }

    public Timer getTimer()
    {
        return timer;
    }

    public Pantallero getTableroDePruebas()
    {
        return tableroDePruebas;
    }

    public Pantallero getTableroDeControl()
    {
        return tableroDeControl;
    }

    @Override
    public void finalizar() throws JuegoNoIniciadoException
    {
        timer.stop();
        tableroDeControl.finalizar();
        tableroDePruebas.finalizar();
        MenuAlgoCity.juego().getJugadorActual().deregistrarAvisoGanador(popupGanador);
        MenuAlgoCity.juego().getJugadorActual().deregistrarAvisoPerdedor(popupPerdedor);
        MenuAlgoCity.juego().getJugadorActual().deregistrarAvisoError(popupError);
    }


}
