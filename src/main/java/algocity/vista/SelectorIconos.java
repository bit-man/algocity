package algocity.vista;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.ColeccionesAlgoCity;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.mapa.*;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class SelectorIconos
        implements Selector {

    private Collection<Inicial<Construccion, Image>> iconos;

    public SelectorIconos() {
        this.iconos = new ArrayList<Inicial<Construccion, Image>>();
        iconos.add(new Inicial<Construccion, Image>(new Comercial(null), cargarImagen("Shopping")));
        iconos.add(new Inicial<Construccion, Image>(new ConstruccionRota(new Comercial(null)), cargarImagen("ShoppingRoto")));

        iconos.add(new Inicial<Construccion, Image>(new Industrial(null), cargarImagen("Industrial")));
        iconos.add(new Inicial<Construccion, Image>(new ConstruccionRota(new Industrial(null)), cargarImagen("IndustrialRota")));

        iconos.add(new Inicial<Construccion, Image>(new Residencial(null), cargarImagen("Residencial")));
        iconos.add(new Inicial<Construccion, Image>(new ConstruccionRota( new Residencial(null)), cargarImagen("ResidencialRota")));

        SinConstruccion sinConstruccionSobreTierra = new SinConstruccion();
        Construible hectareaTierra = new Hectarea(new Posicion(0,0),SuperficieLlano.getInstance() );
        sinConstruccionSobreTierra.construite(hectareaTierra);
        iconos.add(new Inicial<Construccion, Image>(sinConstruccionSobreTierra, cargarImagen("SinConstruccionSobreTierra")));

        SinConstruccion sinConstruccionSobreAgua = new SinConstruccion();
        Construible hectareaAgua = new Hectarea(new Posicion(0,0),SuperficieAgua.getInstance() );
        sinConstruccionSobreAgua.construite(hectareaAgua);
        iconos.add(new Inicial<Construccion, Image>(sinConstruccionSobreAgua, cargarImagen("SinConstruccionSobreAgua")));

        iconos.add(new Inicial<Construccion, Image>(new CentralElectrica(CentralEolica.getInstance(), null, null), cargarImagen("CentralElectrica")));
        iconos.add(
                new Inicial<Construccion, Image>(
                        new ConstruccionRota( new CentralElectrica(CentralEolica.getInstance(), null, null) ),
                        cargarImagen("CentralElectricaRota")
                )
        );

        iconos.add(new Inicial<Construccion, Image>(new PozoDeAgua(null), cargarImagen("PozoDeAgua")));
        iconos.add(new Inicial<Construccion, Image>(new EstacionDeBomberos(null), cargarImagen("Bomberos")));
    }

    private static Image cargarImagen(String nombreImagen) {
        BufferedImage imagen = null;
        try {
            URL url = new URL("file://" + new File("src/main/recursos/iconos/" + nombreImagen + ".png").getAbsolutePath());
            imagen = ImageIO.read(url);
        } catch (Throwable e) {
            // no cargó la imagen
        }

        return imagen;
    }

    @Override
    public Image seleccionar(Construccion construccion) {
        return ColeccionesAlgoCity.filtrar(this.iconos, new SelectorIconoConstruccion(construccion))
                .iterator().next().getSeleccion();

    }

    private class ConstruccionRota extends Construccion {
        private final Construccion construccion;

        public ConstruccionRota(Construccion construccion) {
            this.construccion = construccion;
            this.integridad = (float) 0.5;
        }

        @Override
        public void construite(Construible hectarea) throws JuegoNoIniciadoException, FueraDelMapaException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException {

        }

        @Override
        public void destruite() {

        }

        public boolean esPozoDeAgua() {
            return construccion.esPozoDeAgua();
        };

        public boolean esCentralElectrica() {
            return construccion.esCentralElectrica();
        };

        public boolean esResidencial() {
            return construccion.esResidencial();
        }

        public boolean esIndustrial() {
            return construccion.esIndustrial();
        }

        public boolean esComercial() {
            return construccion.esComercial();
        }

        public boolean esTerrenoBaldio() {
            return construccion.esTerrenoBaldio();
        }

        public boolean esConstruccion() {
            return construccion.esConstruccion();
        };

        public boolean esEstacionDeBomberos() {
            return construccion.esEstacionDeBomberos();
        };
    }
}
