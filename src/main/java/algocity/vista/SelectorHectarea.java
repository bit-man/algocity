package algocity.vista;

import algocity.modelo.juego.Llamador;
import algocity.modelo.juego.LlamadorMaestro;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Operador;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SelectorHectarea
        extends MouseAdapter {

    private final MapaVistaUbicador ubicadorMapa;
    private final Llamador<Hectarea,ObjetoNulo> llamador;

    public SelectorHectarea(MapaVistaUbicador ubicadorMapa) {
        this.ubicadorMapa = ubicadorMapa;
        llamador = new LlamadorMaestro<Hectarea,ObjetoNulo>();
    }

    public void mouseClicked(MouseEvent e) {
        Point ubicacionEnPantalla = e.getLocationOnScreen();
        final boolean estaEnElMapa = ubicadorMapa.estaEnElMapa(ubicacionEnPantalla.getX(), ubicacionEnPantalla.getY());
        if (estaEnElMapa) {
            Hectarea hectarea = ubicadorMapa.getHectarea(ubicacionEnPantalla);
            llamador.llamarYOlvidar(hectarea);
        }
    }

    public void avisarUnaSolaVez(Operador<Hectarea> operador) {
        llamador.registrar(operador);
    }

}
