package algocity.vista;

import algocity.modelo.juego.JuegoNoIniciadoException;

import javax.swing.*;

public interface JugadorVista
{

    public Timer getTimer();

    public Pantallero getTableroDePruebas();

    public Pantallero getTableroDeControl();

    public void finalizar() throws JuegoNoIniciadoException;
}
