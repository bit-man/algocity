package algocity.vista;

import javax.swing.*;
import java.awt.event.ActionListener;

public interface ComandosVista {
    public JCheckBox getComandoDePruebas();

    public JCheckBox getComandoMostrarConexiones();

    public JButton getComandoGuardar();

    public JButton getComandoSalirDelJuego();

    public void addActionListenerModoPruebas(ActionListener listener);

    public void addActionListenerMostrarConexiones(ActionListener listener);

    public void addActionListenerGuardarJuego(ActionListener listener);

    public void addActionListenerSalirDelJuego(ActionListener listener);
}
