package algocity.vista;

import algocity.modelo.mapa.Hectarea;

public class DibujadorHectarea
        extends DibujadorMapaAbstracto {

    private final Selector selectorIconos;
    private MapaColector mapaColector;

    public DibujadorHectarea(MapaColector mapaColector) {
        this.mapaColector = mapaColector;
        this.selectorIconos = new SelectorIconos();
    }

    @Override
    public Boolean operar(Hectarea hectarea) {

        double ladoHectarea = mapaColector.getLadoHectarea();

        // Posición de la hectárea relativa a la primer hectárea
        double xRelativoHectarea00 = hectarea.getPosicion().getX() * ladoHectarea;
        double yRelativoHectarea00 = hectarea.getPosicion().getY() * ladoHectarea;

        contextoGrafico.drawImage(selectorIconos.seleccionar(hectarea.getConstruccion()),
                                (int) xRelativoHectarea00, (int) yRelativoHectarea00,
                                (int)ladoHectarea, (int) ladoHectarea,
                                null);

        mapaColector.colectarLimite(xRelativoHectarea00, yRelativoHectarea00);

        return Boolean.TRUE;
    }

}
