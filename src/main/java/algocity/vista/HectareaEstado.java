package algocity.vista;

import algocity.modelo.mapa.Hectarea;

public interface HectareaEstado
        extends ActualizableVista<Hectarea>, HectareaEstadoComponentizable {
}
