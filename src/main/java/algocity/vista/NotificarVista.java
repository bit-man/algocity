package algocity.vista;

import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;


public class NotificarVista
        extends OperadorDeregistrable<Object>
        implements Operador<Object> {
    private final JugadorEstado estado;

    public NotificarVista(JugadorEstado view) {
        this.estado = view;
    }

    @Override
    public Boolean operar(Object parametro) {
        estado.actualizar();

        return Boolean.TRUE;
    }
}
