package algocity.vista;

import javax.swing.*;

public class Rotulo<T>
    extends JLabel {

    private String nombre;
    private Formateador formateador;

    public Rotulo(String nombre) {
        this(nombre, 0);
        this.formateador = new FormateadorAnchoFijo();
    }

    public Rotulo(String nombre, Integer ancho) {
        super(nombre);
        this.nombre = nombre;
        this.formateador = new FormateadorAnchoVariable(ancho);
    }

    public void setValor(T valor) {
        setText(formateador.forzarAncho(nombre + valor.toString()));
    }

    public void limpiar() {
        setText(formateador.forzarAncho(nombre));
    }

}
