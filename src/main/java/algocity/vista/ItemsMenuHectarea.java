package algocity.vista;

import algocity.controlador.*;
import algocity.modelo.juego.ContratableConexion;
import algocity.modelo.juego.ContratableConstruccion;
import algocity.modelo.juego.EmpresaConstructora;
import algocity.modelo.juego.Jugable;
import algocity.modelo.mapa.Hectarea;

import javax.swing.*;
import java.awt.*;

public class ItemsMenuHectarea
        extends JPopupMenu {

    public ItemsMenuHectarea(Point locationOnScreen, MapaVista vistaMapa, Jugable jugador, MensajeroEstado mensajero, SelectorHectarea selectorHectarea){

        // FIXME sacar del constructor !!
        Hectarea hectarea = vistaMapa.getHectarea(locationOnScreen);

        for ( ContratableConstruccion contratableConstruccion : EmpresaConstructora.contratablesConstruccion(jugador)) {
            JMenuItem menuItem = new JMenuItem(contratableConstruccion.getConstruccion().toString());
            menuItem.setEnabled(!hectarea.getConstruccion().esConstruccion());
            ConstruyendoConstruccion construyendo = new ConstruyendoConstruccion(contratableConstruccion, hectarea);
            HectareaMenuListener hectareaMenuListenerMenuBotonDerecho =
                    new HectareaMenuListenerMenuBotonDerecho(construyendo);
            construyendo.registrarFinalizacion( new AvisadorActualizacion(vistaMapa) );
            hectareaMenuListenerMenuBotonDerecho.registrarError(new PopupObserver( new VistaPopup()));
            menuItem.addMouseListener(hectareaMenuListenerMenuBotonDerecho);
            add(menuItem);
        }

        add(new Separador());

        for ( ContratableConexion contratableConstruccion : EmpresaConstructora.contratablesConexion(jugador)) {
            JMenuItem menuItem = new JMenuItem(contratableConstruccion.getConexion().toString());
            HectareaMenuListener hectareaMenuListenerMenuBotonIzquierdo = new HectareaMenuListenerMenuBotonIzquierdo(
                    new ConstruyendoConexion(contratableConstruccion, hectarea, mensajero, selectorHectarea)
            );
            hectareaMenuListenerMenuBotonIzquierdo.registrarError(new PopupObserver(new VistaPopup()));
            menuItem.addMouseListener(hectareaMenuListenerMenuBotonIzquierdo);
            add(menuItem);
        }
    }

}
