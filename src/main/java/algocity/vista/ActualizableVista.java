package algocity.vista;

public interface ActualizableVista<T> {

    public void actualizar(T parametro);

    public void limpiar();

    public void actualizar();

}
