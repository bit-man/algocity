package algocity.modelo.zona;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.mapa.Construible;

import java.io.Serializable;

public class SinConstruccion
        extends Construccion
        implements  Serializable {

    private boolean estaSobreLLano;
    private boolean estaSobreAgua;

    public SinConstruccion() {
		reparacionSegunConstruccion = (float) 0.0;
    }

	@Override
	public void construite(Construible hectarea) {
        hectarea.setConstruccion(this);
        ubicacion = hectarea.getPosicion();
        estaSobreLLano = hectarea.getSuperficie().esLlano();
        estaSobreAgua = hectarea.getSuperficie().esAgua();
	}

    public boolean esTerrenoBaldio() {
        return true;
    }

    public boolean esConstruccion() {
        return false;
    };

    public String toString() {
        return "Terreno Baldío";
    }

    public void daniar(Catastrofe catastrofe) {

    }

    public boolean estaSobreLlano() {
        return this.estaSobreLLano;
    }

    public boolean estaSobreAgua() {
        return this.estaSobreAgua;
    }

    @Override
    public void destruite() {

    }
}
