package algocity.modelo.zona;

import algocity.modelo.mapa.Posicion;

public interface Ubicable {
    public Posicion getUbicacion();
}
