package algocity.modelo.zona;

import algocity.modelo.juego.Monetizable;

import java.io.Serializable;

public class Comercial
        extends Zona
        implements Serializable {

	public Comercial(Monetizable monedero) {
		super();
		this.consumoElectrico = 2_000_000;
		superficiesPermitidas.add(SuperficieLlano.getInstance());
		reparacionSegunConstruccion = (float) 1.0;
		reparacionSegunConstruccion = (float) 0.07;
        this.monedero = monedero;
        this.costo = 5;
        danioGodzilla = (float) 0.75;
	}

    public boolean esComercial() {
        return true;
    }

    public String toString() {
        return "Zona Comercial";
    }

    @Override
    public void destruite() {

    }

    public boolean equals(Object otroObjeto)
    {
        if (!(otroObjeto instanceof Comercial))
        {
            return super.equals(otroObjeto);
        }

        if (otroObjeto == this)
        {
            return true;
        }

        Comercial otraComercial = (Comercial) otroObjeto;

        return ( consumoElectrico == otraComercial.consumoElectrico ) &&
                super.equals(otraComercial) ;
    }
}
