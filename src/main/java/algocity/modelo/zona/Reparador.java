package algocity.modelo.zona;

import algocity.modelo.mapa.OperadorNoDeregistrable;

import java.io.Serializable;

public class Reparador
        extends OperadorNoDeregistrable<Object>
        implements Serializable
{

    private final EstacionDeBomberos estacionDeBomberos;

    public Reparador(EstacionDeBomberos estacionDeBomberos) {
        this.estacionDeBomberos = estacionDeBomberos;
    }

    @Override
    public Boolean operar(Object parametro) {
        estacionDeBomberos.reparar();

        return Boolean.TRUE;
    }
}
