package algocity.modelo.zona;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.OperadorNoDeregistrable;

public class AvisadorEnergizacion
        extends OperadorNoDeregistrable<ObjetoNulo>
{
    private final Zona zona;

    public AvisadorEnergizacion(Zona zona)
    {
        this.zona = zona;
    }

    @Override
    public Boolean operar(ObjetoNulo parametro)
    {
        try
        {
            zona.getHectarea().extraerEnergia(zona.getConsumoElectrico());

            // Si no hay energía suficiente no llega a esta sentencia y no deregistra
            zona.getHectarea().deregistrarAvisoEnergizacion(this);
        } catch (EnergiaInsuficienteException e)
        {
            // Si no hay energía suficiente no deregistra
        }

        return Boolean.TRUE;
    }
}
