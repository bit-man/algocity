package algocity.modelo.zona;


import algocity.modelo.juego.Monetizable;

import java.io.Serializable;

public class Industrial
        extends Zona
        implements Serializable
{

	private int capacidadDeEmpleo;
	private int empleados;

	public Industrial(Monetizable monedero) {
		super();
		superficiesPermitidas.add(SuperficieLlano.getInstance());
		this.consumoElectrico = 5_000_000;
		reparacionSegunConstruccion = (float) 0.03;
        this.monedero = monedero;
        costo = 10;
		capacidadDeEmpleo = 25;
		empleados = 0;
		danioGodzilla = (float) 0.4;
	}

    public boolean esIndustrial() {
        return true;
    }

	public int getCapacidadDeEmpleo() {
		return this.capacidadDeEmpleo;
	}

	public int getEmpleados() {
		return this.empleados;
	}

	public void setEmpleados(int empleados) {
		this.empleados = empleados;
	}

    public String toString() {
        return "Zona Industrial";
    }

    @Override
    public void destruite() {

    }

    public boolean equals(Object otroObjeto)
    {
        if (!(otroObjeto instanceof Ruptura))
        {
            return super.equals(otroObjeto);
        }

        if (otroObjeto == this)
        {
            return true;
        }

        Industrial otroIndustrial = (Industrial) otroObjeto;

        return
                ( capacidadDeEmpleo == otroIndustrial.capacidadDeEmpleo ) &&
                ( empleados == otroIndustrial.empleados ) &&
                super.equals(otroIndustrial );
    }
}
