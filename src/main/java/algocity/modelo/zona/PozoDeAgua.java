package algocity.modelo.zona;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.Monetizable;
import algocity.modelo.mapa.Construible;
import algocity.modelo.mapa.Loteable;
import algocity.modelo.necesidades.SuperficieInvalidaException;

public class PozoDeAgua
        extends Construccion {

    private final Monetizable monedero;

    public PozoDeAgua(Monetizable monedero){
		costo = 250;
        reparacionSegunConstruccion = (float) 0.0;
        this.monedero = monedero;
	}
    @Override
    public void construite(Construible hectarea) throws SuperficieInvalidaException, DineroInsuficienteException {
        verificarTerreno(hectarea);
        monedero.quitarDinero(getCosto());
        hectarea.setConstruccion(this);
        ubicacion = hectarea.getPosicion();
    }

    private void verificarTerreno(Loteable hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esAgua()) {
            throw new SuperficieInvalidaException(hectarea.getSuperficie());
        }
    }

    public void daniar(Catastrofe catastrofe) {

    }

    @Override
    public boolean esPozoDeAgua() {
        return true;
    }


    public String toString() {
        return "Pozo de Agua";
    }

    @Override
    public void destruite() {

    }
}
