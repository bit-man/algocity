package algocity.modelo.zona;


import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.Jugable;
import algocity.modelo.mapa.Construible;
import algocity.modelo.mapa.Loteable;
import algocity.modelo.necesidades.SuperficieInvalidaException;

public class EstacionDeBomberos
        extends Construccion {

    private Jugable jugador;

    public EstacionDeBomberos(Jugable jugador){
        super();
        costo = 1500;
        this.jugador = jugador;
        reparacionSegunConstruccion = (float) 0.0;
    }
    @Override
    public void construite(Construible hectarea) throws SuperficieInvalidaException, DineroInsuficienteException {
        verificarSuperficie(hectarea);
        try {
            jugador.quitarDinero(getCosto());
        } catch (DineroInsuficienteException e) {
            throw new DineroInsuficienteException();
        }

        hectarea.setConstruccion(this);
        jugador.registrarAvanceTiempo( new Reparador(this) );
        ubicacion = hectarea.getPosicion();
    }

    private void verificarSuperficie(Loteable hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esLlano()) {
            throw new SuperficieInvalidaException(hectarea.getSuperficie());
        }
    }

    public void reparar() {
        // ToDo cada vez que se daña una construcción ponerla en una lista y luego recorrerla
        jugador.getMapa().scan(new BrigadaDeBomberos( new RemovedorEscombros() ));
    }

    public boolean esEstacionDeBomberos() {
        return true;
    };

    public String toString() {
        return "Estación de Bomberos";
    }

    @Override
    public void destruite() {

    }

    public void daniar(Catastrofe catastrofe) {

    }
}
