package algocity.modelo.zona;

import java.io.Serializable;

public class SuperficieLlano
        extends Superficie
        implements  Serializable {

	private static SuperficieLlano superficieLlano=new SuperficieLlano();
	
	private SuperficieLlano(){
	}
	
	public static Superficie getInstance() {
		return superficieLlano;
	}

    // Preservación del singleton después de la deserialización
    private Object readResolve()
    {
        return superficieLlano;
    }

    @Override
    public boolean esLlano() {
        return true;
    }

    public String toString() {
        return "Terreno LLano";
    }
}