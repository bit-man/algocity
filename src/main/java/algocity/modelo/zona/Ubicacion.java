package algocity.modelo.zona;

import algocity.modelo.mapa.Posicion;

import java.io.Serializable;

public abstract class Ubicacion
        implements Ubicable, Serializable
{

    protected Posicion ubicacion;

    /**
     * Permite conocer la ubicación del elemento. En caso de ser
     * una Conexión la ubicación se haya determinado por la hectárea de origen
     * @return posición donde se encuentra ubicado
     */
    public Posicion getUbicacion() {
        return ubicacion;
    }


    public boolean equals(Object otroObjeto)
    {
        if (!(otroObjeto instanceof Ubicacion))
        {
            return super.equals(otroObjeto);
        }

        if (otroObjeto == this)
        {
            return true;
        }

        Ubicacion otraUbicacion = (Ubicacion) otroObjeto;

        return (ubicacion == null && otraUbicacion.ubicacion == null) ||
               (ubicacion != null && ubicacion.equals(otraUbicacion.ubicacion)) ;
    }

}
