package algocity.modelo.zona;

import algocity.modelo.catastrofe.Catastrofe;

public interface Rompible {

    /**
     * Repara un daño en el porcentaje indicado
     * @param porcentajeDeReparacion
     */
    public void reparar(float porcentajeDeReparacion);

    /**
     * Hace daño a la construción
     */
    public void daniar(Catastrofe catastrofe);

    public float getIntegridad();

    public void destruite();

    /**
     * Daño a provocar por Godzilla
     * @return
     */
    public float getDanioGodzilla();
}
