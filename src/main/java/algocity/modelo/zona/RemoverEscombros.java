package algocity.modelo.zona;

import algocity.modelo.juego.Filtrable;
import algocity.modelo.necesidades.Conexion;

public class RemoverEscombros
        implements Filtrable<Conexion> {

    @Override
    public boolean aceptado(Conexion conexion) {
        return (conexion.getIntegridad() == 0) && ! conexion.esReparable() ;
    }
}
