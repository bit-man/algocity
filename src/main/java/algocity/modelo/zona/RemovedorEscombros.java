package algocity.modelo.zona;

import algocity.modelo.juego.ColeccionesAlgoCity;
import algocity.modelo.juego.Trabajable;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.necesidades.Conexion;

public class RemovedorEscombros
    implements Trabajable<Hectarea>{

    @Override
    public Boolean trabajar(Hectarea hectarea) {
        for ( Conexion conexion :
                ColeccionesAlgoCity.filtrar(hectarea.getConexionesDisponibles(), new RemoverEscombros() ) ) {
            conexion.destruite();
        }

        return Boolean.TRUE;
    }
}
