package algocity.modelo.zona;

import java.io.Serializable;

public class SuperficieAgua
        extends Superficie
        implements Serializable {

	private static SuperficieAgua singleton = new SuperficieAgua();
	
	private SuperficieAgua(){}
	
	public static Superficie getInstance() {
		return singleton;
	}

    @Override
    public boolean esAgua() {
        return true;
    }

    // Preservación del singleton después de la deserialización
    private Object readResolve()
    {
        return singleton;
    }

    public String toString() {
        return "Agua";
    }
}
