package algocity.modelo.zona;

import algocity.modelo.catastrofe.Catastrofe;

import java.io.Serializable;

public abstract class Ruptura
        extends Ubicacion
        implements Rompible, Serializable
{

    protected float integridad;
    protected float reparacionSegunConstruccion;
    protected float danioGodzilla;

    public Ruptura() {
        integridad = (float) 1.0;
    }

    /**
     * Repara un daño en el porcentaje indicado
     * @param porcentajeDeReparacion
     */
    public void reparar(float porcentajeDeReparacion) {
        integridad += porcentajeDeReparacion;
        if ( integridad > (float) 1.0) {
            integridad = (float) 1.0;
        }
    }

    /**
     * Hace daño según la construción correspondiente
     */
    public void daniar(Catastrofe catastrofe) {
        integridad -= catastrofe.getDanioSegunConstruccion(this);
        if ( integridad < (float) 0.0) {
            integridad = (float) 0.0;
        }
    }


    public float getReparacionSegunConstruccion() {
        return reparacionSegunConstruccion;
    };

    public float getIntegridad() {
        return integridad;
    }

    public boolean esReparable() {
        return true;
    };

    public float getDanioGodzilla() {
        return danioGodzilla;
    }

    public boolean equals(Object otroObjeto)
    {
        if (!(otroObjeto instanceof Ruptura))
        {
            return super.equals(otroObjeto);
        }

        if (otroObjeto == this)
        {
            return true;
        }

        Ruptura otraRuptura = (Ruptura) otroObjeto;

        return
                (  integridad == otraRuptura.integridad ) &&
                (  reparacionSegunConstruccion == otraRuptura.reparacionSegunConstruccion ) &&
                (  danioGodzilla == otraRuptura.danioGodzilla )
                && super.equals(otraRuptura);
    }

}
