package algocity.modelo.zona;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.mapa.Construible;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.necesidades.SuperficieInvalidaException;

import java.io.Serializable;


public abstract class Construccion
        extends Ruptura
        implements Serializable
{

	protected int costo = 0;

	public Construccion(){
		super();
	}

	public abstract void construite (Construible hectarea) throws JuegoNoIniciadoException, FueraDelMapaException, EnergiaInsuficienteException, SuperficieInvalidaException, DineroInsuficienteException;

	public int getCosto() {
		return costo;
	}

    public boolean esPozoDeAgua() {
		return false;
	};

	public boolean esCentralElectrica() {
		return false;
	};

    public boolean esResidencial() {
        return false;
    }

    public boolean esIndustrial() {
        return false;
    }

    public boolean esComercial() {
        return false;
    }

	public boolean esTerrenoBaldio() {
		return false;
	}

    public boolean esConstruccion() {
        return true;
    };

    public boolean esEstacionDeBomberos() {
        return false;
    };

    public boolean equals(Object otroObjeto) {
        if ( ! (otroObjeto instanceof Construccion ) ) {
            return super.equals(otroObjeto);
        }

        if ( otroObjeto == this ) {
            return true;
        }

        Construccion otraConstruccion = (Construccion) otroObjeto;

        return
                ( esPozoDeAgua() == otraConstruccion.esPozoDeAgua() ) &&
                (  esCentralElectrica() == otraConstruccion.esCentralElectrica() ) &&
                (  esResidencial() == otraConstruccion.esResidencial()) &&
                (  esIndustrial() == otraConstruccion.esIndustrial() ) &&
                (  esComercial() == otraConstruccion.esComercial() ) &&
                (  esTerrenoBaldio() == otraConstruccion.esTerrenoBaldio() ) &&
                (  esConstruccion() == otraConstruccion.esConstruccion() ) &&
                (  esEstacionDeBomberos() == otraConstruccion.esEstacionDeBomberos() ) &&
                ( costo == otraConstruccion.costo)
                && super.equals(otraConstruccion);

    }

}
