package algocity.modelo.zona;

import algocity.modelo.juego.Trabajable;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.OperadorNoDeregistrable;


public class BrigadaDeBomberos
        extends OperadorNoDeregistrable<Hectarea>
{

    private final Trabajable<Hectarea> removedorEscombros;

    public BrigadaDeBomberos(Trabajable<Hectarea> removedorEscombros) {
        this.removedorEscombros = removedorEscombros;
    }

    @Override
    public Boolean operar(Hectarea hectarea) {
        final Construccion construccion = hectarea.getConstruccion();
        final float porcentajeDeReparacion = construccion.getReparacionSegunConstruccion();
        construccion.reparar(porcentajeDeReparacion);

        removedorEscombros.trabajar(hectarea);
        return Boolean.TRUE;
    }
}
