package algocity.modelo.zona;

import algocity.modelo.juego.Monetizable;

public class Residencial
        extends Zona {

    private static final int GENTE_ALOJADA_INICIAL_EN_RESIDENCIAL = 10;
    private int capacidadAlojamiento;
    private int cantidadDeGenteAlojada;

    public Residencial(Monetizable monedero){
		super();
		superficiesPermitidas.add(SuperficieLlano.getInstance());
		this.capacidadAlojamiento = 100;
		this.consumoElectrico = 1_000_000;

        // Arrancamos con changüi
		this.cantidadDeGenteAlojada = GENTE_ALOJADA_INICIAL_EN_RESIDENCIAL;
		reparacionSegunConstruccion = (float) 0.1;
        this.monedero = monedero;
        costo = 5;
        danioGodzilla = (float) 1.0;
 	}

	
	public int getCapacidadDeAlojamiento(){
		return this.capacidadAlojamiento;
		
	}

	public int getCantidadDeGenteAlojada() {
		return cantidadDeGenteAlojada;
	}

    public boolean esResidencial() {
        return true;
    }

    public void setCantidadDeGenteAlojada(int alojados) {
        this.cantidadDeGenteAlojada = alojados;
    }

    public String toString() {
        return "Zona Residencial";
    }

    @Override
    public void destruite() {

    }
}
