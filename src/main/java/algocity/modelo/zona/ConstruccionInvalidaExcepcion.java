package algocity.modelo.zona;

public class ConstruccionInvalidaExcepcion
        extends Throwable {

    public ConstruccionInvalidaExcepcion() {
        super("Construcción inválida");
    }
}
