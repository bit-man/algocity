package algocity.modelo.almacenamiento;

public class ErrorAlGuardarJugadorException
        extends Throwable
{
    public ErrorAlGuardarJugadorException(String nombre, Throwable e)
    {
        super("Error al guardar el usuario " + nombre, e);
    }
}
