package algocity.modelo.almacenamiento;

public class ErrorAlObtenerJugadorException
        extends Throwable {

    public ErrorAlObtenerJugadorException(String nombre, Throwable e) {
        super("Error al obtener el jugador " + nombre, e);
    }
}
