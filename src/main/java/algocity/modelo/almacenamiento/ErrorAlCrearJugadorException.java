package algocity.modelo.almacenamiento;

public class ErrorAlCrearJugadorException
        extends Throwable {

    public ErrorAlCrearJugadorException(String nombre, Throwable e) {
        super("Error al inicializar jugador " + nombre, e);
    }
}
