package algocity.modelo.almacenamiento;

import algocity.modelo.juego.DirectorioDeConfiguracionNoInicializadoException;
import algocity.modelo.juego.Jugador;
import algocity.modelo.juego.JugadorExistenteException;
import algocity.modelo.juego.JugadorInexistenteException;

import java.io.FileNotFoundException;
import java.util.Map;

public interface Persistible {
    public void inicializar() throws DirectorioDeConfiguracionNoInicializadoException;

    public Map<String, Integer> getPuntajes() throws ErrorAlObtenerPuntajes;

    public void agregarJugador(Jugador jugador) throws ErrorAlCrearJugadorException, JugadorExistenteException;

    public Jugador getJugador(String nombre) throws ErrorAlObtenerJugadorException;

    public void borrarJugador(String nombre) throws JugadorInexistenteException;

    public void guardarPuntajes(Map<String, Integer> puntajes) throws FileNotFoundException, GuardarPuntajesException;

    public void guardarJugador(Jugador jugadorActual)
            throws ErrorAlGuardarJugadorException;
}
