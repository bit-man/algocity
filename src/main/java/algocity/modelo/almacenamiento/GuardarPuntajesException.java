package algocity.modelo.almacenamiento;

public class GuardarPuntajesException extends Throwable {
    public GuardarPuntajesException(Throwable e) {
        super("Error al tratar de guardar los puntajes", e);
    }
}
