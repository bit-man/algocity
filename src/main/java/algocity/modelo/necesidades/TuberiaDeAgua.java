package algocity.modelo.necesidades;

import algocity.modelo.catastrofe.Catastrofe;
import algocity.modelo.juego.Monetizable;
import algocity.modelo.mapa.Hectarea;

public class TuberiaDeAgua
    extends Conexion {

    public TuberiaDeAgua(Monetizable jugador){
        super(jugador);
        costoDeConstruccionPorHectarea = 5;
	}

    @Override
    public boolean esTuberiaDeAgua() {
        return true;
    }

    @Override
    public void validarOrigen(Hectarea hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esAgua() ) {
            throw new SuperficieInvalidaException( hectarea.getSuperficie());
        }
    }

    @Override
    public void validarDestino(Hectarea hectarea) throws SuperficieInvalidaException {
        if (!hectarea.getSuperficie().esLlano()) {
            throw new SuperficieInvalidaException(hectarea.getSuperficie());
        }
    }

    @Override
    public void agregadosDeHectarea(Hectarea origen, Hectarea destino) {

    }

    public void daniar(Catastrofe catastrofe) {}

    public void reparar(float porcentajeDeReparacion) {}

    @Override
    public void destruite() {}

    public boolean esReparable() {
        return false;
    };

    public String toString() {
        return "Tubería de Agua";
    }
}
