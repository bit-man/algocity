package algocity.modelo.necesidades;

import algocity.modelo.juego.Monetizable;
import algocity.modelo.mapa.Hectarea;

import java.io.Serializable;

public class RutaPavimentada
        extends Conexion
        implements Serializable {

	public RutaPavimentada(Monetizable jugador){
        super(jugador);
        costoDeConstruccionPorHectarea = 10;
        danioGodzilla = (float) 0.8;
	}

    @Override
    public boolean esRutaPavimentada() {
        return true;
    }


    @Override
    public void validarOrigen(Hectarea hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esLlano() ) {
            throw new SuperficieInvalidaException( hectarea.getSuperficie());
        }
    }

    @Override
    public void validarDestino(Hectarea hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esLlano() ) {
            throw new SuperficieInvalidaException( hectarea.getSuperficie());
        }
    }

    @Override
    public void agregadosDeHectarea(Hectarea origen, Hectarea destino) {

    }


    public String toString() {
        return "Ruta Pavimentada";
    }
}
