package algocity.modelo.necesidades;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.EnergizadorLineaDeTension;
import algocity.modelo.juego.Monetizable;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;

import java.io.Serializable;

public class LineaDeTension
        extends Conexion
        implements Serializable{

    public LineaDeTension(Monetizable jugador){
        super(jugador);
        costoDeConstruccionPorHectarea = 5;
        danioGodzilla = (float) 1.0;
    }

    @Override
    public boolean esLineaDeTension() {
        return true;
    }

    @Override
    protected void validarOrigen(Hectarea hectarea) throws SuperficieInvalidaException, ConstruccionInvalidaExcepcion {

        // ToDo no es redundante ??
        if ( ! hectarea.getSuperficie().esLlano() ) {
            throw new SuperficieInvalidaException( hectarea.getSuperficie());
        }

        if ( ! hectarea.getConstruccion().esCentralElectrica()) {
            throw new ConstruccionInvalidaExcepcion();
        }
    }

    @Override
    protected void validarDestino(Hectarea hectarea) throws SuperficieInvalidaException {
        if ( ! hectarea.getSuperficie().esLlano() ) {
            throw new SuperficieInvalidaException( hectarea.getSuperficie());
        }
    }

    @Override
    protected void agregadosDeHectarea(Hectarea origen, Hectarea destino) {
        destino.ahoraTieneEnergia(new EnergizadorLineaDeTension((CentralElectrica) origen.getConstruccion()));
    }

    public boolean esReparable() {
        return false;
    };

    public String toString() {
        return "Línea de Tensión";
    }

}
