package algocity.modelo.necesidades;

import algocity.modelo.zona.Superficie;

public class SuperficieInvalidaException
        extends Throwable {

    public Superficie getSuperficie() {
        return superficie;
    }

    private final Superficie superficie;

    public SuperficieInvalidaException(Superficie superficie) {
        super("La superficie '" + superficie + "' es inválida");
        this.superficie = superficie;
    }
}
