package algocity.modelo.necesidades;

import algocity.modelo.juego.Filtrable;

public class ComparadorNecesidad
    implements Filtrable<Conexion> {

    private final Conexion facilidad;

    public ComparadorNecesidad(Conexion facilidad) {
        this.facilidad = facilidad;
    }

    public static boolean comparar(Conexion conexion1, Conexion conexion2) {
        return ( conexion2.esLineaDeTension() == conexion1.esLineaDeTension() ) &&
                (conexion2.esRutaPavimentada() == conexion1.esRutaPavimentada() ) &&
                (conexion2.esTuberiaDeAgua() == conexion1.esTuberiaDeAgua());
    }

    @Override
    public boolean aceptado(Conexion parametro) {
        return  parametro.esLineaDeTension() == facilidad.esLineaDeTension() &&
                parametro.esRutaPavimentada() == facilidad.esRutaPavimentada() &&
                parametro.esTuberiaDeAgua() == facilidad.esTuberiaDeAgua();
    }
}
