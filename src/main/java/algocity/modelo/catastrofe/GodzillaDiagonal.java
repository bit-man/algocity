package algocity.modelo.catastrofe;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.Calesita;
import algocity.modelo.juego.Cambiador;
import algocity.modelo.juego.Parametros;
import algocity.modelo.mapa.Dimension;
import algocity.modelo.mapa.Iterador2DDireccion;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;

public class GodzillaDiagonal
        extends Godzilla {

    public GodzillaDiagonal(Mapa mapa, FuncionAzar azar) {
        super(mapa,azar);
    }

    @Override
    protected Cambiador<Iterador2DDireccion> calcularCaminoDeDirecciones() {
        int tramos = azar.nuevoValor( 1, Parametros.GODZILLA_CAMBIOS_DE_DIRECCION_MAXIMO + 1);
        final Dimension dimensionesMapa = mapa.getDimensiones();
        final Posicion origenEnMapaPosicion = origenEnMapa.getPosicion();

        final int deltaX;
        final int deltaY;

        if ( origenEnMapaPosicion.getX() == 0 || (origenEnMapaPosicion.getX() == dimensionesMapa.getX() - 1 ) ) {
            deltaX = (int) (dimensionesMapa.getX() * Math.signum( (dimensionesMapa.getX() / 2) - origenEnMapaPosicion.getX() ));
            // evitar que llegue exactamente al vértice inferior izquierdo
            deltaY = dimensionesMapa.getY() - origenEnMapaPosicion.getY() - 2;
        } else {
            deltaY = (int) (dimensionesMapa.getY() * Math.signum( (dimensionesMapa.getY() / 2) - origenEnMapaPosicion.getY() ));
            // evitar que llegue exactamente al vértice inferior izquierdo
            deltaX = dimensionesMapa.getX() - origenEnMapaPosicion.getX() - 2;
        }

        int deltaSaltoX = deltaX / tramos;
        int deltaSaltoY = deltaY / tramos;

        Cambiador<Iterador2DDireccion> cambiador = new Calesita<Iterador2DDireccion>();
        agregarTramo(deltaSaltoX, cambiador, Iterador2DDireccion.DERECHA);
        agregarTramo(deltaSaltoY, cambiador, Iterador2DDireccion.ABAJO);

        return cambiador;
    }

    private void agregarTramo(int delta, Cambiador<Iterador2DDireccion> cambiador, Iterador2DDireccion direccionPropuesta) {
        Iterador2DDireccion direccionReal = direccionPropuesta;
        if ( delta < 0 ) {
             direccionReal = calcularOpuesta(direccionPropuesta);
        }

        for ( int y = 0; y < Math.abs(delta) ; y++ ) {
            cambiador.agregar(direccionReal);
        }
    }
}
