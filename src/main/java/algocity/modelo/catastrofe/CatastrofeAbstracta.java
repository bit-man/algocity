package algocity.modelo.catastrofe;

import algocity.modelo.juego.Llamador;
import algocity.modelo.juego.LlamadorMaestro;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.Posicion;

import java.util.Collection;
import java.util.HashSet;

public abstract class CatastrofeAbstracta
    implements Catastrofe {

    protected final Llamador<Catastrofe,ObjetoNulo> llamador;
    protected Posicion posicionDaniada;

    public CatastrofeAbstracta() {
        this.llamador = new LlamadorMaestro<Catastrofe,ObjetoNulo>();
    }


    /***
     * Registración de un observador para que avise cada hectarea por la que pasa
     * @param operador
     */
    public void registrar(Operador<Catastrofe> operador) {
        llamador.registrar(operador);
    }


    @Override
    public void deregistrar(Operador<Catastrofe> observador)
    {
        llamador.deregistrar(observador);
    }

    @Override
    public void deregistrarError(Operador<ObjetoNulo> observador)
    {
    }

    public java.util.Collection<Operador<Catastrofe>> deregistrar()
    {
        return llamador.deregistrar();
    }

    @Override
    public Posicion getPosicionDaniada() {
        return posicionDaniada;
    }

    @Override
    public void setPosicionDaniada(Posicion posicionDaniada) {
        this.posicionDaniada = posicionDaniada;
    }


    public Collection<Operador<ObjetoNulo>> deregistrarError()
    {
        return new HashSet<>();
    }
}
