package algocity.modelo.catastrofe;

import algocity.modelo.azar.AzarSimuladoElementoFueraDeRangoException;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.juego.Registrable;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.zona.Ruptura;

public interface Catastrofe
        extends Registrable<Catastrofe,ObjetoNulo> {

    float getDanioSegunConstruccion(Ruptura rompible);

    public boolean esTerremoto();

    public boolean esGodzilla();

    public void hacerDanio() throws FueraDelMapaException, AzarSimuladoElementoFueraDeRangoException;

    public void setPosicionDaniada(Posicion posicion);

    public Posicion getPosicionDaniada();
}
