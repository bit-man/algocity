package algocity.modelo.catastrofe;

import algocity.modelo.juego.Llamador;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;
import algocity.modelo.mapa.OperadorNoDeregistrable;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.zona.Construccion;

public class RompedorDeHectarea
        extends OperadorDeregistrable<Hectarea>
        implements Operador<Hectarea> {

    private final Catastrofe catastrofe;
    private final Llamador<Catastrofe,ObjetoNulo> llamador;

    public RompedorDeHectarea(Catastrofe catastrofe, Llamador<Catastrofe,ObjetoNulo> llamador) {
        this.catastrofe = catastrofe;
        this.llamador = llamador;
    }

    @Override
    public Boolean operar(Hectarea hectarea) {

        // Dañar la construcción
        final Construccion construccion = hectarea.getConstruccion();
        construccion.daniar(catastrofe);

        // Dañar las conexiones
        for ( Conexion conexion : hectarea.getConexionesDisponibles() ) {
            conexion.daniar(catastrofe);
        }

        catastrofe.setPosicionDaniada(hectarea.getPosicion());
        llamador.llamar(catastrofe);
        return Boolean.TRUE;
    }

}