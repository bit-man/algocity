package algocity.modelo.catastrofe;

import algocity.modelo.juego.CalculadorRadios;
import algocity.modelo.juego.Parametros;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.Posicion;
import algocity.modelo.zona.Ruptura;

public class Terremoto
        extends CatastrofeAbstracta {

    private Mapa mapa;
	private Hectarea epicentro;

    public Terremoto(Mapa unMapa, Hectarea unEpicentro) {
        super();
        this.mapa = unMapa;
        this.epicentro = unEpicentro;
    }

    public void hacerDanio() {
        Operador<Hectarea> op = new RompedorDeHectareaSinAviso(this);

        op.operar(epicentro);
        int maxRadio = new CalculadorRadios(epicentro, mapa).calcular();
        for ( int i = 1 ; i <= maxRadio; i++) {
            mapa.operarEnElRadio(i, epicentro, op);
        }

        posicionDaniada = epicentro.getPosicion();
        // Llama una sola vez al final para avisar sólo edl epicentro
        llamador.llamar(this);
    }


    @Override
    public float getDanioSegunConstruccion(Ruptura rompible) {
        // Evita que el daño calculado sea negativo
        return Math.max( (float) 1.0 - Parametros.DANIO_TERREMOTO * distanciaAlEpicentro(rompible), (float) 0.0);
    }

    @Override
    public boolean esTerremoto() {
        return true;
    }

    @Override
    public boolean esGodzilla() {
        return false;
    }

    private int distanciaAlEpicentro(Ruptura rompible) {
        Posicion ubicacion = rompible.getUbicacion();
        Posicion posicionEpicentro = epicentro.getPosicion();
        int deltaX = Math.abs( posicionEpicentro.getX() - ubicacion.getX() );
        int deltaY = Math.abs( posicionEpicentro.getY() - ubicacion.getY() );
        return Math.max(deltaX, deltaY);
    }

    public String toString() {
        return "Terremoto";
    }
}
