package algocity.modelo.catastrofe;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.Calesita;
import algocity.modelo.juego.Cambiador;
import algocity.modelo.juego.Parametros;
import algocity.modelo.mapa.Iterador2DDireccion;
import algocity.modelo.mapa.Mapa;

public class GodzillaZigZag
        extends GodzillaDiagonal {

    public GodzillaZigZag(Mapa mapa, FuncionAzar azar) {
        super(mapa,azar);
    }

    @Override
    protected Cambiador<Iterador2DDireccion> calcularCaminoDeDirecciones() {
        final Cambiador<Iterador2DDireccion> cambiador = super.calcularCaminoDeDirecciones();

        Cambiador<Iterador2DDireccion> nuevoCambiador = new Calesita<>();

        while(  ! cambiador.dioLaVuelta() ) {
            boolean introducirCambio = azar.nuevoValor( (float) 0.0, (float) 1) > Parametros.GODZILLA_PROBABILIDAD_CAMBIO_DE_DIRECCION;
            Iterador2DDireccion nuevaDireccion = cambiador.item();

            if ( introducirCambio ) {
                nuevaDireccion = calcularOpuesta(nuevaDireccion);
            }

            nuevoCambiador.agregar(nuevaDireccion);
            cambiador.cambiar();
        }

        return nuevoCambiador;
    }
}
