package algocity.modelo.catastrofe;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.juego.Calesita;
import algocity.modelo.juego.Cambiador;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Iterador2D;
import algocity.modelo.mapa.Iterador2DDireccion;
import algocity.modelo.mapa.Mapa;


public class GodzillaLineal
        extends Godzilla {

    public GodzillaLineal(Mapa mapa, FuncionAzar azar) {
        super(mapa, azar);
    }

    /**
     * Calcula la dirección a moverse según la hectarea de origen
     * @return
     */
    protected Cambiador<Iterador2DDireccion> calcularCaminoDeDirecciones() {
        final Calesita<Iterador2DDireccion> direccionCalesita = new Calesita<>();

        for (Iterador2DDireccion direccion : Iterador2DDireccion.values()) {

            final Iterador2D<Hectarea> iterator = mapa.iterator(origenEnMapa.getPosicion());
            if (!iterator.hayMasHacia(direccion)) {
                direccionCalesita.agregar(calcularOpuesta(direccion));
                return direccionCalesita;
            }
        }

        return null;
    }

}
