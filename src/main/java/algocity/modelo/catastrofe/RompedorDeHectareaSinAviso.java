package algocity.modelo.catastrofe;

import algocity.modelo.juego.LlamadorMaestro;
import algocity.modelo.juego.ObjetoNulo;

public class RompedorDeHectareaSinAviso
    extends RompedorDeHectarea {

    public RompedorDeHectareaSinAviso(Terremoto terremoto) {
        super(terremoto, new LlamadorMaestro<Catastrofe,ObjetoNulo>());
    }
}
