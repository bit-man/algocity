package algocity.modelo.azar;

import algocity.modelo.juego.Calesita;
import algocity.modelo.juego.Cambiador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class AzarSimulado
        extends FuncionAzar
        implements Serializable {

    private Cambiador<Float> cambiador;

    public AzarSimulado(float f) throws AzarSimuladoElementoFueraDeRangoException {
        this.cambiador = new Calesita<Float>();
        cambiador.agregar(f);
        init();
    }

    public AzarSimulado(Collection<Float> lista) throws AzarSimuladoListaNullException, AzarSimuladoListaVaciaException, AzarSimuladoElementoFueraDeRangoException {
        if ( lista == null ) {
            throw new AzarSimuladoListaNullException();
        }

        if ( lista.isEmpty() ) {
            throw new AzarSimuladoListaVaciaException();
        }

        this.cambiador = new Calesita<Float>();
        cambiador.agregar(lista);
        init();
    }

    private void init() throws AzarSimuladoElementoFueraDeRangoException {
        while( ! cambiador.dioLaVuelta()) {
            Float item = cambiador.item();
            if ( item < 0 || item >= 1) {
                throw new AzarSimuladoElementoFueraDeRangoException(item);
            }
            cambiador.cambiar();
        }

        cambiador.reiniciar();
    }

    @Override
    protected float nuevoValorPuro() {
        Float item = cambiador.item();
        cambiador.cambiar();
        return item;
    }
}
