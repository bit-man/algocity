package algocity.modelo.azar;

public class AzarSimuladoListaNullException
        extends Throwable {

    public AzarSimuladoListaNullException() {
        super("Lista nula");
    }
}
