package algocity.modelo.azar;

public class AzarSimuladoElementoFueraDeRangoException
        extends Throwable {

    private final float elemento;

    public AzarSimuladoElementoFueraDeRangoException(float elemento) {
        super("El elemento " + elemento + " es menor que 0 o mayor o igual a 1");
        this.elemento = elemento;
    }

    public float getElemento() {
        return elemento;
    }
}
