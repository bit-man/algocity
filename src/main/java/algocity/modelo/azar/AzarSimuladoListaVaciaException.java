package algocity.modelo.azar;

public class AzarSimuladoListaVaciaException
        extends Throwable {
    public AzarSimuladoListaVaciaException() {
        super("Lista vacía");
    }
}
