package algocity.modelo.azar;

public abstract class FuncionAzar {

    /**
     * Valor al azar delimitados por los parámetros max y min
     * @param min valor mínimo
     * @param max valor máximo
     * @return un número al azar cuyo valor esté entre el valor mínimo (inclusive)
     *          y el máximo (no inclusive)
     */
    public float nuevoValor(float min, float max) {
        return (max - min) * nuevoValorPuro() + min;
    }
    /**
     * Valor al azar delimitados por los parámetros max y min
     * @param min valor mínimo
     * @param max valor máximo
     * @return un número entero al azar cuyo valor esté entre el valor mínimo (inclusive)
     *          y el máximo (no inclusive)
     */
    public int nuevoValor(int min, int max) {
        return (int) Math.floor( (max - min) *  nuevoValorPuro() + min );
    };

    /**
     * Boolean al azar con una probabilidad de ocurrencia del 50%
     * @return true o false
     */
    public boolean nuevoBoolean() {
        return nuevoValorPuro() < 0.5;
    }

    /**
     * Valor entre 0.0 (inclusive) y 1.0 (exclusive) elegido según la estrategia de azar
     * @return
     */
    protected abstract float nuevoValorPuro();

    public int nuevoValorExtremo(int unExtremo, int elOtroExtremo) {
        return nuevoBoolean() ? unExtremo : elOtroExtremo;
    };
}
