package algocity.modelo.azar;


import java.io.Serializable;
import java.util.Random;

public class AzarReal
        extends FuncionAzar
        implements Serializable {

    public AzarReal() {}

    /**
     * Este seed es bastante simple y hace que el random sea predecible
     * pero para los efectos del trabajo consideramos que está bien
     */
    Random random = new Random(System.currentTimeMillis());

    @Override
    protected float nuevoValorPuro() {
        return random.nextFloat();
    }

}
