package algocity.modelo.energia;

import java.io.Serializable;

public class EnergizadorLineaDeTension
    implements Energizador, Serializable {
    private final CentralElectrica central;

    public EnergizadorLineaDeTension(CentralElectrica central) {
        this.central = central;
    }

    @Override
    public void dameEnergía(int energia) throws EnergiaInsuficienteException {
        central.dameEnergia(energia);
    }

    @Override
    public boolean estaEnergizada() {
        return true;
    }

    @Override
    public int consultarEnergiaRestante() {
        return central.getCapacidadRestante();
    }

    @Override
    public void compensarEnergia(int energia) {
        central.compensarEnergia(energia);
    }
}
