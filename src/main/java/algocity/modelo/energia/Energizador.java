package algocity.modelo.energia;

public interface Energizador {

    public void dameEnergía( int energia ) throws EnergiaInsuficienteException;

    public boolean estaEnergizada();

    public int consultarEnergiaRestante();

    public void compensarEnergia(int energia);
}
