package algocity.modelo.energia;

import java.io.Serializable;

public class EnergizadorCentralElectrica
    implements Energizador, Serializable
{

    private final CentralElectrica central;

    public EnergizadorCentralElectrica(CentralElectrica central) {
        this.central = central;
    }

    @Override
    public void dameEnergía(int energia) throws EnergiaInsuficienteException {
        central.dameEnergia(energia);
    }

    @Override
    public boolean estaEnergizada() {
        return true;
    }

    @Override
    public int consultarEnergiaRestante() {
        return central.getCapacidadRestante();
    }

    @Override
    public void compensarEnergia(int energia) {
        central.compensarEnergia(energia);
    }

    public boolean equals(Object otroObjeto) {
        if (this == otroObjeto) {
            return true;
        }

        if (!(otroObjeto instanceof EnergizadorCentralElectrica)) {
            return false;
        }

        EnergizadorCentralElectrica otro = (EnergizadorCentralElectrica) otroObjeto;

        return central.equals(otro.central);
    }
}
