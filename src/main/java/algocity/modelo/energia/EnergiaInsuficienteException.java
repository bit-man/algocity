package algocity.modelo.energia;

public class EnergiaInsuficienteException
        extends Throwable {

    public EnergiaInsuficienteException() {
        super("Energía insuficiente");
    }
}
