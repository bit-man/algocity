package algocity.modelo.energia;

import java.io.Serializable;

public class EnergizadorNulo
        implements Energizador, Serializable {
    private static Energizador singleton = new EnergizadorNulo();

    private EnergizadorNulo() {};

    @Override
    public void dameEnergía(int energia) throws EnergiaInsuficienteException {
        throw  new EnergiaInsuficienteException();
    }

    @Override
    public boolean estaEnergizada() {
        return false;
    }

    @Override
    public int consultarEnergiaRestante() {
        return 0;
    }

    @Override
    public void compensarEnergia(int energia) {

    }

    // Preservación del singleton después de la deserialización
    private Object readResolve()
    {
        return singleton;
    }

    public static Energizador getInstance() {
        return singleton;
    }
}
