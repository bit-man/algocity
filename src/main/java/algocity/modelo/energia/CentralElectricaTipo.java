package algocity.modelo.energia;

public abstract class CentralElectricaTipo {

	protected int capacidadAbastecimiento;
	protected int costoInfraestructura;
	protected int rango;
	protected float reparacionSegunConstruccion;

	public int getCapacidadAbastecimiento(){
		return capacidadAbastecimiento;
	}


	public int getCostoInfraestructura(){
		return costoInfraestructura;
	}

	public int getRango() {
		return rango;
	};

	public float getReparacionSegunConstruccion() {
		return reparacionSegunConstruccion;
	}
}
