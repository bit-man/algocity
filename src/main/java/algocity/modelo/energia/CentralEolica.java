package algocity.modelo.energia;

import java.io.Serializable;

public class CentralEolica
		extends CentralElectricaTipo
		implements Serializable
{
	
	private static CentralEolica singleton = new CentralEolica();

	private CentralEolica() {
		super();
		costoInfraestructura = 1_000;
		capacidadAbastecimiento = 100_000_000;
		rango = 4;
		reparacionSegunConstruccion = (float) 0.15;
	}

	public static CentralEolica getInstance() {
		return singleton;
	}

	// Preservación del singleton después de la deserialización
	private Object readResolve()
	{
		return singleton;
	}

    public String toString() {
        return "Central Eólica";
    }
}
