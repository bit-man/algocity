package algocity.modelo.energia;

import java.io.Serializable;

public class CentralMineral
        extends CentralElectricaTipo
		implements Serializable
{
	
	private static CentralMineral singleton = new CentralMineral();

	private CentralMineral() {
		super();
		costoInfraestructura = 3000;
		capacidadAbastecimiento = 400_000_000;
		rango = 10;
		reparacionSegunConstruccion = (float) 0.1;
	}

	public static CentralMineral getInstance() {
		return singleton;
	}

	// Preservación del singleton después de la deserialización
	private Object readResolve()
	{
		return singleton;
	}

    public String toString() {
        return "Central Mineral";
    }
}
