package algocity.modelo.energia;

import java.io.Serializable;

public class CentralNuclear
		extends CentralElectricaTipo
		implements Serializable
{
	
	private static CentralNuclear singleton = new CentralNuclear();

	private CentralNuclear() {
		super();
		capacidadAbastecimiento = 1_000_000;
		costoInfraestructura = 10_000;
		rango = 25;
		reparacionSegunConstruccion = (float) 0.03;
	}

	public static CentralNuclear getInstance() {
		return singleton;
	}

	// Preservación del singleton después de la deserialización
	private Object readResolve()
	{
		return singleton;
	}

    public String toString() {
        return "Central Nuclear";
    }
}
