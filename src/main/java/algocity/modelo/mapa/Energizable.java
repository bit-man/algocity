package algocity.modelo.mapa;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.energia.Energizador;
import algocity.modelo.juego.ObjetoNulo;

public interface Energizable
{
    public void compensarEnergia(int energia);

    public boolean estaEnergizada();

    public void ahoraTieneEnergia(Energizador energizador);

    public int consultarEnergia();

    public void extraerEnergia(int consumoElectrico) throws EnergiaInsuficienteException;

    public void registrarAvisoEnergizacion(Operador<ObjetoNulo> avisadorEnergizacion);

    public void deregistrarAvisoEnergizacion(Operador<ObjetoNulo> avisadorEnergizacion);
}
