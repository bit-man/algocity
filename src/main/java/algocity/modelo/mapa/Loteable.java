package algocity.modelo.mapa;

import algocity.modelo.necesidades.Conexion;
import algocity.modelo.zona.Construccion;
import algocity.modelo.zona.Superficie;

import java.util.Collection;

public interface Loteable {

    public Superficie getSuperficie();

    public void setConstruccion(Construccion construccion);

    public Posicion getPosicion();

    public Collection<Conexion> getConexionesDisponibles();
}
