package algocity.modelo.mapa;

public interface Censable {

    public int getPoblacion();

    public void setPoblacion(int agregar);
}
