package algocity.modelo.mapa;

public class FueraDelMapaException
        extends Throwable {
    private Posicion posicion;

    public FueraDelMapaException(Posicion posicion) {
        super("Error al querer sobrepasar los límites del mapa. Posición : " + posicion);
        this.posicion = posicion;
    }
}
