package algocity.modelo.mapa;

/**
 * Notar que este observador no es serializable, porque la
 * facilidad de estos observadores es que son deregistrados antes de la serialización
 * y vueltos a registrar luego de esta
 */
public abstract class OperadorDeregistrable<T>
    implements Operador<T>
{
    public Boolean esDeregistrable()
    {
        return true;
    }

}
