package algocity.modelo.mapa;

public interface Iterador2DVirtual<T>
    extends Iterador2D<T> {

    public boolean esObservableElementoActual();
}
