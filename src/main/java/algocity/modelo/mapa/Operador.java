package algocity.modelo.mapa;

public interface Operador<T> {

    public Boolean operar(T parametro);

    public Boolean esDeregistrable();

}
