package algocity.modelo.mapa;

import java.io.Serializable;

/**
 * Notar que este observador no deregistrable es serializable
 * porque si no se deregistra al guadar el jugador
 * entonces al serializar el jugador también se serializa el operador
 */
public abstract class OperadorNoDeregistrable<T>
    implements Operador<T>, Serializable
{
    public Boolean esDeregistrable()
    {
        return false;
    }

}
