package algocity.modelo.mapa;

public interface Accesible {
    public void scan(Operador<Hectarea> operador);
    public Iterador2D<Hectarea> iterator(Posicion hectarea);
    public Iterador2D<Hectarea> iterator(int x, int y);
}
