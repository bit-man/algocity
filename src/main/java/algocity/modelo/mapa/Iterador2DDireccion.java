package algocity.modelo.mapa;

/**
 * Direcciones permitidas de iteración
 */

public enum Iterador2DDireccion {
    ARRIBA(0,-1), ABAJO(0, 1), DERECHA(1,0), IZQUIERDA(-1,0);

    private final int deltaX;
    private final int deltaY;

    Iterador2DDireccion(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    /**
     * Calcula la posición requerida según el desplazamiento
     * @param x Posicion inicial X
     * @param y Posicion inicial Y
     * @return devuelve la posición con el desplazamiento incorporado
     */
    public Posicion desplazamiento(int x, int y) {
        return new Posicion( x + deltaX, y + deltaY);
    }


}
