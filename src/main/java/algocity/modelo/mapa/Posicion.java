package algocity.modelo.mapa;


import java.io.Serializable;

public class Posicion
    implements Serializable {

    private final Integer x;
    private final Integer y;

    public Posicion(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public boolean equals(Object that) {
        if (!(that instanceof Posicion)) {
            return super.equals(that);
        }

        if (that == this) {
            return true;
        }

        // Se le indica al compilador que no muestre un warning indicando
        // que que el cast no puede ser verificado en tiempo de
        // compilación y que se haga bajo nuestro propio riesgo en tiempo
        // ejecución
        @SuppressWarnings("unchecked")
        Posicion cmp = (Posicion) that;
        return (x.equals(cmp.getX()) && y.equals(cmp.getY()));
    }

    public String toString() {
        return "{ x=" + x + ", y=" + y + " }";
    }
}
