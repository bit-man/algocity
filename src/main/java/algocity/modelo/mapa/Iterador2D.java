package algocity.modelo.mapa;

/**
 * Iterador que permite moverse en una superficie con 2 direcciones
 * @param <T> Tipo de elemento
 */
public interface Iterador2D<T> {

    /**
     * Elemento actual
     * @return devuelve el elemento actual
     */
    public T elementoActual();

    /**
     * Mueve una posición hacia la dirección indicada.
     * Debe devolver true el método hayMasHacia() con la misma dirección hacia la
     * que se quiere mover
     * @param direccion dirección en que se va a mover la posición actual
     * @throws FueraDelMapaException se arroja esta excepcción cuando se quiere
     * acceder a una posición fueraq de los límites del mapa
     */
    public void moverHacia(Iterador2DDireccion direccion) throws FueraDelMapaException;

    /**
     * Busca si hay un elemento en la dirección deseada
     * @param direccion Dirección de búsqueda
     * @return Devuelve true si hay un elemento o false en caso contrario
     */
    public boolean hayMasHacia(Iterador2DDireccion direccion);
}
