package algocity.modelo.juego;

public interface Calculadora<T> {
    public T calcular();
}
