package algocity.modelo.juego;

public interface Trabajable<T> {
    public Boolean trabajar(T parametro);
}
