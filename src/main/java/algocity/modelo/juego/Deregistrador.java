package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class Deregistrador<T,S>
    implements Registrable<T,S>
{

    public Collection<Operador<T>> deregistrar() {
        Set<Operador<T>> devolver = new HashSet<Operador<T>>();

        Iterator<Operador<T>> iterator = getObservadores().iterator();

        while( iterator.hasNext() ) {
            Operador<T> operador = iterator.next();
            if ( operador.esDeregistrable() ) {
                devolver.add(operador);
                iterator.remove();
            }
        }

        return devolver;
    }

    protected abstract Collection<Operador<T>> getObservadores();

    @Override
    public Collection<Operador<S>> deregistrarError()
    {
        return new HashSet<>();
    }

    @Override
    public void deregistrar(Operador<T> observador)
    {
        Iterator<Operador<T>> iterator = getObservadores().iterator();

        while( iterator.hasNext() ) {
            Operador<T> operador = iterator.next();
            if ( operador == observador ) {
                iterator.remove();
            }
        }
    }

}
