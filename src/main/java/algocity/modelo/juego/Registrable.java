package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

import java.util.Collection;

public interface Registrable<T,S> {
    public void registrar(Operador<T> observador);

    public Collection<Operador<T>> deregistrar();

    public Collection<Operador<S>> deregistrarError();

    public void deregistrar(Operador<T> observador);

    void deregistrarError(Operador<S> observador);
}
