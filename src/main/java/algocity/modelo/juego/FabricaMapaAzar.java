package algocity.modelo.juego;

import algocity.modelo.azar.FuncionAzar;
import algocity.modelo.mapa.Mapa;

public class FabricaMapaAzar
    implements FabricaMapa
{
    private final FuncionAzar azar;

    public FabricaMapaAzar(FuncionAzar azar)
    {
        this.azar = azar;
    }

    public Mapa crear()
    {
        return  new Mapa(Parametros.HECTAREAS_HORIZONTALES, Parametros.HECTAREAS_VERTICALES, azar);
    }
}
