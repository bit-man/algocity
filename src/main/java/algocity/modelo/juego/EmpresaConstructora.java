package algocity.modelo.juego;

import algocity.modelo.energia.CentralElectrica;
import algocity.modelo.energia.CentralEolica;
import algocity.modelo.energia.CentralMineral;
import algocity.modelo.energia.CentralNuclear;
import algocity.modelo.necesidades.LineaDeTension;
import algocity.modelo.necesidades.RutaPavimentada;
import algocity.modelo.necesidades.TuberiaDeAgua;
import algocity.modelo.zona.*;

import java.util.Collection;
import java.util.HashSet;

public class EmpresaConstructora {

    private EmpresaConstructora(){}

    private static Collection<ContratableConstruccion> crearContartistas(Jugable jugador) {
        Collection<ContratableConstruccion> contratableConstruccion = new HashSet<ContratableConstruccion>();
        contratableConstruccion.add(new Constratista(new Comercial(jugador)));
        contratableConstruccion.add(new Constratista(new Industrial(jugador)));
        contratableConstruccion.add(new Constratista(new Residencial(jugador)));
        contratableConstruccion.add(new Constratista(new EstacionDeBomberos(jugador)));
        contratableConstruccion.add(new Constratista(new PozoDeAgua(jugador)));
        contratableConstruccion.add(new Constratista(new CentralElectrica(CentralEolica.getInstance(), jugador.getMapa(), jugador)));
        contratableConstruccion.add(new Constratista(new CentralElectrica(CentralNuclear.getInstance(), jugador.getMapa(), jugador)));
        contratableConstruccion.add(new Constratista(new CentralElectrica(CentralMineral.getInstance(), jugador.getMapa(), jugador)));
        return contratableConstruccion;
     }

    public static Collection<ContratableConstruccion> contratablesConstruccion(Jugable jugador) {
        return crearContartistas(jugador);
    }

    public static Collection<ContratableConexion> contratablesConexion(Jugable jugador) {
        Collection<ContratableConexion> contratableConexion = new HashSet<ContratableConexion>();
        contratableConexion.add( new ContratistaConexion( new LineaDeTension(jugador)) );
        contratableConexion.add( new ContratistaConexion( new RutaPavimentada(jugador)) );
        contratableConexion.add( new ContratistaConexion( new TuberiaDeAgua(jugador)) );
        return contratableConexion;
    }
}
