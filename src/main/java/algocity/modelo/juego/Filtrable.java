package algocity.modelo.juego;

public interface Filtrable<T> {
    public boolean aceptado(T parametro);
}
