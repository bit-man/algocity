package algocity.modelo.juego;

public class JugadorInexistenteException
        extends Throwable {

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    private final String nombreUsuario;

    public JugadorInexistenteException(String nombreUsuario) {
        super("El jugador '" + nombreUsuario + "' no existe");
        this.nombreUsuario = nombreUsuario;
    }
}
