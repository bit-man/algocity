package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public abstract class MinisterioSubroganteDeFin
    implements Trabajable<ObjetoNulo>, Registrable<Object,ObjetoNulo>, Serializable
{

    protected final LlamadorMaestro llamador;
    protected int contadorCondicionDeFin;
    private boolean seguirEjecutando;

    public MinisterioSubroganteDeFin() {
        this.llamador = new LlamadorMaestro();
        contadorCondicionDeFin = 0;
        seguirEjecutando = true;
    }


    protected Boolean ejecucionFinal(int condicionDeFin) {
        // Esto evita que una vez que se pierde y se graba el juego
        // se pueda seguir jugando
        if ( ! seguirEjecutando ) {
            return seguirEjecutando;
        }

        boolean condicionCumplida = contadorCondicionDeFin == condicionDeFin;

        if ( condicionCumplida ) {
            llamador.llamarYOlvidar(ObjetoNulo.getInstance());
        }

        this.seguirEjecutando = contadorCondicionDeFin < condicionDeFin;
        return seguirEjecutando;
    }

    @Override
    public void registrar(Operador<Object> observador) {
        llamador.registrar(observador);
    }


    @Override
    public void deregistrar(Operador<Object> observador)
    {
        llamador.deregistrar(observador);
    }

    @Override
    public void deregistrarError(Operador<ObjetoNulo> observador)
    {
    }

    public java.util.Collection<Operador<Object>> deregistrar()
    {
        return llamador.deregistrar();
    }

    public Collection<Operador<ObjetoNulo>> deregistrarError()
    {
        return new HashSet<>();
    }
}
