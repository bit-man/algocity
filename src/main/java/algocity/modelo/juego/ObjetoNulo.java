package algocity.modelo.juego;

public class ObjetoNulo {

    private static ObjetoNulo singleton = new ObjetoNulo();

    private ObjetoNulo(){};

    public static ObjetoNulo getInstance() {
        return singleton;
    }

    // Preservación del singleton después de la deserialización
    private Object readResolve()
    {
        return singleton;
    }
}
