package algocity.modelo.juego;

public class LongitudNombreJugadorInsuficienteException
        extends Throwable {
    public LongitudNombreJugadorInsuficienteException(String mensaje) {
        super(mensaje);
    }
}
