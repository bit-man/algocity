package algocity.modelo.juego;

public interface Llamador<T,S>
    extends Registrable<T,S>{
    public void llamar(T parametro);
    public void llamarYOlvidar(T parametro);
}
