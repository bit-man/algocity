package algocity.modelo.juego;

import algocity.modelo.mapa.Mapa;

public interface FabricaMapa
{
    public Mapa crear();
}
