package algocity.modelo.juego;

public class JugadorExistenteException
        extends Throwable {
    public JugadorExistenteException(String mensaje) {
        super("El jugador " + mensaje + " ya existe");
    }
}
