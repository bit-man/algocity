package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

import java.io.Serializable;
import java.util.Iterator;

public class LlamadorCondicional<T,S>
    extends LlamadorMaestro<T,S>
    implements Serializable {

    public LlamadorCondicional() {
        super();
    }

    @Override
    public void llamar(T parametro) {
        Iterator<Operador<T>> iterator = getObservadores().iterator();

        boolean puedoContinuar = true;

        while( iterator.hasNext() && puedoContinuar ) {
            Operador<T> operador = iterator.next();
            puedoContinuar = operador.operar(parametro);
        }
    }

}
