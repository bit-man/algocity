package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

public interface RegistrableError {
    public void registrarAvisoError(Operador<String> observador);
}
