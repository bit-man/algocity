package algocity.modelo.juego;

import java.io.Serializable;

public class MonetizableNull
    implements Monetizable, Serializable
{

    private static MonetizableNull singleton = new MonetizableNull();

    private MonetizableNull(){};

    public static MonetizableNull getInstance() {
        return singleton;
    }

    @Override
    public int getDinero() {
        return 0;
    }

    @Override
    public void agregarDinero(int dinero) {

    }

    @Override
    public void quitarDinero(int dinero) throws DineroInsuficienteException {

    }

    // Preservación del singleton después de la deserialización
    private Object readResolve()
    {
        return singleton;
    }
}
