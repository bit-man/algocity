package algocity.modelo.juego;

import java.io.Serializable;

public class MinisterioDeGanancias
        extends MinisterioSubroganteDeFin
        implements Serializable {

    private final MinisterioDeBienestarSocial ministerioDeBienestarSocial;

    public MinisterioDeGanancias(MinisterioDeBienestarSocial ministerioDeBienestarSocial) {
        super();
        this.ministerioDeBienestarSocial = ministerioDeBienestarSocial;
    }

    @Override
    public Boolean trabajar(ObjetoNulo parametro) {

        if ( ministerioDeBienestarSocial.obtenerUltimoCenso() > 0 ) {
            contadorCondicionDeFin++;
        } else {
            contadorCondicionDeFin = 0;
        }

        return ejecucionFinal(Parametros.GANADOR_CANTIDAD_DE_AUMENTOS_CONSECUTIVOS_DE_POBLACION);
    }

}
