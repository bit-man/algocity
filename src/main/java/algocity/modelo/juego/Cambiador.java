package algocity.modelo.juego;

import java.util.Collection;

public interface Cambiador<T> {

    public T item();

    public void cambiar();

    public void agregar(T item);

    public boolean dioLaVuelta();

    public void agregar(Collection<T> lista);

    public void reiniciar();
}
