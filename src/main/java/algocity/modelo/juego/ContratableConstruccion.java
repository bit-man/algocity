package algocity.modelo.juego;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.Construccion;

public interface ContratableConstruccion {
    public void construir(Hectarea hectarea) throws FueraDelMapaException, JuegoNoIniciadoException, DineroInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException;

    public Construccion getConstruccion();
}
