package algocity.modelo.juego;


import algocity.modelo.mapa.OperadorNoDeregistrable;

import java.io.Serializable;

public class DespertarAlMinisterio
        extends OperadorNoDeregistrable<Object>
        implements Serializable
{

    private final Trabajable cuerpoDeTrabajo;

    public DespertarAlMinisterio(Trabajable cuerpoDeTrabajo) {
        this.cuerpoDeTrabajo = cuerpoDeTrabajo;
    }

    @Override
    public Boolean operar(Object parametro) {
        return cuerpoDeTrabajo.trabajar( ObjetoNulo.getInstance());
    }
}
