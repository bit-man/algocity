package algocity.modelo.juego;

import java.io.Serializable;

public class MinisterioDeHacienda
    implements Trabajable<ObjetoNulo>, Serializable {

    private final Jugador jugador;

    public MinisterioDeHacienda(Jugador jugador) {
        this.jugador = jugador;
    }

    public Boolean trabajar(ObjetoNulo parametro) {
        if ( jugador.getTiempoActual() % Parametros.PERIODICIDAD_RECOLECCION_IMPUESTOS == 0) {
            jugador.agregarDinero(jugador.getMapa().getPoblacion() * Parametros.IMPUESTO_POR_HABITANTE);
        }
        return Boolean.TRUE;
    }
}
