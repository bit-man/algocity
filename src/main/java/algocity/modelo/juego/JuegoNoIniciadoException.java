package algocity.modelo.juego;

public class JuegoNoIniciadoException
        extends Throwable {
    public JuegoNoIniciadoException() {
        super("No hay juego iniciado");
    }
}
