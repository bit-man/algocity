package algocity.modelo.juego;

import java.io.Serializable;

/**
 * Los puntos otorgados son directamente proporcionales al crecimiento de la población
 */
public class MinisterioDePuntos
        implements Trabajable<ObjetoNulo>, Serializable {

    private final MinisterioDeBienestarSocial ministerio;
    private final Jugador jugador;

    public MinisterioDePuntos(Jugador jugador, MinisterioDeBienestarSocial ministerio) {
        this.ministerio = ministerio;
        this.jugador = jugador;
    }

    @Override
    public Boolean trabajar(ObjetoNulo paarmetro) {
        int balance = ministerio.obtenerBalance();
        if ( balance > 0) {
            jugador.agregarPuntaje( Parametros.MULTIPLICADOR_PUNTAJE * balance );
        }
        return Boolean.TRUE;
    }
}
