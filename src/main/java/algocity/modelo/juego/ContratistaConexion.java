package algocity.modelo.juego;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;

public class ContratistaConexion
        implements ContratableConexion {

    private final Conexion conexion;

    public ContratistaConexion(Conexion conexion) {
        this.conexion = conexion;
    }

    @Override
    public void construir(Hectarea hectareaOrigen, Hectarea hectareaDestino) throws ConstruccionInvalidaExcepcion, DineroInsuficienteException, SuperficieInvalidaException, JuegoNoIniciadoException

    {
        conexion.construite(hectareaOrigen, hectareaDestino);
    }

    @Override
    public Conexion getConexion() {
        return conexion;
    }
}
