package algocity.modelo.juego;

import java.util.ArrayList;
import java.util.Collection;

public class ColeccionesAlgoCity {

    public static <T> boolean contiene(Collection<T> collection, Filtrable<T> comparator) {
        return filtrar(collection, comparator).size() > 0;
    }

    public static <T> Collection<T> filtrar(Collection<T> coleccionOrigen, Filtrable<T> filtro) {
        Collection<T> coleccionDestino = new ArrayList<T>();
        for(  T elemento : coleccionOrigen ) {
            if (filtro.aceptado(elemento)) {
                coleccionDestino.add(elemento);
            }
        }
        return coleccionDestino;
    }
}
