package algocity.modelo.juego;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.Construccion;

public class Constratista
        implements ContratableConstruccion {
    private final Construccion construccion;

    public Constratista(Construccion construccion) {
        this.construccion = construccion;
    }

    @Override
    public void construir(Hectarea hectarea) throws FueraDelMapaException, JuegoNoIniciadoException, DineroInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException {
        construccion.construite(hectarea);
    }

    @Override
    public Construccion getConstruccion() {
        return construccion;
    }

}
