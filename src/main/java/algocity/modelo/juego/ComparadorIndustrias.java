package algocity.modelo.juego;

import algocity.modelo.zona.Construccion;

public class ComparadorIndustrias
        implements Filtrable<Construccion> {
    @Override
    public boolean aceptado(Construccion parametro) {
        return parametro.esIndustrial();
    }
}
