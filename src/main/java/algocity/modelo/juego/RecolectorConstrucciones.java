package algocity.modelo.juego;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.OperadorNoDeregistrable;
import algocity.modelo.zona.Construccion;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public class RecolectorConstrucciones
        extends OperadorNoDeregistrable<Hectarea>
        implements Serializable
{

    private Collection<Construccion> construcciones;

    public Collection<Construccion> getConstrucciones() {
        return construcciones;
    }

    public RecolectorConstrucciones() {
        construcciones = new HashSet<Construccion>();
    }

    @Override
    public Boolean operar(Hectarea hectarea) {
        construcciones.add( hectarea.getConstruccion());

        return Boolean.TRUE;
    }

}
