package algocity.modelo.juego;

import java.io.File;

public class Parametros {
    public static final int HECTAREAS_HORIZONTALES = 100;
    public static final int HECTAREAS_VERTICALES = 100;
    public static final int DINERO_INICIAL = 20_000;
    public static final int MINIMA_CANTIDAD_DE_REGISTROS_PARA_ACUMULAR_PUNTAJE = 10;
    public static final int MULTIPLICADOR_PUNTAJE = 10;
    public static final double PROBABILIDAD_CATASTROFE = 0.01;
    public static final int PERIODICIDAD_RECOLECCION_IMPUESTOS = 30;
    public static final int IMPUESTO_POR_HABITANTE = 10;
    public static final float UMBRAL_DE_TIERRA = (float) 0.9;
    public static final int GANADOR_CANTIDAD_DE_AUMENTOS_CONSECUTIVOS_DE_POBLACION = 10;
    public static final float GODZILLA_PROBABILIDAD_CAMBIO_DE_DIRECCION = (float) 0.9;
    public static final int GODZILLA_CAMBIOS_DE_DIRECCION_MAXIMO = (int) 10;
    public static final int PERDEDOR_CUENTA_CONSECUTIVA_SIN_POBLACION = 10;
    public static final float DANIO_TERREMOTO = (float) 0.015;
    public static final File DIRECTORIO_ALMACENAMIENTO_CONFIGURACION = new File(System.getProperty("user.home"), ".algocity");
    public static final int PUNTAJE_INICIAL_JUGADOR = 0;
    public static final String ALMACENAMIENTO_DIRECTORIO_JUGADOR = "jugador";
    public static final String ALMACENAMIENTO_PUNTAJES = "puntajes";
}
