package algocity.modelo.juego;

import java.io.File;

public class DirectorioDeConfiguracionNoInicializadoException extends Throwable {

    public DirectorioDeConfiguracionNoInicializadoException(File directorioAlmacenamiento, Throwable e) {
        this(directorioAlmacenamiento);
        initCause(e);
    }

    public DirectorioDeConfiguracionNoInicializadoException(File directoriodeAlmacenamiento)
    {
        super("El directorio de Configuración del juego no puede crearse en " + directoriodeAlmacenamiento.getAbsolutePath());
    }
}
