package algocity.modelo.juego;

import algocity.modelo.mapa.Hectarea;
import algocity.modelo.necesidades.Conexion;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;

public interface ContratableConexion {

    public void construir(Hectarea hectareaOrigen, Hectarea hectareaDestino) throws ConstruccionInvalidaExcepcion, DineroInsuficienteException, SuperficieInvalidaException, JuegoNoIniciadoException;

    public Conexion getConexion();

}
