package algocity.modelo.juego;

public interface Monetizable
{
    public int getDinero();

    public void agregarDinero(int dinero);

    public void quitarDinero(int dinero) throws DineroInsuficienteException;

}