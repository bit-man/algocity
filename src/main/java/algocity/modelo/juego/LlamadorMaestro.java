package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class LlamadorMaestro<T,S>
        extends Deregistrador<T,S>
        implements Llamador<T,S>, Serializable {

    protected Collection<Operador<T>> observadores;

    public LlamadorMaestro() {
        init();
    }

    private void init() {
        observadores = new ArrayList<Operador<T>>();
    }

    @Override
    public void registrar(Operador<T> observador) {
        observadores.add(observador);
    }

    @Override
    public void deregistrarError(Operador<S> observador)
    {

    }


    @Override
    protected Collection<Operador<T>> getObservadores()
    {
        return observadores;
    }

    @Override
    public void llamar(T parametro) {
        for ( Operador<T> observador : observadores ) {
            observador.operar(parametro);
        }
    }

    @Override
    public void llamarYOlvidar(T parametro) {
        llamar(parametro);
        init();
    }
}
