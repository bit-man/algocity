package algocity.modelo.juego;

import algocity.modelo.mapa.Mapa;

public interface Jugable
    extends Monetizable, Temporizable, Puntuable, RegistrableError {

    public String getNombre();

    public Mapa getMapa();

}

