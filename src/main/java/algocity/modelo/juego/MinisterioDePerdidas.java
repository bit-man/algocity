package algocity.modelo.juego;

import algocity.modelo.mapa.Mapa;

import java.io.Serializable;

public class MinisterioDePerdidas
    extends MinisterioSubroganteDeFin
    implements Serializable {

    private final Mapa mapa;

    public MinisterioDePerdidas(Mapa mapa) {
        super();
        this.mapa = mapa;
    }

    @Override
    public Boolean trabajar(ObjetoNulo parametro) {

        if ( mapa.getPoblacion() == 0 ) {
            contadorCondicionDeFin++;
        } else {
            contadorCondicionDeFin = 0;
        }

        return ejecucionFinal(Parametros.PERDEDOR_CUENTA_CONSECUTIVA_SIN_POBLACION);
    }

}
