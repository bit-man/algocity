package algocity.modelo.juego;

public interface Puntuable {
    public int getPuntaje();
    public void agregarPuntaje( int puntos );
}
