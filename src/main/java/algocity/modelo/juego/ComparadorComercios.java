package algocity.modelo.juego;

import algocity.modelo.zona.Construccion;

public class ComparadorComercios
        implements Filtrable<Construccion> {

    @Override
    public boolean aceptado(Construccion parametro) {
        return parametro.esComercial();
    }

}
