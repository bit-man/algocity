package algocity.modelo.juego;

import algocity.modelo.zona.Construccion;

public class ComparadorResidencias
        implements Filtrable<Construccion> {

    @Override
    public boolean aceptado(Construccion parametro) {
        return parametro.esResidencial();
    }
}
