package algocity.modelo.juego;

import algocity.modelo.mapa.Dimension;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Mapa;
import algocity.modelo.mapa.Posicion;

public class CalculadorRadios
    implements Calculadora<Integer> {

    private final Hectarea epicentro;
    private final Mapa mapa;

    public CalculadorRadios(Hectarea epicentro, Mapa mapa) {
        this.epicentro = epicentro;
        this.mapa = mapa;
    }

    @Override
    public Integer calcular() {
        final Posicion epicentroPosicion = epicentro.getPosicion();
        final Dimension dimensiones = mapa.getDimensiones();
        // Máximo radio en las direcciones X e Y
        final int maxPosXMapa = dimensiones.getX() - 1;
        final int maxPosYMapa = dimensiones.getY() - 1;
        final int maxX = Math.max(maxPosXMapa - epicentroPosicion.getX(), epicentroPosicion.getX() );
        final int maxY = Math.max(maxPosYMapa - epicentroPosicion.getY(), epicentroPosicion.getY() );
        return Math.max(maxX, maxY);
    }
}
