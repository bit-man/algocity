package algocity.modelo.juego;

public class DineroInsuficienteException
        extends Throwable {

    public DineroInsuficienteException() {
        super("No hay dinero suficiente");
    }
}
