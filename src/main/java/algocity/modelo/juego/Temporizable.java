package algocity.modelo.juego;

import algocity.modelo.mapa.Operador;

public interface Temporizable {

    public void registrarAvanceTiempo(Operador<Object> observador);

    public java.util.Collection<Operador<Object>> deregistrarAvanceDeTiempo();

    public int getTiempoActual();

    public void avanzarTiempo() ;

    public void deregistrarAvanceDeTiempo(Operador<Object> notificador);
}
