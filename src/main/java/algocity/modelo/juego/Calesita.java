package algocity.modelo.juego;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Calesita<T>
    implements Cambiador<T>, Serializable{

    /**
     * Originalmente estaba implementado con un iterador pero
     * se debió cambiar la implementación porque los iteradores
     * de las colecciones no son serializables
     */
    private List<T> calesita;
    private int ubicacionProximoItem;
    private boolean dioLaVuelta;

    public Calesita() {
        this.calesita = new ArrayList<T>();
        dioLaVuelta = false;
        init();
    }

    private void init() {
        this.ubicacionProximoItem = 0;
    }

    public T item() {
        return calesita.get(ubicacionProximoItem);
    }

    public boolean dioLaVuelta() {
        return dioLaVuelta;
    }

    @Override
    public void agregar(Collection<T> lista) {
        for( T item : lista) {
            agregar(item);
        }
    }

    @Override
    public void reiniciar() {
        init();
        dioLaVuelta = false;
    }

    public void cambiar() {
        ubicacionProximoItem++;
        if (  ubicacionProximoItem == calesita.size()) {
            init();
            this.dioLaVuelta = true;
        } else {
            dioLaVuelta = false;
        }
    }

    public void agregar(T item) {
        calesita.add(item);
        init();
    }
}
