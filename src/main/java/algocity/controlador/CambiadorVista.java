package algocity.controlador;

import javax.swing.*;

public interface CambiadorVista
{
    public void cambiar();

    public void setPrender(JFrame frame);

    public CambiadorVista inverso();
}
