package algocity.controlador;



import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InsertarImagenes extends JPanel{
	
	
	 public  InsertarImagenes( String imageURL){
		 
		 
         setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
         add( new JLabel( new ImageIcon(imageURL )));
             
     }

}
