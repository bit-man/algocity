package algocity.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VueltaAtrasAccion
        implements ActionListener
{
    private final CambiadorVista cambiadorVista;

    public VueltaAtrasAccion(CambiadorVista cambiadorVista)
    {
        this.cambiadorVista = cambiadorVista;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        cambiadorVista.cambiar();
    }
}
