package algocity.controlador;

import algocity.vista.MenuAlgoCity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VueltaAlMenuInicial
		implements ActionListener
{
	
	private JFrame ventana;
	
	public VueltaAlMenuInicial(JFrame ventana) {
		this.ventana=ventana;
	}
	
	@Override
    public void actionPerformed(ActionEvent e) {
		  ventana.setVisible(false);
		  try {
			  MenuAlgoCity menu = new MenuAlgoCity();
		  } catch (Throwable ex) {
			  JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
		  }
	}

}
