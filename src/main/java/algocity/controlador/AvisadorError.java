package algocity.controlador;

import algocity.modelo.mapa.Operador;

public interface AvisadorError {
    public void registrarError(Operador<String> observadorError);
}
