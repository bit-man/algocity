package algocity.controlador;

import algocity.modelo.juego.LlamadorMaestro;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Operador;
import algocity.modelo.mapa.OperadorDeregistrable;

public class LlamadorConstructorConexion
        extends OperadorDeregistrable<Hectarea>
        implements Operador<Hectarea>, AvisadorError {

    private final ConstruyendoConexion construyendoConexion;
    private LlamadorMaestro<String,ObjetoNulo> llamador;

    public LlamadorConstructorConexion(ConstruyendoConexion construyendoConexion) {
        this.construyendoConexion = construyendoConexion;
        llamador = new LlamadorMaestro<String,ObjetoNulo>();
    }

    @Override
    public Boolean operar(Hectarea hectareaDestino) {
        try {
            construyendoConexion.construir(construyendoConexion.getOrigen(), hectareaDestino);
        } catch (Throwable ex) {
            llamador.llamar(ex.getLocalizedMessage());
        }
        return Boolean.TRUE;
    }

    @Override
    public void registrarError(Operador<String> observadorError) {
        llamador.registrar(observadorError);
    }
}
