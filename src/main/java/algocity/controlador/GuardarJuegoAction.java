package algocity.controlador;

import algocity.modelo.almacenamiento.ErrorAlGuardarJugadorException;
import algocity.modelo.juego.Juego;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.vista.MensajeroEstado;
import algocity.vista.MenuAlgoCity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuardarJuegoAction
        implements ActionListener
{
    private MensajeroEstado mensajero;

    public GuardarJuegoAction(MensajeroEstado mensajero)
    {
        this.mensajero = mensajero;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try
        {
            MenuAlgoCity.juego().guardarJuego();
            this.mensajero.agregarMensaje("Juego guardado");
        } catch (Throwable ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR: " + ex.getLocalizedMessage());
        }
    }
}
