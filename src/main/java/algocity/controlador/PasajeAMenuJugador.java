package algocity.controlador;

import algocity.vista.MenuJugador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PasajeAMenuJugador implements ActionListener {
	 private final JFrame panel;
	
	  public PasajeAMenuJugador(JFrame panel) {
		this.panel=panel;
	}

	@Override
	  public void actionPerformed(ActionEvent e) {
		  panel.setVisible(false);
		  try {
			  MenuJugador menujugador = new MenuJugador();
		  } catch (Throwable ex) {
			  JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
		  }
	  }
}
