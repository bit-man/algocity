package algocity.controlador;

import algocity.modelo.juego.Llamador;
import algocity.modelo.juego.LlamadorMaestro;
import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Operador;

import java.awt.event.MouseEvent;

public class HectareaMenuListenerMenuBotonDerecho
        implements  HectareaMenuListener {
    protected final Construyendo construyendo;
    protected final Llamador<String,ObjetoNulo> llamador;

    public HectareaMenuListenerMenuBotonDerecho(Construyendo construyendo) {
        this.construyendo = construyendo;
        llamador = new LlamadorMaestro<String,ObjetoNulo>();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        try {
            construyendo.construir();
        } catch (Throwable ex) {
            llamador.llamar("ERROR : " + ex.getLocalizedMessage());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void registrarError(Operador<String> observadorError) {
        llamador.registrar(observadorError);
    }
}
