package algocity.controlador;

import algocity.modelo.juego.ObjetoNulo;
import algocity.modelo.mapa.Operador;

public interface AvisadorFinalizacion {
    public void registrarFinalizacion( Operador<ObjetoNulo> observadorError);
}
