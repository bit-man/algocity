package algocity.controlador;

import algocity.modelo.juego.Temporizable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AvanzarTiempo
        implements ActionListener {
    private final Temporizable temporizable;
    private final int factorDeMultiplicacion;

    public AvanzarTiempo(Temporizable temporizable) {
        this(temporizable,1);
    }

    public AvanzarTiempo(Temporizable temporizable, int factorDeMultiplicacion) {
        this.temporizable = temporizable;
        this.factorDeMultiplicacion = factorDeMultiplicacion;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for ( int i = 0; i < factorDeMultiplicacion; i++) {
            temporizable.avanzarTiempo();
        }
    }
}
