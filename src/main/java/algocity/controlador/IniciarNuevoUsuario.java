package algocity.controlador;

import algocity.modelo.juego.Juego;
import algocity.modelo.mapa.OperadorNoDeregistrable;
import algocity.vista.AlgoCityJugador;
import algocity.vista.MenuAlgoCity;

import javax.swing.*;

public class IniciarNuevoUsuario
    extends OperadorNoDeregistrable<String>
{
    @Override
    public Boolean operar(String nombre)
    {
        try
        {
            Juego juego = MenuAlgoCity.juego();
            juego.agregarJugador(nombre);
            juego.iniciarPartida(nombre);
            new AlgoCityJugador().jugar();
        } catch (Throwable ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR: " + ex.getLocalizedMessage());
        }

        return Boolean.TRUE;
    }


}
