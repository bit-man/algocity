package algocity.controlador;

import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.vista.JugadorVista;
import algocity.vista.MenuAlgoCity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SalirDelJuegoAction
    implements ActionListener
{
    private final JugadorVista vista;

    public SalirDelJuegoAction(JugadorVista vista)
    {
        this.vista = vista;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try
        {
            vista.finalizar();
            MenuAlgoCity.juego().finJugadorActual();
        } catch (JuegoNoIniciadoException ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
        }
    }
}
