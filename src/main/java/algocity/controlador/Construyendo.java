package algocity.controlador;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.DineroInsuficienteException;
import algocity.modelo.juego.JuegoNoIniciadoException;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.necesidades.SuperficieInvalidaException;
import algocity.modelo.zona.ConstruccionInvalidaExcepcion;

public interface Construyendo {
    public void construir() throws FueraDelMapaException, JuegoNoIniciadoException, DineroInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException, ConstruccionInvalidaExcepcion;

}
