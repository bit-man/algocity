package algocity.controlador;

import algocity.modelo.catastrofe.Terremoto;
import algocity.modelo.mapa.Mapa;
import algocity.vista.AdaptadorMensajeroCatastrofes;
import algocity.vista.MensajeroEstado;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GenerarTerremoto
        implements ActionListener {
    private final Mapa mapa;
    private final MensajeroEstado mensajero;

    public GenerarTerremoto(Mapa mapa, MensajeroEstado mensajero) {
        this.mapa = mapa;
        this.mensajero = mensajero;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Terremoto terremoto = new Terremoto(mapa, mapa.hectareaAlAzar());
        terremoto.registrar(new AdaptadorMensajeroCatastrofes(mensajero) );
        terremoto.hacerDanio();
    }
}
