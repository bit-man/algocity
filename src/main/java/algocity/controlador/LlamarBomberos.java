package algocity.controlador;

import algocity.modelo.juego.Jugable;
import algocity.modelo.juego.Jugador;
import algocity.vista.Mensajero;
import algocity.modelo.zona.EstacionDeBomberos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LlamarBomberos implements ActionListener {
    private final Mensajero mensajero;
    private Jugable jugador;

    public LlamarBomberos(Jugador jugador, Mensajero mensajero) {
        this.jugador = jugador;
        this.mensajero = mensajero;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new EstacionDeBomberos(jugador).reparar();
        mensajero.agregarMensaje("Pasaron los bomberos");
    }
}
