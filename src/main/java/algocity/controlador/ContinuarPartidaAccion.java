package algocity.controlador;

import algocity.modelo.mapa.Operador;
import algocity.vista.AlgoCityJugador;
import algocity.vista.MenuAlgoCity;

import javax.swing.*;

public class ContinuarPartidaAccion
        implements Operador<String>
{
    @Override
    public Boolean operar(String nombre)
    {
        try
        {
            MenuAlgoCity.juego().continuarPartida(nombre);
            new AlgoCityJugador().jugar();
        } catch (Throwable ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
        }

        return Boolean.TRUE;
    }

    @Override
    public Boolean esDeregistrable()
    {
        return null;
    }
}
