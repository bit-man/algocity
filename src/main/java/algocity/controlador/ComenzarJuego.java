package algocity.controlador;

import algocity.modelo.mapa.OperadorNoDeregistrable;
import algocity.vista.AlgoCityJugador;
import algocity.vista.MenuAlgoCity;

import javax.swing.*;

public class ComenzarJuego
        extends OperadorNoDeregistrable<String>
{

    @Override
    public Boolean operar(String nombre)
    {
        try
        {
            MenuAlgoCity.juego().iniciarPartida(nombre);
            new AlgoCityJugador().jugar();
        } catch (Throwable ex)
        {
            JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
        }

        return Boolean.TRUE;
    }

}
