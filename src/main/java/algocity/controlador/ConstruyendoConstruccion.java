package algocity.controlador;

import algocity.modelo.energia.EnergiaInsuficienteException;
import algocity.modelo.juego.*;
import algocity.modelo.mapa.FueraDelMapaException;
import algocity.modelo.mapa.Hectarea;
import algocity.modelo.mapa.Operador;
import algocity.modelo.necesidades.SuperficieInvalidaException;

public class ConstruyendoConstruccion
        implements Construyendo, AvisadorFinalizacion {

    private final ContratableConstruccion contratableConstruccion;
    private final Hectarea hectarea;
    private final Llamador llamador;

    public ConstruyendoConstruccion(ContratableConstruccion contratableConstruccion, Hectarea hectarea) {
        this.contratableConstruccion = contratableConstruccion;
        this.hectarea = hectarea;
        this.llamador = new LlamadorMaestro();
    }

    @Override
    public void construir() throws FueraDelMapaException, JuegoNoIniciadoException, DineroInsuficienteException, EnergiaInsuficienteException, SuperficieInvalidaException {
        contratableConstruccion.construir(hectarea);
        llamador.llamar(ObjetoNulo.getInstance());
    }

    @Override
    public void registrarFinalizacion(Operador<ObjetoNulo> observadorFinalizacion) {
        llamador.registrar(observadorFinalizacion);
    }
}
