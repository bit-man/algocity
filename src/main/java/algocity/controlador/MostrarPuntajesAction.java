package algocity.controlador;

import algocity.vista.MostrarPuntajesVista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MostrarPuntajesAction
        implements ActionListener
{
    @Override
    public void actionPerformed(ActionEvent e)
    {
        new MostrarPuntajesVista();
    }
}
