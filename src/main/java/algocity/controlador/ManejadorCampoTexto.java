package algocity.controlador;

import algocity.vista.HabilitaPanelDeJugador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ManejadorCampoTexto
		extends JFrame
		implements ActionListener
{

	private String textoAccion;
	private LlamadorAction controlador;
	private CambiadorVista cambiador;

	public ManejadorCampoTexto(String textoAccion, LlamadorAction controlador, CambiadorVista cambiador) {
		this.textoAccion = textoAccion;
		this.controlador= controlador;
		this.cambiador = cambiador;
    }
    
    public void actionPerformed( ActionEvent evento )
    {
		  try {
			  new HabilitaPanelDeJugador(textoAccion, controlador, cambiador);
		  } catch (Throwable ex) {
			  JOptionPane.showMessageDialog(null, "ERROR : " + ex.getLocalizedMessage());
		  }
    	}
    }
