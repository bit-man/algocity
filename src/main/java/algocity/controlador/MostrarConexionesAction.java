package algocity.controlador;

import algocity.vista.VistaMapa;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MostrarConexionesAction
        implements ActionListener {
    private final VistaMapa vistaMapa;

    public MostrarConexionesAction(VistaMapa vistaMapa) {
         this.vistaMapa = vistaMapa;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        vistaMapa.cambiarDibujador();
        vistaMapa.actualizar();
    }
}
