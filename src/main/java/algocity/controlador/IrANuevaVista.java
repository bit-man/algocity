package algocity.controlador;

import javax.swing.*;

public class IrANuevaVista
    implements CambiadorVista
{
    private JFrame prender;
    private final JFrame apagar;

    public IrANuevaVista(JFrame prender, JFrame apagar) {
        this.prender = prender;
        this.apagar = apagar;
    }

    public IrANuevaVista( JFrame apagar)
    {
        this(null, apagar);
    }

    public void cambiar() {
        apagar.setVisible(false);
        prender.setVisible(true);
    }

    public void setPrender(JFrame frame)
    {
        this.prender = frame;
    }

    public CambiadorVista inverso()
    {
        return new IrANuevaVista(apagar, prender);
    }
}
