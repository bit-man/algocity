package algocity.controlador;

import algocity.modelo.mapa.Operador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LlamadorAction
        implements ActionListener {

    private final Operador<String> operador;
    private JTextField nombre;

    public LlamadorAction(Operador<String> operador)
    {
        nombre = new JTextField("JugadorAnónimo");
        this.operador = operador;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        operador.operar(nombre.getText());
    }

    public void setNombre(JTextField nombre)
    {
        this.nombre = nombre;
    }

}
