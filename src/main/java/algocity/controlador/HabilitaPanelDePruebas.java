package algocity.controlador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class HabilitaPanelDePruebas
        implements ActionListener{
    private final JFrame tableroDePruebas;

    public HabilitaPanelDePruebas(JFrame tableroDePruebas) {
        this.tableroDePruebas = tableroDePruebas;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
            tableroDePruebas.setVisible( ! tableroDePruebas.isVisible());
    }

}
