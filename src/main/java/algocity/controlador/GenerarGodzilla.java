package algocity.controlador;

import algocity.modelo.catastrofe.Godzilla;
import algocity.vista.AdaptadorMensajeroCatastrofes;
import algocity.vista.Mensajero;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GenerarGodzilla implements ActionListener {
    private final Mensajero mensajero;
    private Godzilla godzilla;

    public GenerarGodzilla(Godzilla godzilla, Mensajero mensajero) {
        this.godzilla = godzilla;
        this.mensajero = mensajero;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        godzilla.registrar(new AdaptadorMensajeroCatastrofes(mensajero));
        godzilla.hacerDanio();

    }
}
