package algocity.controlador;

import algocity.modelo.mapa.Operador;

public class HectareaMenuListenerMenuBotonIzquierdo
    extends HectareaMenuListenerMenuBotonDerecho {

    public HectareaMenuListenerMenuBotonIzquierdo(ConstruyendoConexion construyendoConexion) {
        super(construyendoConexion);
    }

    public void registrarError(Operador<String> observadorError) {
        ((ConstruyendoConexion)construyendo).registrarError(observadorError);
    }
}
