package algocity.controlador;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ForzarSalida
        implements ActionListener {

    private final JFrame panel;

    public ForzarSalida(JFrame panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         panel.dispose();
    }
}
